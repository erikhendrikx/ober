<?php 
# import_inschrijvingen_stap1.php
# Scherm om de voorspelling voor een kwalificatie in te voeren
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 24mei2019         -            E. Hendrikx 
# Symptom:   		    None.
# Problem:     	    None
# Fix:              None
# Feature:          Verkleinen input veld voor bestandsnaam
# Reference: 
?>
<html
<head>
<title>Upload inschrijvingen</title>
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">
<link rel="stylesheet" type="text/css" href="css/beheer.css"  /> 

		
<style type=text/css>
body {font-size: 8pt; font-family: Comic sans, sans-serif, Verdana; }

a    {text-decoration:none;font-size 9pt;}
a:hover {color:red;}




.inputfile + label {
	cursor: pointer; /* "hand" cursor */
}

input, label {
  display: block;
}

input[type=file]::-webkit-file-upload-button {
  border: 1px solid grey;
  background: red;
  color:white;
  width:150pt;
  height:24pt;font-size:11pt;padding:5pt;

  }
  
  
</style>
</head>
<body>
	

<?php

// Database gegevens. 
include('conf/mysqli.php');
$pageName = basename($_SERVER['SCRIPT_NAME']);
include('page_stats.php');
ob_start();

/// Als eerste kontrole op laatste aanlog. Indien langer dan 2uur geleden opnieuw aanloggen

 
 


?>
 
	<div style='background-color:white;font-size:9pt;font-family:verdana;padding:5pt;border:1pt solid #16B0F8;box-shadow: 8px 8px 8px #888888;'>


<h1>Import inschrijvingen via Excel xlsx bestand</t1>

<?php
  

// set max file size for upload (500 kb)
$max_file_size = 500000;

	
?>
<form action="import_leden_xlsx_stap2.php" method="POST" id='form-id' enctype="multipart/form-data">
 

<div style='text-align:left;color:black;font-size:9pt;font-family:arial;'>
	<blockquote>Met behulp van dit programma kunnen inschrijvingen vanuit een Excel bestand rechtstreeks in het OnTip systeem worden geimporteerd. <br>
	<h5>Stap 1</h5>
   Hieronder (zie stap 1) staat een link naar een template (= voorbeeld) bestand. Klik op deze link en sla dit bestand op op uw PC (xlsx formaat).<br> 
  <b>Omdat de structuur van dit bestand van wezenlijk belang is voor het importeren, dient u alleen gebruik te maken van dit voorbeeld bestand voor de import.</b><br>
	Vul m.b.v Excel de gegevens van de deelnemers in en sla het gewijzigde bestand onder een andere naam op in xlsx formaat. Verwijder geen kolommen !!
  <br>
	<h5>Stap 2</h5>
	Upload dit bestand vervolgens door op deze pagina het bestand te selecteren. Voordat de inschrijvingen worden geimporteerd zullen de gegevens eerst worden gekontroleerd. Er wordt NIET om een bevestiging gevraagd.<br>
	Er worden geen bevestigingen naar de deelnemers gestuurd, wel een bevestiging van het importeren naar het email adres van de organisatie (<?php echo $email_organisatie; ?>) .
	</blockquote>
	<br>
		
	
	<fieldset style='margin-left:25pt;width:85%;padding:15pt;border:1pt solid #16B0F8;box-shadow: 4px 4px 4px #888888;'>
	
	<legend style='left-padding:5pt;color:red;font-size:9px;font-family:Arial;'>Stap 1. xlsx template tbv inschrijvingen</legend>
	
	<table width=95% border = 0 >
	<tr>
<td width=55%  style='text-align:left;color:blue;font-size:11pt;font-family:arial;'>
<img src='../ontip/images/icon_excel.jpg' border = 0 width =22  >	<a href = '../ontip/xlsx/template_inschrijven.xlsx' > Klik hier om het template bestand op te halen.</a></td>

</tr>
</table>
		
</fieldset>
	
 <br>		

<br>

	<fieldset style='margin-left:25pt;width:85%;padding:15pt;border:1pt solid #16B0F8;box-shadow: 4px 4px 4px #888888;'>
	
	<legend style='left-padding:5pt;color:red;font-size:9px;font-family:Arial;'>Stap 2. Upload bestand met inschrijvingen</legend>
	
	
<table width=95% border = 0 >
	<tr>
<td width=45%  style='text-align:left;color:black;font-size:10pt;font-family:arial;'><img src='../ontip/images/icon_excel.jpg' border = 0 width =22  >Selecteer een Excel bestand met inschrijvingen voor importeren :  </td>
<td style='text-align:center;vertical-align:text-top;'>
	  <input style='text-align:left;vertical-align:background-color:lightblue;height:24pt;font-size:10pt;border:1pt solid grey;width:500pt;' id='file'  name="userfile" type="file" size="180">
		<INPUT type='submit' value='Upload gekozen bestand'  name = 'submit' style='background-color:darkgreen;;color:white;height:28pt;font-size:11pt;padding:5pt;'>

	</td>
</tr>
</table>
</fieldset>

</div>
</form>
<br>
</html>
