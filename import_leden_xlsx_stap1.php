<?php
# import_leden_xlsx_stap1.php

 
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#

ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
include ('../ontip/versleutel_string.php'); // tbv telnr en email
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}

$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT * FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$licentie       = $result0['Licentie'];
$vereniging_id  = $result0['Vereniging_id'];

$qry            = mysqli_query($con,"SELECT * From vereniging where Id = ".$vereniging_id." ")	or die(' Fout in select vereniging');  
$result2        = mysqli_fetch_array( $qry );
$vereniging     = $result2['Vereniging'];


?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="//bootstrap-extension.com/css/4.5.1/bootstrap-extension.min.css" type="text/css">
<script src="//bootstrap-extension.com/js/4.5.1/bootstrap-extension.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OnTip Baanreservering</title>
</head>
<body>
<br>
  
  <div id='top' class="container"  > 
  <div class= "row  align-content-right">
     <div class="col-12 text-center fixed-top">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php"   ></a>
     <a class=" btn btn-secondary btn-lg  "  href ='#top' class='back'>TOP</a>  
  </div> <!-- col-->    
  </div>  <!--- row---->    
   <br>
  <br>
  
    <div  class="card border border-success"  id ='kalender' style='box-shadow: 3px 3px 2px #888888;border-spacing: 4px 3px;'  >
  <div class="card-header">
     <div class = 'row  '>
     <strong><h4>Import leden bestand</h4></strong> 
	</div>
	</div><!---- header---->
	 
	 <div class="card-body">
           <br>
		    <p class = 'text'>Dit scherm kan gebruikt worden voor het importeren van een Excel (xlsx formaat) bestand met leden.<br>
	  Het bestand moet voldoen aan het template dat via de link hieronder gedownload kan worden. De import functie voert geen controles uit op dubbele of foutieve gegevens.
	  De import voegt alleen leden toe.</p>
		    <br>
	       <a href = 'https://www.ontip.nl/baanreservering/xlsx/leden_template.xlsx' > Klik hier voor Excel template</a>
           </b>
		   <b>
        <form action="import_leden_xlsx_stap2.php" method="POST" id='form-id' enctype="multipart/form-data">
          <input type="hidden" name="ftp_server" value="81.26.219.37">
            <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>">
      
          <input type="hidden" name="zendform" value="-1">

           
          <!--div class="col-sm-8 mr-auto ml-auto   p-4">
          <form method="post" enctype="multipart/form-data" action="upload.php">
            <div class="form-group">
              <label><strong>Upload leden bestand xlsx</strong></label>
              <div class="custom-file">
                <input type="file" name="userfile" multiple class="custom-file-input form-control" id="customFile">
                <label class="custom-file-label" for="customFile">Kies bestand (via browse or sleep bestand hierheen)</label>
              </div>
			  </div>
			 <div class="form-group">
  
      
             <select id ="inputVereniging" name= 'vereniging_id'  class="form-control" aria-describedby="verenigingHelp">
        <option value ='' selected>Kies...</option>
		<?php
			$sql        = mysqli_query($con,"SELECT * From vereniging  order by Vereniging_nr  ")     or die(' Fout in select ver');  	
			   while($row = mysqli_fetch_array( $sql )) {?>
			    <option value = '<?php echo $row['Id'];?>'  ><?php echo $row['Vereniging_nr']." - ".$row['Vereniging'];?></option>
        <?php
               }
		?>
      </select>
	    
	</div--->  
   <div>
			  <p class = 'text'>Vereniging : <?php echo $vereniging;?>
			  </div>
             
            <div class="form-group">
              <button type="submit" name="upload" value="upload" id="upload" class="btn btn-block btn-dark"><i class="fa fa-fw fa-upload"></i> Upload en import</button>
            </div>
          </form>
        </div>
       

</div> <!--- body--->
 </div> <!---ccard--->





 </div> <!--- row--->
 <script>
 $(document).ready(function() {
  $('input[type="file"]').on("change", function() {
    let filenames = [];
    let files = document.getElementById("customFile").files;
    if (files.length > 1) {
      filenames.push("Total Files (" + files.length + ")");
    } else {
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          filenames.push(files[i].name);
        }
      }
    }
    $(this)
      .next(".custom-file-label")
      .html(filenames.join(","));
  });
});
</script>
</body>
</html>
