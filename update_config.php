<?php
# update_config.php
# aanpassen instellingen
# aangeroepen vanuit  main.php knoppen balk
 
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#

// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

header("Location: ".$_SERVER['HTTP_REFERER']);

include('conf/mysqli.php'); 
//include('action.php');

 
$vereniging_id       = $_POST['vereniging_id'];
$aantal_banen        = $_POST['aantal_banen'];
$max_aantal_splrs    = $_POST['max_aantal_splrs'];
$max_aantal_splrs_per_baan    = 4;
$max_speelduur       = $_POST['max_speelduur'];
$max_freq_per_dag    = $_POST['max_freq_per_dag'];
$max_freq_per_week   = $_POST['max_freq_per_week'];

 if (isset($_POST['max_aantal_splrs_per_baan'])){
	$max_aantal_splrs_per_baan    = 2; 
	 
 }
 
 
$zondag_open     = 'N' ;
$maandag_open    = 'N';
$dinsdag_open    = 'N';
$woensdag_open   = 'N';
$donderdag_open  = 'N';
$vrijdag_open    = 'N';
$zaterdag_open   = 'N';
 
 
if (isset($_POST['zondag_open'])){
	$zondag_open = 'J';
}
if (isset($_POST['maandag_open'])){
	$maandag_open = 'J';
}
if (isset($_POST['dinsdag_open'])){
	$dinsdag_open = 'J';
}
if (isset($_POST['woensdag_open'])){
	$woensdag_open = 'J';
}
if (isset($_POST['donderdag_open'])){
	$donderdag_open = 'J';
}
if (isset($_POST['vrijdag_open'])){
	$vrijdag_open = 'J';
}
if (isset($_POST['zaterdag_open'])){
	$zaterdag_open = 'J';
}

if ($aantal_banen ==''){
	$message .= "* Er is geen aantal banen ingevuld.<br>";
	$error = 1;
}

if ($max_aantal_splrs ==''){
	$message .= "* Er is geen max aantal spelers ingevuld.<br>";
	$error = 1;
}

if ($max_speelduur==''){
	$message .= "* Er is geen max duur van een wedstrijd ingevuld.<br>";
	$error = 1;
}

$sql      = mysqli_query($con,"SELECT * from vereniging where Id = ".$vereniging_id."  ") or die('Aanloggen: Fout in select');  
$result     = mysqli_fetch_array( $sql );
$vereniging = $result['Vereniging'];


/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1

if ($error == 0){
			$update_query= "UPDATE `baanres_config` 
	              SET Vereniging                     = '".$vereniging."',
				      Aantal_banen                   =  ".$aantal_banen.",
				      Max_aantal_spelers             =  ".$max_aantal_splrs.",
				      Max_aantal_spelers_per_baan    =  ".$max_aantal_splrs_per_baan.",
					  Max_freq_per_dag               =  ".$max_freq_per_dag." , 
					  Max_freq_per_week              =  ".$max_freq_per_week." , 
				      Maximale_speelduur             =  ".$max_speelduur.",
					  Zondag_open                    = '".$zondag_open."', 
					  Maandag_open                   = '".$maandag_open."', 
					  Dinsdag_open                   = '".$dinsdag_open."', 
					  Woensdag_open                  = '".$woensdag_open."', 
					  Donderdag_open                 = '".$donderdag_open."', 
					  Vrijdag_open                   = '".$vrijdag_open."', 
				      Zaterdag_open                  = '".$zaterdag_open."', 
	    			  Laatst       = now() 
				  where Vereniging_id = ".$vereniging_id." "; 
				  
	echo "<br>".$update_query;
   mysqli_query($con,$update_query) or die ('Fout in update cfg' );  


 } // error = 0

?>

	
	


