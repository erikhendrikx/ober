<?php
# lijst_reserveringen_datum
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 11jun2020          1.0.1           E. Hendrikx
# Symptom:   		 None.
# Problem:       	 Bij verwijderen ook reserveringen van andere datum weg
# Fix:               Delid uitgebreid met datum
# Feature:           None.
# Reference: 
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">

 
<style type="text/css"  >
@media print {
    body {
        margin: 10;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 13px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
    }
 .noprint {display:none;}     
	 

h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
//include('action.php');
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}


if (!isset($_POST['datum'])){
	$datum         = $_GET['datum'];
	//$vereniging_id = $_GET['ver_id'];
} else {
	$datum         = $_POST['datum'];
	//$vereniging_id = $_POST['ver_id'];
}
 
 if (!isset($_GET['sort'])){
	$sort_key        =  'Begin_tijd_reservering';
} else {
	$sort_key        = $_GET['sort'];

}
  
$pageName      = basename($_SERVER['SCRIPT_NAME']); 
$part          = explode("-", $datum);
$_datum        = strftime("%a %e %B %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie, Admin_user FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$admin_user     = $result0['Admin_user'];
 
$sql           = mysqli_query($con,"SELECT * from vereniging where Id = ".$vereniging_id."  ") or die('Fout in select');  
$result        = mysqli_fetch_array( $sql );
$vereniging    = $result['Vereniging'];
$today         = date('Y-m-d');
  
?>
<div class= 'container'> 
  <div class= "row  ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th d-print-none"          data-toggle="tooltip" data-placement="bottom" title="Terug "             href="main.php?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $today;?>"   ></a>
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-dashboard d-print-none"    data-toggle="tooltip" data-placement="bottom" title="Statistieken"      href="lijst_reserveringen_datum_stats.php"   ></a>
  </div>
  

<div class="row">
  <div class ='col'>
 <h3> Baan reserveringen <h3>
  <h5>voor <?php echo $vereniging;?>  op <?php echo $_datum;?></h5>

 <FORM action="verwijder_reserveringen_datum.php" method=post  name='myForm'>
  
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
        <input type="hidden" name="datum" value="<?php echo $datum;?>" /> 

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
	  <th class= 'd-print-none' width=2% ><img src='images/trash_checked.png' width=20> </th>
      <th  ><a href="<?php echo $pageName;?>?datum=<?php echo $datum;?>&sort=Baan" target='_self'>Baan</a></th>
      <th  ><a href="<?php echo $pageName;?>?datum=<?php echo $datum;?>" target='_self'>Begin</a> </th>
      <th  >Eind </th>
      <th  >Spelers</th>
    </tr>
	</thead>
	<?php
 	
	 $qry_res_kalender  = mysqli_query($con,"SELECT Id,Baan, Begin_tijd_reservering, Eind_tijd_reservering  From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id." and Datum = '".$datum."' Group by Vereniging_id, Baan,Begin_tijd_reservering
							    	    	order by ".$sort_key." ")	or die(' Fout in select reservering ');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
			
			$delid = $row['Baan'].";".$row['Begin_tijd_reservering'];
			?>
	  <tr>
     	   <td class="d-print-none " >        
     		 <input type='checkbox' name= 'delete[]' class='trash'   id= 'trash_<?php echo $row['Id']; ?>' value='<?php echo $delid; ?>' /><label for="trash_<?php echo $row['Id']; ?>"></label>	
           </td>                 
      	    <td><?php echo $row['Baan'];?></td>
     	    <td><?php echo $row['Begin_tijd_reservering'];?></td>
     		<td><?php echo $row['Eind_tijd_reservering'];?></td>
     		<td>
     	    	<?php	 
     	    	$qry_res_splrs  = mysqli_query($con,"SELECT * From baanres_kalender
                            			    				where Vereniging_id = ".$vereniging_id." and 
															      Datum         = '".$datum."' and 
																  Baan          = ".$row['Baan']."
     	    						    				and Begin_tijd_reservering  = '".$row['Begin_tijd_reservering']."' order by Licentie ")	or die(' Fout in select reserv 2' );  
     	    	while($row2 = mysqli_fetch_array( $qry_res_splrs )) {
					
					if ($admin_user != 'X'){
    	    		echo $row2['Licentie']." ";
 					} else {
					$string = $row['Baan'].";".$row['Begin_tijd_reservering'].";".$row2['Licentie'];
					?>
					
					<input type='checkbox'  name='deluser[]'  value = '<?php echo $string;?>'/> <?php echo  $row2['Licentie']." ";?>
					<?php	
					}
     	    	}// end while
                    ?>
              </td>
     		
		</tr>
		<?php } // end while
?>		
 </table>
 </section>
  <em class='d-print-none'><h5>Bij verwijderen worden van de geselecteerde regel de reserveringen voor de aangegeven toegangscodes verwijderd.</h5></em>
<?php
	if ($admin_user == 'X'){ ?>
 <em class='d-print-none'><h5>Bij selectie van de afzonderlijke licentie in de laatste kolom worden alleen van die geselecteerde licenties de reserveringen verwijderd.</h5></em>
	<?php } ?>		
		
	<br>	
<div class="d-print-none">
        <input type="submit" class="btn btn-info" value="Verwijder geselecteerde reserveringen">
</div>
	</form>
 
 <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Nr</th>
      <th  >Toegangscode</th>
      <th  >Naam</th>
    </tr>
	</thead>
		
	<?php
	$i=1;
	$qry_naam_splrs  = mysqli_query($con,"SELECT k.Licentie, n.Naam From baanres_kalender as k 
		                                    join namen as n 
											   on k.Licentie = n.Licentie 
		where k.Vereniging_id = ".$vereniging_id." and k.Datum = '".$datum."'  group by k.Licentie order by k.Licentie")	or die(' Fout in select reserv 3' );  
												
		while($row = mysqli_fetch_array( $qry_naam_splrs )) {?>
		  <tr>
			  <td><?php echo $i;?></td>
			  <td><?php echo $row['Licentie'];?></td>
			  <td><?php echo $row['Naam'];?></td>
			  </tr>
		<?php
		$i++;
		}// end while

?>		
		
   </table>

 	    <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
  <br><br><hr>
  
   </div> <!----col----->
  </div> <!---- row ----->
  
  <div class="row">
  <div class ='col'>
  <h3  class= 'd-print-none'> Baan reserveringen andere datum <h3>
  <h4 class= 'd-print-none'>voor <?php echo $vereniging;?></h4>
  
 <FORM action="lijst_reserveringen_datum.php?ver_id=<?php echo $_GET['ver_id'];?>" method=post  name='myForm'>
       
        <input type="hidden" name="ver_id" value="<?php echo $vereniging_id;?>" /> 
     

  <div class="form-group row d-print-none form-inline">
    <div class="col-2 d-print-none text-left" >
      <label for="inputDatum">Datum</label>
    </div>
	 <div class="col-9 d-print-none" >
        <select id ="//datum" name= 'datum'  class="form-control d-print-none" aria-describedby="userHelp">
        <option value ='' selected>Kies...</option>
		<?php
		$today = date('Y-m-d');
	 
		  
	      $qry   = mysqli_query($con,"SELECT * From baanres_kalender where Vereniging_id = ".$vereniging_id."  group by Datum order by Datum ")	or die(' Fout in select datum');  
 		   while($row = mysqli_fetch_array( $qry )) {
			   $part          = explode("-", $row['Datum'] );
                $_datum       = strftime("%a %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
                 ?>
			    <option value = "<?php echo $row['Datum'];?>"  ><?php echo $_datum;?></option>
        <?php
               }
		           /// vandaag ook in de lijst 
				   $part          = explode("-", $today );
				   $_datum       = strftime("%a %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
              ?>
	      	 
      </select>
	   <small id="userHelp" class="form-text text-muted d-print-none">Selecteer de datum uit de lijst </small>
	</div>  
   </div>
     <input type="submit" class="btn btn-info d-print-none" value="Ophalen datum">
  </form>
  <hr/> 
  
  
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  </div>   <!---- col---> 
  </div> <!---- row ----->
 </div>  <!---- container---->
 
 
<script>
$(document).ready(function(){
  $("#myBtn").click(function(){
    $('.toast').toast('show');
  });
});
</script>


 </body>
 </html>
 