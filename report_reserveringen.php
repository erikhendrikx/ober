<?php 
# report_reserveringen.php
# Baanreservering  Report
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 17okt2020         -            E. Hendrikx 
# Symptom:   		None.
# Problem:     	    None
# Fix:              None
# Feature:          Link toegevoegd voor users per vereniging
# Reference: 
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">

@media print {
    body {
        margin: 10px;
		margin-top:15px;
		margin-right:15px;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 11px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
   }
	
@media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}

@media print {
    h5 {  font: 44px Arial, Helvetica,"Lucida Grande", serif;
          color:blue; }  
}

.noprint {display:none;}    
 
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$licentie       = $result0['Licentie'];
 
$part          = explode("-", $datum);
$_datum        = strftime("%A %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 

  $qry   = mysqli_query($con,"SELECT * From namen where  md5(Licentie) =  '".$licentie_md5."'  ")	or die(' Fout in select namen');  
  $result = mysqli_fetch_array( $qry );
  $naam   = $result['Naam'];
  $licentie       = $result['Licentie'];

?>
<div class= 'container'> 
  <div class= "row d-print-none ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?datum=<?php echo $datum;?>&<?php echo $kal;?>"   ></a>
  </div>

  
   
<h3> Report OBER  </h3>
  
 <?php
   $qry_res_totaal  = mysqli_query($con,"SELECT  count(*) as Aantal From namen  ")	or die(' Fout in select namen totaal ');  
   $result = mysqli_fetch_array( $qry_res_totaal );
   $totaal_namen  = $result['Aantal'];
   
   $qry_res_namen  = mysqli_query($con,"SELECT Vereniging_id, Vereniging, count(*) as Aantal From namen 
                   			    		 Group by Vereniging_id  Order by  Vereniging_id ")	or die(' Fout in select namen ');  
	
?>
<br>
<h5 style= 'color:blue;font-weight:bold;'>Aantal namen per vereniging</h5>
<h6 style= 'color:black;font-weight:bold;'>Klik op de vereniging id voor een detail lijst.</h6>
  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th style='border-bottom:1pt solid red;'  class='text-right red' width=2% >Nr.</th>
      <th style='border-bottom:1pt solid red;'  width=7%  >Ver Id</th>
      <th style='border-bottom:1pt solid red;'  >Vereniging</th>
      <th style='border-bottom:1pt solid red;' class='text-right red'>Aantal</th>
     </tr>
	</thead>
	<?php
	
	$i=1;
	while($row = mysqli_fetch_array( $qry_res_namen )) { ?>
		
	  <tr>
    	<td class='text-right'><?php echo $i;?>.</td>
        <td class='text-right'><a href="namen_vereniging.php?ver_id=<?php echo $row['Vereniging_id']; ?>" target='_self'><?php echo $row['Vereniging_id']; ?></a></td>
        <td><?php echo $row['Vereniging']; ?></td>
	    <td class='text-right'><?php echo $row['Aantal'];?></td>
	 
       </td>
		</tr>
		<?php
          $i++;
		  }  // end while kalender row 
?>	
   <tr>
	  <td colspan = 3><b>Totaal</b></td>
	  <td class='text-right ' style='border-top:2pt solid black;'><b><?php echo $totaal_namen;?></b></td>
	 </tr>
	
 </table>
 
  
 <hr/> 
  
   <?php
   $qry_res_totaal  = mysqli_query($con,"SELECT  count(*) as Aantal From baanres_kalender   ")	or die(' Fout in select kalender ');  
   $result = mysqli_fetch_array( $qry_res_totaal );
   $totaal_aantal  = $result['Aantal'];

   $qry_res_kalender  = mysqli_query($con,"SELECT Vereniging_id, Vereniging, count(*) as Aantal From baanres_kalender as k   
                                         join vereniging as v on k.Vereniging_id = v.Id 
                   			    		 Group by Vereniging_id  Order by  Aantal DESC ")	or die(' Fout in select kalender ');  
	
?>
 <div class="pagebreak"></div>
 <br>
<h5 style= 'color:blue;font-weight:bold;' >Aantal reservingen per vereniging</h5>

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th style='border-bottom:1pt solid red;' class='text-right' width=2% >Nr.</th>
      <th style='border-bottom:1pt solid red;' width=7%  >Ver Id</th>
      <th style='border-bottom:1pt solid red;'  >Vereniging</th>
      <th style='border-bottom:1pt solid red;' class='text-right' >Aantal</th>
     </tr>
	</thead>
	<?php
	
	$i=1;
	while($row = mysqli_fetch_array( $qry_res_kalender )) { ?>
		
	  <tr>
    	<td class='text-right'><?php echo $i;?>.</td>
        <td class='text-right'><?php echo $row['Vereniging_id']; ?></td>
        <td><?php echo $row['Vereniging']; ?></td>
	    <td class='text-right'><?php echo $row['Aantal'];?></td>
	 
       </td>
		</tr>
		<?php
          $i++;
		  }  // end while kalender row 
?>		
     <tr>
	  <td colspan = 3><b>Totaal</b></td>
	  <td class='text-right ' style='border-top:2pt solid black;'><b><?php echo $totaal_aantal;?></b></td>
	 </tr>

 </table>
 
  
 	 
 <hr/> 
 
   <?php
  /// maanden in kalender bepalen

$maanden = array();
$qry_res_maanden  = mysqli_query($con,"SELECT Date_format(Datum, '%Y-%m') AS Jaar_maand From baanres_kalender  
                   			    		 Group by Jaar_maand  Order by  Jaar_maand ")	or die(' Fout in select maanden ');    
   while($row = mysqli_fetch_array( $qry_res_maanden )) {
	   $maanden[] = $row['Jaar_maand'];
   }
    
   
?>

<h5 style= 'color:blue;font-weight:bold;'>Aantal reservingen per vereniging per maand</h5>

  <table class="table table-striped table-hover " >
  <thead>
    <tr  style='border-bottom:1pt solid red;'>
      <th style='border-bottom:1pt solid red;' class='text-right' width=2% >Nr.</th>
      <th style='border-bottom:1pt solid red;'  width=7%   >Ver Id</th>
      <th style='border-bottom:1pt solid red;' >Vereniging</th>
	<?php
       foreach($maanden as $maand)	{?>
		 <th style='border-bottom:1pt solid red;'  class='text-right' ><?php echo $maand;?></th>   
		   
	<?php	   
	   }
?>	  
 
     </tr>
	 
  <?php
  $i=1;
   $qry_res_kalender  = mysqli_query($con,"SELECT Vereniging_id, Vereniging, count(*) as Aantal From baanres_kalender as k   
                                         join vereniging as v on k.Vereniging_id = v.Id 
                   			    		 Group by Vereniging_id  Order by  Vereniging_id ")	or die(' Fout in select kalender 1');  
     while($row = mysqli_fetch_array( $qry_res_kalender )) { ?>										 
  
   <tr >
   	    <td class='text-right'><?php echo $i;?>.</td>
        <td class='text-right'><?php echo $row['Vereniging_id']; ?></td>
        <td><?php echo $row['Vereniging']; ?></td>
   <?php
           foreach($maanden as $maand)	{   
       
         									 
           $qry_res_maand = mysqli_query($con,"SELECT  Vereniging_id, Vereniging,count(*) as Aantal From baanres_kalender as k   
                                             join vereniging as v on k.Vereniging_id = v.Id 
	    									 where Date_format(Datum, '%Y-%m') = '".$maand."'
											 and Vereniging_id  = ".$row['Vereniging_id']."
                       			    		 Group by Vereniging_id   Order by  Vereniging_id  ")	or die(' Fout in select kalender2 ');  
	        $result = mysqli_fetch_array( $qry_res_maand );
	    	?>
	    	  <td class='text-right border'><?php echo $result['Aantal'];?></td>
	    	<?php
	       } // end for each
            ?>	 
	   </tr>
			<?php
          $i++;
	  }  // end while kalender row 
?>		
   <tr>
	  <td colspan = 3><b>Totaal</b></td>
	  
	  <?php
	   foreach($maanden as $maand)	{   
           $qry_res_maand = mysqli_query($con,"SELECT  count(*) as Aantal From baanres_kalender as k   
                                             join vereniging as v on k.Vereniging_id = v.Id 
	    									 where Date_format(Datum, '%Y-%m') = '".$maand."'    ")	or die(' Fout in select kalender2 ');  
	        $result = mysqli_fetch_array( $qry_res_maand );
	    	?>
	    <td class='text-right ' style='border-top:2pt solid black;'><b><?php echo $result['Aantal'];?></b></td>
	 
	    	<?php
	       } // end for each
  ?>

	 </tr>



 </table>
 
 	    <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
 <hr/>  
  
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  
 </div>  <!---- container---->
  

 </body>
 </html>
 