<?php
# verwerk_aanlog.php
# Kontrole van wchtwoord op aanloggen.php
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
#
# 29dec2018          1.0.1            E. Hendrikx
# Symptom:   		    None.
# Problem:       	  Onbekende var key
# Fix:              Opgelost
# Feature:          None.
# Reference: 
#


ob_start();
include 'conf/mysqli.php'; 
//include 'versleutel.php'; 

function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}
$count =0 ;

$licentie           = $_POST['licentie']; 
$pin                = $_POST['pin']; 
$pin_encrypt        = md5($pin);
$ip                 = md5($_SERVER['REMOTE_ADDR']);

//include('action.php');
 
// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

if ($licentie ==''){
	$message .= "* Er is geen licentie ingevuld.<br>";
	$error = 1;
}

if ($pin ==''){
	$message .= "* Er is geen pin ingevuld.<br>";
	$error = 1;
}

if ($error == 0){

$sql      = mysqli_query($con,"SELECT Vereniging_id, count(*) as Aantal  FROM namen WHERE  Licentie = '".$licentie."' and PIN_encrypt = '".$pin_encrypt."' ") or die(' Fout in select PIN');  
$result   = mysqli_fetch_array( $sql );
$vereniging_id = $result['Vereniging_id'];
$count         = $result['Aantal'];
}

if ($count == 0){
	$message .= "* Licentie combinatie PIN is niet correct.<br>";
	$error = 1;
} else {
   $vereniging_id = $result['Vereniging_id']	;
   
   	$update_query= "UPDATE `namen` 
	              SET Aangelogd    = 'N',
				      IP_adres     = ' '
			    where IP_adres     = '".$ip."' " ; 
   
    mysqli_query($con,$update_query) or die ('Fout in update namen' );  
	
	$update_query = "UPDATE `namen` 
	              SET Aangelogd    = 'J',
				      IP_adres     = '".$ip."',
					  Laatst       = now() 
				  where Licentie = '".$licentie."' and PIN_encrypt = '".$pin_encrypt."' "; 
		//echo "<br>".$update_query;
    mysqli_query($con,$update_query) or die ('Fout in update namen' );  
	
	 
 	
}


/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1


// in script 'aanloggen.php' staat nu target = _top'  dit zorgt voor het openen in hetzelfde window !!!!!
if ($url ==''){
 $url = "main.php?ver_id=".$vereniging_id."&kal_on";
}
if ($error == 0){
  redirect($url);
}
ob_end_flush();
?>
