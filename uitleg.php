<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">


<style type="text/css">

input[type=checkbox] {
  transform: scale(0.8);
}
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 15px;
       width: 15px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}
.uitleg {
  font-size: 1vh;
  font-family:verdana;
}
 
	</style>
</head>

<body>
<?php
include('conf/mysqli.php'); 
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$licentie       = $result0['Licentie'];

?>
<div class="container"> 
  
  <div class= "row  align-content-right">
    <div class="col-12 text-center fixed-top">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php"   ></a>
     <a class=" btn btn-secondary btn-lg  "  href ='#top' class='back'>TOP</a>  
  </div> <!-- col-->    
  </div>  <!--- row---->    

<div class= "container" >

<h3 style='color:teal;'>Uitleg</h3>

<div class='row'>
  <div class = 'col-12'>
  
 <div class="card"  >
 <div class="card-header">
    Inleiding
  </div>
  <div class="card-body">
    <h5 class="card-title"> </h5>
    <p class="card-text text-justify"><b>Dit programma is ontwikkeld ter ondersteuning van de Jeu de Boules verenigingen in de corona crisis.<br>
Het helpt om te gaan met de beperkende maatregelen m.b.t de 1.5m afstand en het maximaal aantal spelers op een sportpark.</b>
</p>
 </div>
 </div>
 </div>
 </div>
 
 <br>
 <div class='row'>
  <div class = 'col-12'>
  
 <div class="card"  >
 <div class="card-header">
    Help functies (*)
  </div>
  <div class="card-body">
    <h5 class="card-title"> </h5>
    <p class="card-text text-justify">Op diverse plekken in het scherm vind je knoppen om je wat uitleg te geven. De tekst op een knop begint dan met een *.
</p>
 </div>
 </div>
 </div>
 </div>
 
 
 
 <br>
 <div class='row'>
  <div class = 'col-12'>
  
 <div class="card"  >
 <div class="card-header">
    Beknopte instructie
  </div>
  <div class="card-body">
    <h5 class="card-title"><b>Kalender</b></h5>
    <p class="card-text text-justify">Het hoofdscherm bevat een kalender en een tabel voor de reserveringen. Deze laatste is overigens pas zichtbaar nadat in de kalender een datum is geselecteerd.<br>
	Je kan een datum selecteren door simpel op de datum te klikken die je wenst. De beheerder van de vereniging kan in een apart scherm aangeven welke dagen van de week de vereniging geopend is. De dagen waarop de vereniging gesloten is hebben een donkergrijze achtergrond en kunnen dus niet geselecteer worden. Ook de dagen  in het verleden of eventuele feestdagen kunnen niet geselecteerd worden.<br>
    Via de kalender knop bovenaan het scherm kan de kalender <b>aan/uit</b> gezet worden. Bij invoer van reserveringen wordt de kalender automatisch uitgeschakeld.
	<br>
	Met behulp van de blauwe knoppen boven de kalender kan je een maand terug of verder  bladeren. Als je tussen de blauwe knoppen in klikt ga je terug naar de huidige maand.
    <br></p>
	<hr>
	 <h5 class="card-title"><b>Reserveren</b></h5>
	 <br>
	  <p class="card-text text-justify">Nadat een datum gekozen is, wordt de tabel voor de reseveringen zichtbaar.<br>
       De beheerder van de vereniging heeft per dag aangegeven van hoe laat tot hoe laat er gespeeld kan worden. En aangeven hoeveel minuten je een baan kan reserveren.<br>
       Aan de hand van deze gegevens heeft het programma de speelmomenten bepaald.<br>
	   Ook heeft de beheerder aangegeven hoeveel banen er beschikbaar zijn en wat het maximum aantal spelers is dat aanwezig mag zijn op het sportpark.<br>
	   Door een vinkje te zetten in de het vakje dat overeenkomt met de keuze van het tijdstip en de baan kan je een baan reserveren.<br><br>
	   Per baan (en tijdstip) kunnen er maximaal 4 personen reserveren. Als er al een persoon een reservering heeft aangemaakt voor een baan, dan zie je dat middels het oranje icoontje met de kwart cirkel en de 1. Zijn er 2 personen dan zie je een halve cirkel met een 2 en bij 3 personen een driekwart cirkel met een 3.
	   Je kan dan nog steeds een reservering maken. Bij 4 reserveringen komt er een kruis te staan en is de baan vol.<br>
	   Er wordt een controle uitgevoerd of je voor dat tijdstip al een reservering hebt aangemaakt op een andere baan of dat het maximaal aantal personen op het sportpark voor dat tijdstip is bereikt. Dat wordt bijgehouden door een teller kin de kolom (#/Res). Het eerste getal geeft het aantal reserveringen aan en het tweede getal het maximum.
	   
 <br></p>
	<hr>
	 <h5 class="card-title"><b>Reserveringen bekijken of verwijderen</b></h5>
	 <br>
	  <p class="card-text text-justify">Boven aan het scherm zie je een knop met een kopje. Als je hierop klikt krijg je jouw reserveringen voor de geselecteerde datum.
	   
	   Je kan hier ook zien welke andere spelers voor dezelfde baan en tijdstip hebben gereserveerd. Via een vinkje in  het vakje voor de tijd, kan je een reservering selecteren om te verwijderen. Klik je vervolgens op de knop onderaan, dan wordt de reservering verwijderd.
	   
	    	   
    <br></p>
		<hr>
	 <h5 class="card-title"><b>PIN code wijzigen</b></h5>
	 <br>
	  <p class="card-text text-justify">Op het aanlogscherm zie je een knop 'PIN code wijzigen'. Als je hierop klikt krijg je de mogelijkheid je PIN code te wijzigen. Je ontvangt een emailbericht naar het email adres die bij je licentie is geregistreerd. 
          Hierin staat dan een link om de PIN code wijziging te activeren.
 	   
    <br></p>
	<hr>
	 <h5 class="card-title"><b>Toevoegen als App op je smartphone</b></h5>
	 <br>
	  <p class="card-text text-justify">Open de OBER webpagina in je smartphone. Klik op de opties (3 verticale puntjes bovenaan scherm) en Klik op toevoegen aan startscherm.
 	    
    <br></p>
</div>
    
  </div>
 <br> 
  <div class="card"  >
 <div class="card-header">
    Beheerder
  </div>
  <div class="card-body">
    <h5 class="card-title"> </h5>
    <p class="card-text text-justify">Een gebruiker van dit programma kan speciale rechten krijgen om zaken voor de vereniging te kunnen instellen en een reserveringslijst per datum uit te draaien.
	  Via de knop met wieltje bovenaan het scherm, kunnen de instellingen voor de vereniging aangepast worden. Hierbij  zijn er de volgende onderdelen:
	  <ul>
	   <li> Vereniging specifieke instellingen
	   <li> Beheerders rechten toekennen of intrekken
	   <li> Openingstijden
	   <li> Feestdagen kalender
	 </ul>
	Na selectie van een datum in de kalender kan de beheerder via de tweede knop van links een lijst tonen met de reserveringen voor de geselecteerde datum.
</div></p>
    
  </div>
</div>
 </div>
 <br>
 
 
</div>
 </div>
  

 </div>  <!---- container---->
 
 
 <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

 </body>
 </html>
 