<?php
# verwerk_aanlog.php
# Kontrole van wchtwoord op aanloggen.php
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
#
# 29dec2018          1.0.1            E. Hendrikx
# Symptom:   		    None.
# Problem:       	  Onbekende var key
# Fix:              Opgelost
# Feature:          None.
# Reference: 
#


ob_start();
include 'conf/mysqli.php'; 
//include 'versleutel.php'; 

function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}
$count =0 ;

$naam          = $_POST['naam']; 
$wachtwoord    = $_POST['secureontip']; 
$vereniging_id = $_POST['vereniging_id'];

include('action.php');

$encrypt = md5($wachtwoord);
$ip      = md5($_SERVER['REMOTE_ADDR']);

$sql      = mysqli_query($con,"SELECT count(*) as Aantal FROM namen WHERE  Naam ='".$naam."' and Vereniging_id = ".$vereniging_id." and Wachtwoord_encrypt ='".$encrypt."' ") or die('Aanloggen: Fout in select');  
$result   = mysqli_fetch_array( $sql );
$count    = $result['Aantal'];

if ($count == 1){

mysqli_query($con,"Update namen set Laatst = now(),
                              Aangelogd = 'J',
                              Wachtwoord = '[versleuteld]',
                              Wachtwoord_encrypt = '".$encrypt."' ,
                              IP_adres = '". $ip."' ,
							  Laatst  = now()
	                    WHERE Naam  ='".$naam ."' and Vereniging_id = ".$vereniging_id."  ");

}
else {
$message = "Het aanloggen is niet goed gegaan. Mogelijk redenen :<br>";
$message .= "- Verkeerde usernaam of wachtwoord<br>";

$error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert

?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
}  // count = 0



// in script 'aanloggen.php' staat nu target = _top'  dit zorgt voor het openen in hetzelfde window !!!!!
if ($url ==''){
 $url = "index.php?ver_id=".$vereniging_id;
}
redirect($url);
ob_end_flush();
?>
