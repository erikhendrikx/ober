<?php
# beheer_config.php
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 16okt2018          1.0.1           E. Hendrikx
# Symptom:   		 None.
# Problem:       	 None.
# Fix:               None.
# Feature:           None.
# Reference: 
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

 
  
<link href="css/bootstrap.min.css" rel="stylesheet">

<style type="text/css">
 .noprint {display:none;}     
	 

.back { color:white;text-decoration:none;}
.container a:hover { color:yellow;text-decoration:none ;}


h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	
 .funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}
  
.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}


	</style>
</head>

<body data-spy="scroll" data-target = '#navbar' >
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
include ('../ontip/versleutel_string.php'); // tbv telnr en email
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}

$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT * FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$licentie       = $result0['Licentie'];
$vereniging_id  = $result0['Vereniging_id'];
$admin_extra    = $result0['Admin_extra'];

if ($result0['Admin_user'] =='N' ){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;

}
  
if ($admin_extra =='N'){
	$disable_option_extra = 'disabled';
} else {
	$disable_option_extra = '';
}
 
$qry            = mysqli_query($con,"SELECT * From vereniging where Id = ".$vereniging_id." ")	or die(' Fout in select vereniging');  
$result2        = mysqli_fetch_array( $qry );
$vereniging     = $result2['Vereniging'];

 $qry                       = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id." ")	or die(' Fout in select vereniging cfg');  
 $result                    = mysqli_fetch_array( $qry );
 $interval_min              = $result['Maximale_speelduur'];
 $aantal_banen              = $result['Aantal_banen'];
 $max_aantal_splrs          = $result['Max_aantal_spelers'];
 $max_aantal_splrs_per_baan = $result['Max_aantal_spelers_per_baan'];
 $max_freq_per_dag          = $result['Max_freq_per_dag'];
 $max_freq_per_week         = $result['Max_freq_per_week'];
 $total_cols                = $aantal_banen+2;
 $vereniging_id             = $vereniging_id;
   
 $aantal_banen_valid = '';
	 if ( $aantal_banen  < 1   ){
		 $aantal_banen_valid     = 'is-invalid';
		 $aantal_banen_feedback  = "Aantal banen moet groter zijn dan 0 "; 
	    }
 $max_aantal_spelers_valid = '';
	 if ( $max_aantal_splrs  < 1   ){
		 $max_aantal_spelers_valid       = 'is-invalid';
		 $max_aantal_spelers_feedback    = "Max aantal spelers moet groter zijn dan 0 "; 
	    }
		
 $max_aantal_spelers_per_baan_valid = '';
	 if ( $max_aantal_splrs_per_baan  != 2 and   $max_aantal_splrs_per_baan != 4  ){
		 $max_aantal_spelers_per_baan_valid       = 'is-invalid';
		 $max_aantal_spelers_per_baan_feedback    = "Max aantal spelers per baan moet zijn 2 of 4 "; 
	    }
	
 $interval_min_valid = '';
	 if ( $interval_min  < 15  ){
		 $interval_min_valid     = 'is-invalid';
		 $interval_min_feedback  = "Max speelduur moet groter zijn dan 15 "; 
	    }
		
$max_freq_per_dag_valid  ='';
   if ($max_freq_per_dag < 1)	{
	   $max_freq_per_dag_valid  =   'is-invalid';
	   $max_freq_per_dag_feedback  = "Max aantal per dag moet groter zijn dan 0"; 
       }	

$max_freq_per_dag_valid  ='';
   if ($max_freq_per_dag > 10)	{
	   $max_freq_per_dag_valid  =   'is-invalid';
	   $max_freq_per_dag_feedback  = "Max aantal per dag moet kleiner zijn dan 10"; 
       }
   
$max_freq_per_week_valid  ='';
   if ($max_freq_per_week < 1)	{
	   $max_freq_per_week_valid  =   'is-invalid';
	   $max_freq_per_week_feedback  = "Max aantal per week moet groter zijn dan 0"; 
       }	

$max_freq_per_week_valid  ='';
   if ($max_freq_per_week <=  $max_freq_per_dag)	{
	   $max_freq_per_week_valid  =   'is-invalid';
	   $max_freq_per_week_feedback  = "Max aantal per week kan niet lager zijn dan per dag"; 
       }
   	
?>
<div class= 'container'> 
<div class= "row  align-content-right">
     <div class="col-12 text-center fixed-top">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>"   ></a>
     <a class=" btn btn-secondary btn-lg  "  href ='#top' class='back'>TOP</a>  
  </div> <!-- col-->    
  </div>  <!--- row---->    
  
<br>  
<br>
 
<h2> Beheer reserveringen systeem  voor <?php echo $vereniging;?></h2>
<br>
 
<div class= "row justify-content "  id ='navbar'>
	 <a  class="btn btn-primary btn-sm   mt-1                    "   href= '#general'                >Algemeen</a>
	 <a  class="btn btn-primary btn-sm   mt-1                    "   href= '#openings_tijden'        >Open</a>
	 <a  class="btn btn-primary btn-sm   mt-1                    "   href= '#feestdagen_kalender'    >Feestdagen</a>
	 <a  class="btn btn-primary btn-sm   mt-1                    "   href= '#afwijkende_baanummers'  >Baan 1-2-3</a>
	 <a  class="btn btn-primary btn-sm   mt-1   <?php echo $disable_option_extra;?> "   href= 'import_excel_bestanden_main.php'  >Import </a>

  </div>
<br>  
	 
<a href="#" class="btn btn-sm btn-dark" role="button" data-toggle="popover" title="Uitleg" data-html="true" data-trigger="hover" data-content="In dit scherm kunnen diverse zaken ingesteld worden. Hierboven vind je knoppen om snel naar een onderdeel te gaan.
	   Via de knop TOP bovenaan het scherm kan je direct terug naar het begin. Deze knop blijft trouwens altijd in beeld tijdens het scrollen. De knop Import is standaard uitgeschakeld. Indien je interesse hebt om reserveringen te importeren vanuit een Excel bestand, neem dan contact op met de beheerder Erik Hendrikx.
       <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Uitleg scherm</a>

	  

<h3 id= 'general' style='color:teal;'>Vereniging specifieke instellingen</h3>
 
  	 <a  class="btn btn-secondary btn-lg  "    data-toggle="tooltip" data-placement="bottom" title="Leden lijst"      href="lijst_gebruikers_vereniging.php?ver_id_md5=<?php echo md5($vereniging_id);?> "   >Leden lijst</a>
 
   <FORM action="update_config.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
   <div class="form-group row">
    <label for="inputEmail3" class="col-sm-4 col-form-label">Vereniging</label>
    <div class="col-sm-4"><b>
     <?php echo $vereniging;?>
	 </b>
    </div>
  </div> <!--- row--->
 <div class="form-group row">
    <label for="inputBanen" class="col-sm-4 col-form-label">Aantal beschikbare banen</label>
    <div class="col-sm-2">
      <input type="text" class="form-control <?php echo $aantal_banen_valid;?>" id="inputBanen" name ='aantal_banen' value = '<?php echo $aantal_banen;?>' /> 
    <div class="invalid-feedback">
        <?php echo $aantal_banen_feedback;?>
      </div> 
	  </div>
  </div> <!--- row--->
 <div class="form-group row">
    <label for="maxSplrs" class="col-sm-4 col-form-label">Maximaal aantal personen op speelveld</label>
     <div class="col-sm-2">
      <input type="text" class="form-control <?php echo $max_aantal_spelers_valid;?>" " id="maxSplrs" name ='max_aantal_splrs' value = '<?php echo $max_aantal_splrs;?>' /> 
      <div class="invalid-feedback">
        <?php echo $max_aantal_spelers_feedback;?>
      </div>   
   </div>
  </div>
  
  <div class="form-group row">
    <label for="maxFreqDag" class="col-sm-4 col-form-label">Maximaal aantal reserveringen per persoon</label>
     <div class="col-sm-2">Per dag
      <input type="text" class="form-control <?php echo $max_freq_per_dag_valid;?>" " id="maxFreqDag" name ='max_freq_per_dag' value = '<?php echo $max_freq_per_dag;?>' /> 
      <div class="invalid-feedback">
        <?php echo $max_freq_per_dag_feedback;?>
      </div>   
   </div>
   </div>
    <div class="form-group row">
     <label for="maxFreqWeek" class="col-sm-4 col-form-label"> </label>
     <div class="col-sm-2">Per week
      <input type="text" class="form-control <?php echo $max_freq_per_week_valid;?>" " id="maxFreqWeek" name ='max_freq_per_week' value = '<?php echo $max_freq_per_week;?>' /> 
      <div class="invalid-feedback">
        <?php echo $max_freq_per_week_feedback;?>
      </div>   
   </div>
  </div>
  
  
  
 <div class="form-group row">
    <label for="maxSplrs" class="col-sm-4 col-form-label">Maximaal aantal personen per baan<br><em>Klik op schakelaar 2/4</em></label>
     <div class="col-sm-2">
	 <?php
	 $option_checked ='';
	 if ($max_aantal_splrs_per_baan == 2 ) {
		   $option_checked ='checked';
	 } ?>
	  <div class='form-check'>
		 <label class ="form-check-label"> 
        <input type="checkbox" name ='max_aantal_splrs_per_baan'  class='form-check-input'  <?php echo $option_checked;?>  data-toggle="toggle" data-onstyle="success" data-offstyle="primary" data-on="2 pers" data-off="4 pers">
        </label>
	   </div ><!--- form check--->
	 </div> <!--- col--->
</div>  <!--- row--->

  <div class="form-group row">
    <label for="maxTijd" class="col-sm-4 col-form-label">Maximale duur van een speelperiode (minuten)</label>
     <div class="col-sm-2">
      <input type="text" class="form-control <?php echo $interval_min_valid;?>" " id="maxTijdSplrs"  name ='max_speelduur' value = '<?php echo $interval_min;?>' /> 
      <div class="invalid-feedback">
        <?php echo $interval_min_feedback;?>
      </div>    
  </div>  <!--- col--->
 </div>  <!-- form group-->
  
  <div class="form-group row">
    <label for="DagenOpen" class="col-sm-4 col-form-label">Dagen van de week geopend <br><em>(klik op de dag)</em></label>
      <div class="col-sm-7"  style= 'font-size:8pt;'>

	     <?php 
		 $dagen = array();
		 $dagen[] = 'zondag';
		 $dagen[] = 'maandag';
		 $dagen[] = 'dinsdag';
		 $dagen[] = 'woensdag';
		 $dagen[] = 'donderdag';
		 $dagen[] = 'vrijdag';
		 $dagen[] = 'zaterdag';
		 
		 $x=1;
		 foreach ($dagen as $dag){
           $var = $dag.'_open';	 
			 
	     $option_checked ='';
		 if ($result[ucfirst($var)] =='J'){
			 $option_checked ='checked';
		 }// end if
		 ?>
		    <div class='form-check'>
			 <label class ="form-check-label form-check-inline"> 
	      	 <input data-width="150" data-height="35" class='form-check-input'  type="checkbox" name='<?php echo $var;?>' <?php echo $option_checked;?>  data-toggle="toggle" data-onstyle="success" data-offstyle="light" 
		                    data-on="<?php echo $dag;?> open" data-off="<?php echo $dag;?> dicht"></label>
			</div>  <!-- form check-->
			 
		  <?php
		  }// end for
	
		 ?>
		
	</div> <!-- col-->
  </div> <!-- form group-->
	
		   <input type="submit" class="btn btn-info" value="Opslaan wijzigingen instellingen">
</form>
  
<br>
<h4 id= 'beheerders'  style='color:teal;' >Beheerders <?php echo $vereniging;?> </h4> 
<a href="# "  class='btn btn-sm btn-dark' role="button" data-toggle="popover" title="Uitleg" data-html="true" data-trigger="hover" data-content="De gebruikers hieronder hebben beheerrechten voor de vereniging. Bij het intrekken van de rechten worden alleen de rechten ingetrokken. De gebruiker blijft bestaan. Je kan je eigen admin rechten niet intrekken.
				 De admin rechten van de Super Admin kunnen door niemand ingetrokken worden
       <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Uitleg beheerders</a>
 
	<br>

  <?php 
$qry   = mysqli_query($con,"SELECT * From namen where Vereniging_id = ".$vereniging_id." and Admin_user != 'N' order by Naam ")	or die(' Fout in select admins');  
?>

   <FORM action="update_admin_users.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
     <table class="table table-striped table-hover" >
     <thead>
    <tr >
      <th width=2% ><img src ='images/trash_checked.png' width=25></th>
      <th  >Licentie</th>
      <th  >Naam</th>
	   <th >Super Admin</th>
     </tr>
	</thead>
	
	
	 <?php
   
     while($row = mysqli_fetch_array( $qry )) {
		 $disable_option = ''; 
		 if ($row['Licentie'] ==  $licentie or $row['Admin_user'] == 'X' ){
			 $disable_option = 'disabled';
		 }
	 	?>
	   <tr>
	    <td>        
		 <input type='checkbox' <?php echo $disable_option;?> name= 'Delete[]' class='trash'   id= 'trash_<?php echo $row['Id']; ?>' value='<?php echo $row['Id']; ?>' /><label for="trash_<?php echo $row['Id']; ?>"></label>	
       </td>    
	   
	      <td><?php echo $row['Licentie'];?></td>
	      <td><?php echo $row['Naam'];?></td>
		  <?php
		   if ($row['Admin_user'] == 'X'){ ?> 
			 <td>J</td>  
			 <?php
		   } else {   ?>
		   <td>-</td>
		   <?php
		   }
		  ?>
		  
	   </tr>
  
	 <?php } ?>
	 </table>
	 
   
 <input type="submit" class="btn btn-info" value="Intrekken admin rechten">
	   </form>
  
	  <h4  id='toevoegen_beheerder'  style='color:teal;'>Toevoegen beheerder</h4>

 
  <br>
  <FORM action="update_admin_users.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
 <div class="form-group row">
 
 
   <div class="col-6" >
      <label for="inputCity">Naam</label>
  
  <select id ="adminuser" name= 'adminuser'  class="form-control" aria-describedby="userHelp">
        <option value ='' selected>Kies...</option>
		<?php
	      $qry   = mysqli_query($con,"SELECT * From namen where Vereniging_id = ".$vereniging_id."  and Admin_user = 'N'  order by Naam ")	or die(' Fout in select admins');  
 		   while($row = mysqli_fetch_array( $qry )) {?>
			    <option value = "<?php echo $row['Id'];?>"  ><?php echo $row['Naam'];?></option>
        <?php
               }
		?>
      </select>
	   <small id="userHelp" class="form-text text-muted">Selecteer de naam uit de lijst </small>
	</div>  
   </div>
 <input type="submit" class="btn btn-info" value="Toekennen admin rechten aan geselecteerd(e) persoon of personen">
	   </form>
 
 <hr color='teal' style='height:3pt;'/>

	<h3 id= 'openings_tijden'  style='color:teal;' >Openingstijden </h3>
	
	<a href='#'   class='btn btn-sm btn-dark' role="button" data-toggle="popover" title="Uitleg" data-html="true" data-trigger="hover" 
	           data-content="Hieronder kan je per dag de openingstijden van de vereniging instellen. Via een vinkje in de eerste kolom kan je deze verwijderen. Bovenaan in het scherm bij de vereniging instellingen kan je aangeven welke dagen van de week de vereniging geopend is.
	   	    <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Uitleg openingstijden</a>



 
	<br>
<?php 
$qry   = mysqli_query($con,"SELECT * From baanres_speelmomenten where Vereniging_id = ".$vereniging_id." order by Dagnr_van_de_week, Begin_tijd ")	or die(' Fout in select vereniging cfg');  
?>
<br>
  <FORM action="update_speelmomenten.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
       <table class="table table-striped table-hover" >
        <thead>
         <tr >
          <th width=2% ><img src ='images/trash_checked.png' width=25></th>
          <th  >Dag van de week</th>
          <th  >Begin tijd</th>
          <th  >Eind tijd</th>
	    </tr>
	   </thead>
	
   
  <?php
     while($row = mysqli_fetch_array( $qry )) {
	
	   /// input checks
	 $begin_tijd_valid = '';
	 if ($row['Begin_tijd']==''  and $row['Dag_gesloten']  =='N' ){
		 $begin_tijd_valid     = 'is-invalid';
		 $begin_tijd_feedback  = "Begin tijd moet ingevuld zijn"; 
	    }
	
	 $eind_tijd_valid = '';
	 if ($row['Eind_tijd']==''  and $row['Dag_gesloten']  =='N' ){
		 $eind_tijd_valid = 'is-invalid';
		 $eind_tijd_feedback  = "Eind tijd moet ingevuld zijn"; 
	    }
		else {

	if ($row['Eind_tijd']==''  and $row['Dag_gesloten']  =='N'   and  $row['Eind_tijd'] < $row['Begin_tijd'] ){
		 $eind_tijd_valid = 'is-invalid';
		 $eind_tijd_feedback  = "Eind tijd kan niet eerder zijn dan begin"; 
	      }
		}
       ?>
	   <tr>
	    <td>        
		 <input type="checkbox" class='trash'  style='font-size:9pt;' id="trash_<?php echo $row['Id']; ?>"   value = <?php echo $row['Id'];?> name = "Delete[]" ><label for="trash_<?php echo $row['Id']; ?>"></label>
       </td> 
       <td>
      <?php echo $row['Dag_van_de_week'];?>
       </td>   
	   <td>
		  <input required type="text" class="form-control <?php echo $begin_tijd_valid;?>"  name = 'begin_tijd_<?php echo $row['Id']; ?>' placeholder="Begin tijd" <?php echo $option;?>  size =6 value= '<?php echo $row['Begin_tijd'];?>'>
           <div class="invalid-feedback"><?php echo $begin_tijd_feedback;?> </div>   
       </td>
	   <td>
	   <input required type="text" class="form-control <?php echo $eind_tijd_valid;?>" name = 'eind_tijd_<?php echo $row['Id']; ?>' placeholder="Eind tijd" <?php echo $option;?> size =6 value= '<?php echo $row['Eind_tijd'];?>'>
      <div class="invalid-feedback"><?php echo $eind_tijd_feedback;?></div>     
	   </td>
	   </tr>
  
	 <?php } ?>
	 </table>
	 <input type="submit" class="btn btn-info" value="Openingstijd verwijderen">
	 
 	 <hr>
	 
	  <h3  style='color:teal;'>Openingstijd toevoegen </h3>
	  
	  <a href="# "  class='btn btn-sm btn-dark' role="button" data-toggle="popover" title="Uitleg" data-html="true" data-trigger="hover" data-content="Hieronder kan je een openingstijd toevoegen. Let op ! Dit kan alleen voor de dagen dat de vereniging open is. Voor de andere dagen blokkeert de selectie.
       <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Uitleg toevoegen openingstijd</a>
	   
	  <br><br>
	<div class="form-group row">
	 
	<div class="col-4">   
      <label for="exampleFormControlSelect1">Dag van de week</label>
    <select class="form-control " id="exampleFormControlSelect1" aria-describedby="selectHelp"   name="newday">
	 <option value ='' select>Kies dag...</option> 
	  <?php
	  setlocale(LC_TIME, 'nl_NL');
	   for ($d=1;$d<8;$d++){

           $disabled_option = '';
           // controle of de dag geopend is
		   $dag = ucfirst(strftime('%A', strtotime("Sunday +{$d} days")));
		   $var = $dag."_open";
		   
           $qry   = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id."  and ".$var." ='J' ")	or die(' Fout in select vereniging cfg');  
           $result = mysqli_fetch_array( $qry );
           
		   if ($result[$var] !='J' ){
			    $disabled_option = 'disabled';
		   }// end if
		   ?>
		   <option value = <?php echo $d;?> <?php echo $disabled_option;?>     ><?php echo ucfirst(strftime('%A', strtotime("Sunday +{$d} days")));?></option>
		   <?php
		   
	   } // end for
     ?>
    </select>
	  <small id="selectHelp" class="form-text text-muted">Maak een keuze uit de lijst</small> 
    </div>
	<div class="col-1"> 
	</div>
  <div class="col-3"> van 
      <input type="text" class="form-control" name = "begin_tijd_new" <?php echo $option;?> size=4   placeholder = "begintijd : uu:mm" >
    </div>
    <div class="col-3"> tot
      <input type="text" class="form-control" name = "eind_tijd_new" <?php echo $option;?> size =4 placeholder = "eindtijd : uu:mm"  >
    </div>
	
	</div> <!--- row-->
	   <input type="submit" class="btn btn-info" value="Openingstijd toevoegen">
	   </form>
	   
<hr color='teal' style='height:3pt;'/>

 <h3 id= 'feestdagen_kalender' style='color:teal;'>Feestdagen kalender </h3>


   <FORM action="update_feestdagen.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
	<?php 
     $qry   = mysqli_query($con,"SELECT * From baanres_feestdagen where Vereniging_id = ".$vereniging_id." order by Datum  ")	or die(' Fout in select vereniging cfg');  
     ?>
<br>
  <FORM action="update_feestdagen.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
     <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th width=2% ><img src ='images/trash_checked.png' width=25></th>
      <th  >Datum</th>
      <th  >Omschrijving</th>
     </tr>
	</thead>
	
  <?php
     while($row = mysqli_fetch_array( $qry )) {
	?>
	   <tr>
	    <td>        
		 <input type='checkbox' name= 'delete[]' class='trash'   id= 'trash_<?php echo $row['Id']; ?>' value='<?php echo $row['Id']; ?>' /><label for="trash_<?php echo $row['Id']; ?>"></label>	
       </td>    
	   
	      <td><?php echo $row['Datum'];?></td>
	      <td><?php echo $row['Omschrijving'];?></td>
	   </tr>
  
	 <?php } ?>
	 </table>
	 
	  	   <input type="submit" class="btn btn-info" value="Verwijder geselecteerde feestdag(en)">
  </form>
 
	 <hr>
	 <h3 id='feestdagen_toevoegen' style='color:teal;'>Feestdagen toevoegen </h3>
    
	 <FORM action="insert_feestdag.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
	<div class="form-group row">
	  
	
	  <div class="col-3">   
       <div class="form-group">
          <label for="pin">Datum</label>
          <input  type="text" name = 'newdate'  class="form-control" id="newdate" aria-describedby="dateHelp" placeholder="Datum feestdag">
  	     <small id="dateHelp" class="form-text text-muted">jjjj-mm-dd  (2020-01-17)</small> 
       </div>
	  </div> <!--- col---->
	  <div class="col-1">
	  </div>
	  
	  <div class="col-6">   
       <div class="form-group">
          <label for="pin">Omschrijving</label>
          <input  type="text" name = 'omschrijving'  class="form-control" id="newdate" aria-describedby="omsHelp" placeholder="Omschrijving feestdag">
  	     <small id="omsHelp" class="form-text text-muted">Niet verplicht</small> 
       </div>
	  </div> <!--- col---->
	  	</div> <!--- row-->

	   <input type="submit" class="btn btn-info" value="Toevoegen feestdag">
   </form>
 <hr color='teal' style='height:3pt;'/>

  <h3 id= 'afwijkende_baanummers' style='color:teal;'>Afwijkende baan nummers </h3>
<a href="#" class="btn btn-sm btn-dark" role="button" data-toggle="popover" title="Uitleg" data-html="true" data-trigger="hover" data-content="Indien de banen niet genummerd worden van 1,2,3.. e.v  kunnen hieronder de afwijkende nummers vermeld worden , bijv 1,4,7,....
       <b>LET OP !</b> Er wordt niet gekontroleerd of een nummer vaker gebruikt wordt. <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Uitleg afwijkende baan nummers</a>

   <br>
   
   <br>
  
  <FORM action="update_baannummers.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
	    <input type="hidden" name="aantal_banen" value="<?php echo $aantal_banen;?>" /> 
	 
        <div class="custom-control custom-checkbox checkbox-lg">
              <input type="checkbox" class="custom-control-input" name = 'reset' id="defaultUnchecked">
              <label class="custom-control-label" for="defaultUnchecked">Reset nummers (1.. <?php echo $aantal_banen;?>) (reserveringen voor bepaalde baan nummers blijven bestaan !!)</label>
        </div>
   <br>
     <table class="table table-striped table-hover" >
        <thead>
         <tr >
          <th  >Baan</th>
          <th  >Afwijkend nr</th>
        </tr>
       </thead>
 
 	    <?php 
		for ($i=1;$i<=$aantal_banen;$i++){
			$qry    = mysqli_query($con,"SELECT * From baanres_baan_nummers where Vereniging_id = ".$vereniging_id." and Baan = ".$i."  order by Baan  ")	or die(' Fout in select baan afw');  
            $result = mysqli_fetch_array( $qry );
			$baan_afw_nr = $result['Baan_afw_nr'];
			?>
		 <tr>
           <td>	Baan <?php echo $i;?></td>
		   <td>
              <input type="text" class="form-control <?php echo $banen_valid;?>" id="inputBanen" placeholder= 'Type afwijkend baan nummer' size = 3 name ='baan_<?php echo $i;?>' value = '<?php echo $baan_afw_nr;?>' /> 
               
		  </td>
           </tr> <!--- row--->
			<?php
		}
			?>
	</table>
	
	
	<input type="submit" class="btn btn-info" value="Aanpassen baan nummers">
	</form>
<hr color='teal' style='height:3pt;'/>

 </div>  <!---- container---->
 
<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})

 
$(document).ready(function(){
  $('.toast').toast('show');
});
 

</script>

 </body>
 </html>
 