<?php
# import_leden_xlsx_stap3.php
 
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 22mar2020         0.0.1       E. Hendrikx 
# Symptom:	        None.
# Problem:     	    None
# Fix:              None
# Feature:          PHPSpreadsheet
# Reference: 

// ivm include in import_inschrijvingen_xlsx_stap2.php niet actief
//include('conf/mysqli.php');
setlocale(LC_ALL, 'nl_NL');
 
$sql        = mysqli_query($con,"SELECT * From vereniging  where Id = ".$vereniging_id." ")     or die(' Fout in select ver');  	
$result     = mysqli_fetch_array( $sql );
$vereniging = $result['Vereniging'];


require '../ontip/PHPspreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// via include in import_inschrijvingen_xlsx_stap2.php
$inputFileName   =  'xlsx/leden_4_200601035022.xlsx';
$inputFileName   = $xlsx_file;
$spreadsheet     = IOFactory::load($inputFileName);

$worksheet = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow         = $worksheet->getHighestRow(); // e.g. 10
$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

$kop     = $spreadsheet->getActiveSheet()->getCell('A1')->getValue();

if ($kop !='Leden'){
	echo "Dit bestand komt niet overeen met de template."
	exit;
}

for ($i=3;$i < 1000;$i++){

       $volgnr      = $spreadsheet->getActiveSheet()->getCell('A'.$i)->getValue();
 
if ($volgnr ==''){
	$i=1000;
	
} else {
	
// Get the value from cells
       	$licentie    = $spreadsheet->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
      	$naam        = $spreadsheet->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
        $pin         = $spreadsheet->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
     	if ($pin !=''){
			$pin_encrypt = md5($pin);
		} else {
		    $pin_encrypt ='';
		}
   		
		// check op geldige waarden
         if ($volgnr !='Nr' and $naam !='' ){
           	
         $j++;  	
         $insert_query = "INSERT INTO `namen` (`Id`, `Vereniging_id`, `Vereniging`, `Licentie`, `Naam`,PIN_encrypt, `Laatst`) 
	                              VALUES (NULL, ".$vereniging_id.", '".$vereniging."','".$licentie."','".$naam."','".$pin_encrypt."', now() )";
 //        echo "<br>". $insert_query;
         mysqli_query($con,$insert_query) or die ('Fout in insert :'.$insert_query);   
		 } //end if
		 
    } // end if
 } // end for

	?>
  <script language="javascript">
        alert("Er zijn <?php echo $j;?> inschrijvingen geimporteerd ")
    </script>
<br>
</fieldset>
 
