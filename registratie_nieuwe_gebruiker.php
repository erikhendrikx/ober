<?php

# verwerk_aanlog_pin.php
# Kontrole van wchtwoord op aanloggen.php
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
#
# 29dec2018          1.0.1            E. Hendrikx
# Symptom:   		    None.
# Problem:       	  Onbekende var key
# Fix:              Opgelost
# Feature:          None.
# Reference: 
#
 

ob_start();
include 'conf/mysqli.php'; 
//include('action.php');
include ('../ontip/versleutel_string.php'); // tbv telnr en email

function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}
$count =0 ;

$email ='';
$naam               = $_POST['naam']; 
$email              = $_POST['email']; 
$licentie           = $_POST['licentie']; 
$pin1               = $_POST['pin1']; 
$pin2               = $_POST['pin2']; 
$vereniging_id      = $_POST['vereniging_id'];
$admin_user         = 'N'; 

if (isset($_POST['adminuser'])){
	$admin_user   = 'J'; 
	
}

// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

if ($naam ==''){
	$message .= "* Er is geen naam ingevuld.<br>";
	$error = 1;
}

if ($licentie ==''){
	$message .= "* Er is geen toegangscode ingevuld.<br>";
	$error = 1;
} else {
	if (strlen($licentie ) < 4 ){
     	$message .= "* Toegangscode moet uit meer dan 3 karakters bestaan.<br>";
	    $error = 1;
     }
     if (strlen($licentie ) > 10  ){
     	$message .= "* Toegangscode mag niet langer zijn dan 10 karakters.<br>";
	    $error = 1;
     }
}
 
if ($vereniging_id ==''){
	$message .= "* Er is geen vereniging geselecteerd.<br>";
	$error = 1;
}

if ($pin1 ==''){
	$message .= "* Er is geen eerste pin ingevuld.<br>";
	$error = 1;
}

if ($pin2==''){
	$message .= "* Er is geen tweede pin ingevuld.<br>";
	$error = 1;
}

if ($pin1 != $pin2 ){
	$message .= "* PIN1 en PIN2 zijn niet gelijk.<br>";
	$error = 1;
}

if (!is_numeric($pin1) ){
	$message .= "* PIN code is geen getal.<br>";
	$error = 1;
}

if (strlen($pin1) <> 4  ){
	$message .= "* PIN code moet uit 4 cijfers bestaan.<br>";
	$error = 1;
}



if ($error == 0 ){
	$sql        = mysqli_query($con,"SELECT * From vereniging  where Id = ".$vereniging_id."  ")     or die(' Fout in select ver'); 
    $result     = mysqli_fetch_array( $sql ) ;
    $vereniging = $result['Vereniging']	;
 
 
  $qry   = mysqli_query($con,"SELECT * From namen where  Licentie =  '".$licentie."'  ")	or die(' Fout in select namen');  
  $result = mysqli_fetch_array( $qry );
  if ($result['Id']!='' ){
  	$message .= "* Toegangscode ".$licentie." is al bekend in het systeem."; 
	$error = 1;
 }


 if (isset($_POST['adminuser'])){
   $qry            = mysqli_query($con,"SELECT count(*)  as Aantal from namen where Vereniging_id = ".$vereniging_id."  and Admin_user <> 'N' ")	or die(' Fout in admin namen');  
   $result         = mysqli_fetch_array( $qry );
   $aantal_admins  = $result['Aantal']	;  

   if ($aantal_admins > 0){
  	$message .= "* Er is al een admin voor deze vereniging.<br>";
	$error = 1;
      } else {
	$admin_user ='X';
  } // end if   
 } // admin
 
} // error 0
 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
 ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1
 
  if ($error == 0 ){
	  if ($email != ''){
	 	  $_email        = $email;
          $email_encrypt = versleutel_string('@##'.$email);	  
	      $email         = '[versleuteld]';
  } else {
	     $email = '[onbekend]';
		 $_email = '';
	     $email_encrypt ='';
    } 
	  
	  $pin_encrypt   = md5($pin1);
	  
      $insert_query = "INSERT INTO `namen` (`Id`, `Vereniging_id`, `Vereniging`, `Licentie`, `Naam`, `Email`, `Email_encrypt`, `PIN_encrypt`, `Admin_user`,`Laatst`) 
	                              VALUES (NULL, ".$vereniging_id.", '".$vereniging."','".$licentie."','".$naam."', '".$email."','".$email_encrypt."','".$pin_encrypt."','".$admin_user."',now() )";
 //echo "<br>". $insert_query;
 	  mysqli_query($con,$insert_query) or die ('Fout in insert ');   
	  
	  $qry   = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id." ")	or die(' Fout in select vereniging cfg');  
      $result = mysqli_fetch_array( $qry );
	  
      if ($result['Vereniging_id']=='' ){
		       $insert_query = "INSERT INTO baanres_config (`Id`, `Vereniging_id`,`Vereniging`, `Laatst`) 
	                              VALUES (NULL, ".$vereniging_id.",'".$vereniging."', now() )";
     //echo "<br>". $insert_query;
 	  mysqli_query($con,$insert_query) or die ('Fout in insert ');   
  
	  }
  
   }
 

if ($error ==0 and $_email !='' ){
	 
$subject = 'Registratie gebruiker OBER - '. $naam;
$to      = $_email;
$admin   = 'erik.hendrikx@gmail.com';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: OnTip OBER <registratie.ober@ontip.nl>' . "\r\n" .
            'Reply-To: OnTip OBER <erik.hendrikx@ontip.nl>'.  "\r\n" .
	        'Bcc: '. $admin . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
$headers  .= "\r\n";

$bericht = "<table>"   . "\r\n";
$bericht .= "<tr><td><img src= 'https://www.ontip.nl/baanreservering/images/ober.png' width=80>"   . "\r\n";
$bericht .= "<td style= 'font-family:verdana;font-size:14pt;color:blue;'>Bevestiging registratie OBER</b><br>
             <span style='font-size:9pt;'>Een programma voor reserveren van Jeu de boules banen tijdens de corona crisis</span></td></tr>" . "\r\n";

$bericht .= "</table>"   . "\r\n";
$bericht .= "<br><br><hr/>".   "\r\n";

$bericht .= "<h3><u>Registratie gebruiker baan registratie</u></h3>".   "\r\n";

$bericht .= "<table style= 'font-family:verdana;font-size:9pt;' >"   . "\r\n";
$bericht .= "<tr><td  width=200 >Naam</td><td>"             .  $naam     ."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Toegangscode </td><td>"    .  $licentie."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Vereniging </td><td>"      .  $vereniging."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Email     </td><td>"       .  $_email       ."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >PIN     </td><td>"         .  $pin1       ."</td></tr>".  "\r\n";

if (isset($_POST['adminuser'] )){
	$bericht .= "<tr><td  width=200 >Admin rechten toegekend          </td><td>"     .   $admin_user       ."</td></tr>".  "\r\n";
	$bericht .= "<tr><td  width=200 >Aantal admins voor ".$vereniging."</td><td>"     .  $aantal_admins    ."</td></tr>".  "\r\n";
}
$bericht .= "</table>"   . "\r\n";

$bericht .= "<br><div  width=200>U kunt nu aanloggen met bovenstaande lincentie en PIN via Https://www.ontip.nl/ober/</div>". "\r\n";
$bericht .= "<br><hr/>".   "\r\n";
$bericht .= "<br><span style='font-size:9pt;color:darkgrey;'>(c) Erik Hendrikx. Deze email is geautomatiseerd aangemaakt van het OBER programma</span>".   "\r\n";

mail($to, $subject, $bericht, $headers);

echo "<br><h3>Registratie is voltooid. Kontroleer email bericht naar ".$_email."</h3>";

}

if ($error ==0 and $_email =='' ){
    echo "<br><h6>Registratie is voltooid.</h6> ";
	?>
	<a href ='index.html' class="btn btn-primary"> Klik hier om terug gaan naar het aanlog scherm </a>
	<?php
}

?>
<br>


 
