<?php
# import_inschrijvingen_xlsx_stap3.php
# gestart als include in import_inschrijvingen_xlsx_stap2.php

# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 22mar2020         0.0.1       E. Hendrikx 
# Symptom:	        None.
# Problem:     	    None
# Fix:              None
# Feature:          PHPSpreadsheet
# Reference: 

// ivm include in import_inschrijvingen_xlsx_stap2.php niet actief
//include('conf/mysqli.php');
setlocale(LC_ALL, 'nl_NL');

require '../ontip/PHPspreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;

// echo "verwerk ".$xlsx_file;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ophalen toernooi gegevens
$qry2             = mysqli_query($con,"SELECT * From config where Vereniging = '".$vereniging ."' and Toernooi = '".$toernooi ."'  ")     or die(' Fout in select2');  

while($row = mysqli_fetch_array( $qry2 )) {
	 $var  = $row['Variabele'];
	 $$var = $row['Waarde'];
	}

//// Toevoegen aan hulpnaam ivm kontrole dubbel inschrijven
switch($soort_inschrijving){
 	   case 'single'  : $soort = 1; break;
 	   case 'doublet' : $soort = 2; break;
 	   case 'triplet' : $soort = 3; break; 
 	   case 'kwintet' : $soort = 5; break;
 	   case 'sextet'  : $soort = 6; break;
 	  }// end switch

$qry        = mysqli_query($con,"SELECT * From config  where Vereniging = '".$vereniging ."' and Toernooi = '".$toernooi ."' and Variabele = 'soort_inschrijving'  ") ;  
$result     = mysqli_fetch_array( $qry);
$inschrijf_methode   = $result['Parameters'];

if  ($inschrijf_methode == ''){
	   $inschrijf_methode = 'vast';
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// via include in import_inschrijvingen_xlsx_stap2.php
//$inputFileName =  'xlsx/import_'.$toernooi."_".$timest.'.xlsx';
$inputFileName   = $xlsx_file;
$spreadsheet     = IOFactory::load($inputFileName);

$worksheet = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow         = $worksheet->getHighestRow(); // e.g. 10
$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

for ($i=8;$i < 100;$i++){

// Get the value from cells
        $volgnr      = $spreadsheet->getActiveSheet()->getCell('A'.$i)->getValue();
      	$naam1       = $spreadsheet->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
       	$licentie1   = $spreadsheet->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
       	$vereniging1 = $spreadsheet->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
    	
       	$naam2       = $spreadsheet->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
       	$licentie2   = $spreadsheet->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
       	$vereniging2 = $spreadsheet->getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
       	
       	$naam3       = $spreadsheet->getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
       	$licentie3   = $spreadsheet->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
       	$vereniging3 = $spreadsheet->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
       	
       	$naam4       = $spreadsheet->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
       	$licentie4   = $spreadsheet->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
       	$vereniging4 = $spreadsheet->getActiveSheet()->getCell('M'.$i)->getCalculatedValue();
       	
       	$naam5       = $spreadsheet->getActiveSheet()->getCell('N'.$i)->getCalculatedValue();
       	$licentie5   = $spreadsheet->getActiveSheet()->getCell('O'.$i)->getCalculatedValue();
       	$vereniging5 = $spreadsheet->getActiveSheet()->getCell('P'.$i)->getCalculatedValue();
       	
       	$naam6       = $spreadsheet->getActiveSheet()->getCell('Q'.$i)->getCalculatedValue();
       	$licentie6   = $spreadsheet->getActiveSheet()->getCell('R'.$i)->getCalculatedValue();
       	$vereniging6 = $spreadsheet->getActiveSheet()->getCell('S'.$i)->getCalculatedValue();
  
        $email       = $spreadsheet->getActiveSheet()->getCell('T'.$i)->getCalculatedValue();
        $telefoon    = $spreadsheet->getActiveSheet()->getCell('U'.$i)->getCalculatedValue();  

        $status      =   'IM0';       // Via import
        $kenmerk     = 'IM'.date('dHis');
		
		// check op geldige waarden
         if ($volgnr !='Nr' and $naam1 !='' ){
           	
         $j++;  	
         $query = "INSERT INTO inschrijf(Id, Toernooi, Vereniging,Vereniging_id, Datum, Volgnummer,
                                         Naam1, Licentie1, Vereniging1, 
                                         Naam2, Licentie2, Vereniging2, 
                                         Naam3, Licentie3, Vereniging3, 
                                         Naam4, Licentie4, Vereniging4, 
                                         Naam5, Licentie5, Vereniging5, 
                                         Naam6, Licentie6, Vereniging6, 
                                         Email, Telefoon,Status, Kenmerk, Inschrijving)
                        VALUES (0,'".$toernooi."', '".$vereniging ."'  , ".$vereniging_id.", '".$datum."',".$volgnr.", 
                                  '".$naam1."'     ,'".$licentie1."'   , '".$vereniging1."' ,
                                  '".$naam2."'     ,'".$licentie2."'   , '".$vereniging2."' ,
                                  '".$naam3."'     ,'".$licentie3."'   , '".$vereniging3."' ,
                                  '".$naam4."'     ,'".$licentie4."'   , '".$vereniging4."' ,
                                  '".$naam5."'     ,'".$licentie5."'   , '".$vereniging5."' ,
                                  '".$naam6."'     ,'".$licentie6."'   , '".$vereniging6."' , 
                                  '".$email."'     ,'".$telefoon."'    , '".$status."'      , '".$kenmerk."'  ,now()  )";
     // echo "<br>".$query;
      mysqli_query($con,$query) or die ('Fout in insert inschrijving '); 
      
	  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  // voor het voorkomen van dubbele inschrijvingen
          
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam1."','".$vereniging1."',NOW() )";
    //      echo $insert;
         mysqli_query($con,$insert) ; 
          
          if ($naam2 !=''){
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam2."','".$vereniging2."',NOW() )";
    //      echo $insert;
         mysqli_query($con,$insert) ; 
          }
          
          if ($naam3 !=''){
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam3."','".$vereniging3."',NOW() )";
           mysqli_query($con,$insert) ; 
          }
          
          if ($naam4 !=''){
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam4."','".$vereniging4."',NOW() )";
           mysqli_query($con,$insert) ; 
          }
          
          if ($naam5 !=''){
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam5."','".$vereniging5."',NOW() )";
           mysqli_query($con,$insert) ; 
          }
          
          if ($naam6 !=''){
          $insert  = "insert into hulp_naam (Id,Toernooi, Vereniging, Datum, Soort_toernooi,Naam , Vereniging_speler,Laatst) 
                      VALUES (0,'".$toernooi."', '".$vereniging ."' , '".$datum."',".$soort." ,'".$naam6."','".$vereniging6."',NOW() )";
           mysqli_query($con,$insert) ; 
          }    
   
		 } // end if
      else {
      //afbreken lezen bij lege regel
    	  $i =101;
	    }// end if
	

 }// end for i
 if ($error ==0 ) { 
	
 // bepaal aantal na import	
	$qry      = mysqli_query($con,"SELECT count(*) as Aantal From inschrijf where Vereniging = '".$vereniging ."' and Toernooi = '".$toernooi ."'  ")     or die(' Fout in select3 ');  
    $result   = mysqli_fetch_array( $qry );
    $aantal   = $result['Aantal'];
  
  if ($inschrijf_methode =='vast'){
  	$inschrijf_methode  ='team';
  } else {
  	$inschrijf_methode = 'melee';
  }
  ?>
	

<div style ='font-size:9pt;font-family:verdana;position:relative:top:-25pt;'>
	<a href = 'index1.php' target = '_top' style ='font-size:9pt;font-family:verdana;text-decoration:none;'>Hoofdmenu</a> /
	<a href = 'index3.php' target = '_top' style ='font-size:9pt;font-family:verdana;text-decoration:none;'>Inschrijvingen</a> /
	  Importeer inschrijvingen
</div>
<br>
<div style = 'font-family:verdana;font-size:10pt;margin-left:35pt;margin-right:35pt;padding:5pt;justify:right; background-color:white;border:2px solid #16B0F8;box-shadow: 8px 8px 8px #888888;border-radius:15px;'>   
	

<h1>Toernooi gegevens import</h1>
  
 	<br>
					<ul>
					<table width=90%> 
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Toernooi    :</td><td style='font-size:10pt;font-family:verdana;'><?php echo $toernooi;?></td></tr>
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Toernooi voluit   :</td><td style='font-size:10pt;font-family:verdana;'><?php echo $toernooi_voluit;?></td></tr>
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Toernooi datum   :</td><td style='font-size:10pt;font-family:verdana;'><?php echo strftime("%A %e %B %Y", mktime(0, 0, 0, $maand , $dag, $jaar) );?></td></tr>
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Soort toernooi    :</td><td style='font-size:10pt;font-family:verdana;'><?php echo $soort_inschrijving;?></td></tr>
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Inschrijf methode    :</td><td style='font-size:10pt;font-family:verdana;'><?php echo $inschrijf_methode;?></td></tr>
						<tr><td style='font-size:10pt;font-family:verdana;'><li> Aantal inschrijvingen na import  :</td><td style='font-size:10pt;font-family:verdana;'><?php echo $aantal;?></td></tr>
			     </table>
			    </ul>
	
 <br>
 <br>Sorteer de inschrijvingen in Muteer inschrijvingen.<br> Klik <a style= 'color:black;font-weight:bold;' href= 'beheer_inschrijvingen_stap1.php?toernooi=<?php echo $toernooi;?>' target='main'>hier</a> om naar 'muteren inschrijvingen' te gaan en klik op 'Originele sortering'.
<br>

	
  <script language="javascript">
        alert("Er zijn <?php echo $j;?> inschrijvingen geimporteerd ")
    </script>
<br>
</fieldset>
<?php
}// end if error 




if ($error !=0  ) { 	
	?>
	 <script language="javascript">
        alert("Er zijn geen goedgekeurde inschrijvingen gevonden om te importeren")
    </script>
   <form action="import_inschrijvingen_xlsx_stap1.php?toernooi=<?php echo $toernooi; ?>" method="POST" enctype="multipart/form-data"> 
  <input type='submit'  value ='Klik om door te gaan' name= 'input'>  
</form>
  <script type="text/javascript">
		window.location.replace('import_inschrijvingen_xlsx_stap1.php?toernooi=<?php echo $toernooi; ?>');
	</script>
<?php	
}

	


?>

