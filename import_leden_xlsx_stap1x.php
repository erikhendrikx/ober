<?php
# import_leden_xlsx_stap1.php

 
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#

?><html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="//bootstrap-extension.com/css/4.5.1/bootstrap-extension.min.css" type="text/css">
<script src="//bootstrap-extension.com/js/4.5.1/bootstrap-extension.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OnTip Baanreservering</title>
</head>
<body>
<br>
  
  <div id='top' class="container"  > 
    <div  class="card border border-success"  id ='kalender' style='box-shadow: 3px 3px 2px #888888;border-spacing: 4px 3px;'  >
  <div class="card-header">
     <div class = 'row  '>
     <strong><h4>Import leden bestand</h4></strong> 
	</div>
	</div><!---- header---->
	 
	 <div class="card-body">
           
        <form action="import_leden_xlsx_stap2.php" method="POST" id='form-id' enctype="multipart/form-data">
          <input type="hidden" name="ftp_server" value="81.26.219.37">
          <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>">
          <input type="hidden" name="zendform" value="-1">

  
        <div class="container mt-5">
          
          <div class="col-sm-4 mr-auto ml-auto border p-4">
          <form method="post" enctype="multipart/form-data" action="upload.php">
            <div class="form-group">
              <label><strong>Upload leden bestand xlsx</strong></label>
              <div class="custom-file">
                <input type="file" name="userfile" multiple class="custom-file-input form-control" id="customFile">
                <label class="custom-file-label" for="customFile">Kies bestand</label>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" name="upload" value="upload" id="upload" class="btn btn-block btn-dark"><i class="fa fa-fw fa-upload"></i> Upload</button>
            </div>
          </form>
        </div>


</div> <!--- body--->
 </div> <!---ccard--->





 </div> <!--- row--->
 <script>
 $(document).ready(function() {
  $('input[type="file"]').on("change", function() {
    let filenames = [];
    let files = document.getElementById("customFile").files;
    if (files.length > 1) {
      filenames.push("Total Files (" + files.length + ")");
    } else {
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          filenames.push(files[i].name);
        }
      }
    }
    $(this)
      .next(".custom-file-label")
      .html(filenames.join(","));
  });
});
</script>
</body>
</html>
