<?php
# main.php
# hoofdscherm OBER
 
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#
# 5juni2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:         select licentie vervangen door datalist (input & select)
# Reference: 

# 23juni2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:           Speelmomenten niet in juiste volgorde
# Fix:               Order op Begin_tijd
# Feature:          
# Reference: 

# 13okt2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:            
# Fix:               Typefout speeldveld
# Feature:          
# Reference: 


?><html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="//bootstrap-extension.com/css/4.5.1/bootstrap-extension.min.css" type="text/css">
<script src="//bootstrap-extension.com/js/4.5.1/bootstrap-extension.min.js"></script>
 
 <!-- ivm datalist--->
 <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
<link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">

<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OnTip Baanreservering</title>

<style type="text/css">

td, th {padding:5pt;}

 input.baan0[type=checkbox] {
    display:none;
  }
  
 input.baan1[type=checkbox] {
    display:none;
  }
 input.baan2[type=checkbox] {
    display:none;
  }
 input.baan3[type=checkbox] {
    display:none;
  }
 input.baan0[type=checkbox] {
    display:none;
  }
 input.baan[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }

 input.baan0[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
 input.baan1[type=checkbox] + label
   {
	   background:url('images/icon1.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
 
 input.baan2[type=checkbox] + label
   {
	   background:url('images/icon2.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
  
input.baan3[type=checkbox] + label
   {
	   background:url('images/icon3.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }  

 
input.baan0[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan1[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan2[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan3[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

.thick_line_top     {border-top:4pt solid black;}
.thick_line_bottom  {border-bottom:4pt solid black;}


h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 2.0vh;
}

h5{
  font-size: 1.6vh;
}

h6{
  font-size: 1.2vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 
smbut {
  font-size: 1.2vh;
}

.table a
{
    display:block;
    text-decoration:none;
}
 .tooltip-inner {
    width: 450px;
}
   .popover{
        max-width:900px;
    }
	

</style>
<script src="jquery.disable-autofill.js"></script>
<script src="../ontip/js/utility.js"></script>
<script src="../ontip/js/popup.js"></script>

<script language="JavaScript">
function changeDatum(id, datum) {
    // document.getElementById(id).innerHTML = datum;
    document.getElementById(id).value = datum;
    document.getElementById(id).style.color= "red";
   }
   
function ShowHide(id) {
    var x = document.getElementById(id);
    var y = document.getElementById('switch');
   
   
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
       
    }
}
</script>
</head>

<body >
<?php
include('conf/mysqli.php');
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');
$pageName = basename($_SERVER['SCRIPT_NAME']);

$aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}

$_datum ='..';
function Addtime($currentTime, $minToAdd, $maxTime){
    	
    //The number of hours that you want
    //to Add from the date and time.
	
	$part        = explode (":", $begin_tijd);
	//$currentTime = mktime($part[0], $part[1],$part[2]);
	
	 //Convert those hours into seconds so
    //that we can Add them from our timestamp.
    $timeToAdd = ($minToAdd * 60);
 
     //Add the interval to Unix timestamp.
    $newTime = $currentTime +$timeToAdd;
	if ($newTime >= $maxTime){
		 $newTime = $maxTime;
	}
	return   $newTime;
     }
 
 if (!isset($_GET['datum']) or $_GET['datum'] == '' ){
  $datum = date('Y-m-d');
 }

if (isset($_GET['datum']) and $_GET['datum'] !='' ){
	//  2020-05-18
    $datum  = $_GET['datum'];
}

// via aanlog_check_pin
//$ver_id = $_GET['ver_id'];

$part           = explode("-", $datum);
$_datum         = strftime("%A %e %B %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
$datum_dag_naam = ucfirst(strftime("%A", mktime(0,0,0,$part[1],$part[2],$part[0]))  );
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$licentie       = $result0['Licentie'];


if ($_GET['licentie']){
 $licentie = $_GET['licentie'];
}

$qry   = mysqli_query($con,"SELECT * From namen where Vereniging_id = ".$vereniging_id." and Licentie ='".$licentie."'  ")	or die(' Fout in select vereniging');  
  $result = mysqli_fetch_array( $qry );
  $naam_licentie = $result['Naam'];

  $sql          = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id."  ")     or die(' Fout in select config');  	
   $result                    = mysqli_fetch_array( $sql) ;
   $interval_min              = $result['Maximale_speelduur'];
   $aantal_banen              = $result['Aantal_banen'];
   $max_aantal_splrs          = $result['Max_aantal_spelers'];
   $max_aantal_splrs_per_baan = $result['Max_aantal_spelers_per_baan'];
   $max_freq_per_dag          = $result['Max_freq_per_dag'];
   $max_freq_per_week         = $result['Max_freq_per_week'];
   $total_cols                = $aantal_banen+2;
    

$sql        = mysqli_query($con,"SELECT Begin_tijd, Eind_tijd From baanres_speelmomenten where Dag_van_de_week = '".$datum_dag_naam."' and Vereniging_id = ".$vereniging_id."  ")     or die(' Fout in select config');  	
$periode = ""; 
  while($row = mysqli_fetch_array( $sql )) {
	 $begin_tijd  = $row['Begin_tijd'];	
     $eind_tijd   = $row['Eind_tijd'];	
	 $periode .= "[".$begin_tijd."-".$eind_tijd."] ";
  }	
	
  $qry   = mysqli_query($con,"SELECT * From vereniging where Id = ".$vereniging_id ." ")	or die(' Fout in select vereniging');  
  $result = mysqli_fetch_array( $qry );
  $vereniging = $result['Vereniging'];
 
$qry_leden   = mysqli_query($con,"SELECT * From namen where Vereniging_id=  ".$vereniging_id." order by Licentie ")	or die(' Fout in select namen');  
   

// teller per dag en week

$qry_res_splrs  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Datum = '".$datum."'
												and Licentie  = '".$licentie."' ")  
							    	or die(' Fout in select reserv kalender 2');  	  

  $result               = mysqli_fetch_array( $qry_res_splrs );
  $curr_aantal_per_dag  = $result['Aantal'];
  
  // controle op aantal per week
   $part         = explode("-", $datum );
   $week        = strftime("%W",mktime(0,0,0,$part[1],$part[2],$part[0]));

  $qry_res_splrs  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Week(Datum) =  ".$week."
												and Licentie  = '".$licentie."' ")  
							    	or die(' Fout in select reserv kalender 2');  	  

  $result                = mysqli_fetch_array( $qry_res_splrs );
  $curr_aantal_per_week  = $result['Aantal'];

	// default button clickbaar of niet
	$list_disabled_button  = 'disabled';
	$list_disabled_href    = 'aria-disabled="true"';
    $admin_disabled_button = 'disabled';
	$admin_disabled_href   = 'aria-disabled="true"';
	
     if ( isset($_GET['datum']) ){
	 	$list_disabled_button = '';
	    $list_disabled_href   ='aria-disabled="false"';
	 }
	 
	 if ($admin_user !='N'){
	    $admin_disabled_button = '';
	    $admin_disabled_href   = 'aria-disabled="false"';
	 }
	
     if (isset($_GET['kal_off'])){ 
	     $kal      = 'kal_on';
		 $curr_kal = 'kal_off';
	 } else {
		 $kal      = 'kal_off'; 
	     $curr_kal = 'kal_on';
	 }
	
// tabel scroll waarden

if (isset($_GET['start'])){
	$start = $_GET['start'];
} else {
	$start     = 1;
}

if ($start < 1){
	$start    = 1;
}

$max_width = 10;  // aantal banen naast elkaar
if ($max_width > $aantal_banen){
	$max_width = $aantal_banen;
}

$einde      = $start + ($max_width-1);

if ($einde > $aantal_banen){
	$einde = $aantal_banen;
}
// Gelijk houden van breedte
$dif = $einde-$start;

if ($dif < $max_width){
	$start = $einde - ($max_width-1);
}
 
?>
<div id='top' class="container"  > 
   <div class= "row  align-content-right">
     <div class="col-12 text-center fixed-top">
	
	 <!----   buttons in de kop------------------------>
     <a class=" btn btn-secondary btn-sm  "  href ='#top' class='back'>TOP</a>   
 	 <a  class="btn btn-secondary glyphicon glyphicon-user  btn-sm    <?php echo $list_disabled_button;?>"  data-toggle="tooltip" data-placement="bottom" title="Mijn reserveringen "                     role="button" <?php echo $list_disabled_href;?>  href="lijst_reserveringen.php?datum=<?php echo $datum;?>&<?php echo $kal;?>&id=<?php echo md5($licentie);?>"   ></a>
	 <a  class="btn btn-secondary glyphicon glyphicon-list-alt btn-sm <?php echo $admin_disabled_button;?>" data-toggle="tooltip" data-placement="bottom" title="Reserveringen voor <?php echo $datum;?> <?php echo $vereniging;?>" role="button" <?php echo $admin_disabled_href;?> href="lijst_reserveringen_datum.php?datum=<?php echo $datum;?>"  tabindex="-1"></a>
	 <a  class="btn btn-secondary glyphicon glyphicon-cog btn-sm      <?php echo $admin_disabled_button;?>" data-toggle="tooltip" data-placement="bottom" title="Aanpassen vereniging gegevens"           role="button" <?php echo $admin_disabled_href;?> href="beheer_config.php"   tabindex="-1"></a>
 	 <a  class="btn btn-secondary glyphicon glyphicon-education btn-sm " data-toggle="tooltip" data-placement="bottom" title="Handleiding" href="uitleg.php" ></a>
 	 <a  class="btn btn-secondary glyphicon glyphicon glyphicon-edit btn-sm" data-toggle="tooltip" data-placement="bottom" title="Profiel aanpassen" href="profiel_aanpassen.php?ver_id=<?php echo $_GET['ver_id'];?>&licentie_md5=<?php echo $licentie_md5;?>" ></a>
 	 <a  class="btn btn-secondary glyphicon glyphicon-log-out btn-sm"   data-toggle="tooltip" data-placement="bottom" title="Afmelden"    href="afloggen_pin.php?licentie=<?php echo $licentie;?>"  tabindex="-1"></a>
    </div> <!-- col-->    
  </div>  <!--- row---->
  
  <div class= "row" style ='margin-top:80px;'>
   <div class="col-12 text-center">
   
  <!-----  card kalender --------------------------------///----> 
  <div  class="card border border-success"  id ='kalender' style='box-shadow: 3px 3px 2px #888888;border-spacing: 4px 3px;'  >
  <div class="card-header">
     <div class = 'row  '>
    <div class=" card-text text-left col col-md-5  ">
      <!--span class = 'pull-left glyphicon glyphicon-calendar mt-3 mr-2'></span-->
  <?php if (!isset($_GET['kal_off'])){?>
	     <a  class="btn btn-secondary glyphicon glyphicon-resize-small btn-sm pull-left mr-2" data-toggle="tooltip" data-placement="bottom" title="Kalender verbergen" href="<?php echo $pageName;?>?ver_id=<?php echo $_GET['ver_id'];?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&<?php echo $kal;?>" ></a>
	  <?php } else {?>
	     <a  class="btn btn-secondary glyphicon glyphicon-resize-full btn-sm pull-left mr-2" data-toggle="tooltip" data-placement="bottom" title="Kalender tonen" href="<?php echo $pageName;?>?ver_id=<?php echo $_GET['ver_id'];?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&<?php echo $kal;?>" ></a>
       <?php	  
	  	  } ?> 
	  <strong><h4>Kalender</h4></strong> 
	</div>
	<div class='col text-right'>
	 	   <a tabindex="0" class="btn btn-sm btn-info"  data-html="true"   role="button" data-toggle="popover" data-trigger="focus" data-placement="bottom"  title=" Hoe selecteer ik een datum?" 
       data-content="Klik in onderstaande kalender op een dag naar keuze. Met de blauwe knoppen boven de kalender kan je een maand terug of verder. Door tussen de blauwe knoppen te klikken kom je weer in de huidige maand.
	    In de legenda onder de kalender wordt de betekenis van de kleuren uitgelegd.<br>Na het selecteren van een datum kan je voor die datum in het onderste gedeelte van het scherm een of meer banen reserveren.
		Via de knop links naast het woord Kalender kan je de kalender verbergen of weer tonen.<hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Hoe kies ik een datum?</em></a>
	    </div> <!--- col--->
  </div> <!--- row--->
  </div>  <!--- card header---->
  
       <?php
          if (!isset($_GET['kal_off']) ){
			  ?>
			  <div class="card-body">
			  <center>
			  <h4 class="card-title">Kies een datum voor de reservering</h4>
               <p class="card-text"> 
			   </center>
			    <center> 
	           <?php
                    include("include_kalender.php");
			    ?>
              </center>
      
        	</div> <!-- card body---> 
         <div class="card-footer bg-transparent border-success  ">
          <center>
            <table>
        	 <tr>
        	  <td style='font-size:9pt;'> Betekenis: </td>
        	  <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:black;border:1pt solid red;' 	> vandaag</td>
        	  <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:blue;border:1pt solid blue;' data-toggle="tooltip" data-placement="bottom" title="Op deze dag heb ik reserveringen"  	> mijn dag</td>
        	  <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:black;border:1pt solid black;'	> dag open</td>
        	  <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:lightgrey;color:white;' 	                > dag in verleden</td>
        	  <td  class="bg-secondary"  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;;color:white;'                    	> dag gesloten</td>
        	  <td  class="bg-warning"  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;;color:black;'                      	> feestdag gesloten</td>
        	</tr>
             </table>	
           </center>
         </div>   <!---  footer--->
     <?php
         }// end if kal off
     ?>
     </div> <!--- card--->	
    </div> <!------col---->
   </div> <!---- row---->
   
  <div class="row">
   <div class="col" style= 'font-size:9pt;'>
   <br>
   
    <!-----  card reserveringen --------------------------------///----> 
    <div class="card border border-success" style='box-shadow: 3px 3px 2px #888888;border-spacing: 4px 3px;'>
    <div class="card-header  "> 
	<div class='row'>
      
	  <div class=" card-text text-left col ml-2">
      <span class = 'pull-left glyphicon glyphicon-list mt-3 mr-2'></span>
      <strong><h4>Reserveringen</h4></strong>
	  </div>
	  
	  <div class=" card-text text-right col mt-1">
          <a tabindex="0" class="btn btn-sm btn-info" role="button" data-html='true' data-toggle="popover" data-placement="bottom"  data-trigger="focus"
		                  title="Hoe reserveer ik een baan?" 
                data-content="Heb je al een datum geselecteerd in de kalender?<br>Klik in het onderstaande schema op een keuze van tijdstip en baan om een reservering te maken.<br>Dat kan ook als er al een reservering voor die baan bestaat.<br>Er kunnen maximaal <?php echo $max_aantal_splrs_per_baan;?> personen per baan een reservering aanmaken. <br>
				              De vereniging kan een limiet stellen aan het maximum aantal reserveringen per dag en per week, zodat meer leden kunnen komen spelen.<br>Deze limieten zie je onderaan het scherm staan.<br>
	                           Het aantal reserveringen per baan wordt aangegeven met de plaatjes met 1..3.<br>  Als je met de muis over de <b>..</b>  gaat (of klikt op een tablet) zie je de namen van de spelers die gereserveerd hebben.
		                       Klik daarna op de knop 'Ophalen of Verzenden'.<hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Hoe reserveer ik een baan?
		  </a>
	     </div> <!---  col --->
	   </div> <!--- row----> 
	 </div> <!--- card header----> 
    
	<div class="card-body ml-0 mr-0">
      <h5 class="card-body text-center " > 
		Met behulp van onderstaande tabel kunnen banen worden gereserveerd. Maximaal <?php echo $max_aantal_splrs_per_baan;?> personen per baan. Maximaal <?php echo $max_aantal_splrs;?> personen op speelveld.<br>
     </h5>
   <FORM action="insert_reservering.php" method=post  name='myForm'>
        <input type="hidden" name="zendform"           value="1" /> 
        <input type="hidden" name="vereniging_id"      value="<?php echo $vereniging_id;?>" /> 
        <input type="hidden" name="datum"              value="<?php echo $datum;?>" /> 
        <input type="hidden" name="speelduur"          value="<?php echo $interval_min;?>" /> 
        <input type="hidden" name="max_splrs"          value="<?php echo $max_aantal_splrs;?>" /> 
        <input type="hidden" name="max_splrs_per_baan" value="<?php echo $max_aantal_splrs_per_baan;?>" /> 
	    <input type="hidden" name="max_freq_per_dag"   value="<?php echo $max_freq_per_dag;?>" /> 
        <input type="hidden" name="max_freq_per_week"  value="<?php echo $max_freq_per_week;?>" /> 
    	<input type="hidden" name="url"                value="<?php echo $pageName;?>" /> 
		<input type="hidden" name="kal"                value="<?php echo $curr_kal;?>" /> 
		
     <?php
        // de licentie wordt al ingevuld. Alleen admin users kunnen dit overrulen
        if ($admin_user == 'N'){
	        $user_option = 'disabled';
			?>
			<input type="hidden" name="licentie" value="<?php echo $licentie;?>" /> 
			<?php
         } ?>
	
<!------- table reserveringen ------------------------////----->
 <div id = 'reserveringen' class="table-responsive-md"> 	  
   <table width=100% class="col-12 table table-hover  table-bordered   shadow border-primary  ml-0 mr-0" style= 'border:1pt solid grey;'  >
  <thead>
  <tr class='table-primary table-responsive-md'>
      <th colspan =<?php echo $total_cols;?> ><h5><b>Baanreservering voor periodes van <?php echo $interval_min;?> minuten. Maximaal <?php echo $aantal_banen;?> banen beschikbaar.</b></h5></th>
      </tr>
  </thead>
   <thead>
    <tr>
      <th width=20% colspan =2 ><h5><b>Aanlogcode </b></h5></th>
	 
	   <?php
	   // bij admin user keuze om licentie te selecteren
	   if ($admin_user =='N'){?>
	   <th   colspan =<?php echo $max_width+4;?> >
	    <h4><?php echo $licentie." - ".$naam_licentie;?></h4>
		<?php
	   } else { ?>
     <th   colspan =<?php echo $max_width+4;?> >
	 <form class="form-inline d-none d-md-block"> 
     <div class ='input-group my-group'>
	    <select style ='font-size:9pt;'id ="adminuser" width=100pt name= 'licentie'  class="form-control selectpicker  "  data-live-search="true"   >
           <option value ='<?php echo $licentie;?>' selected><?php echo $licentie;?>-<h6><?php echo $naam_licentie;?></h6></option>
	         <?php
	   		   while($row = mysqli_fetch_array( $qry_leden )) {?>
			    <option value = "<?php echo $row['Licentie'];?>"  ><?php echo $row['Licentie'];?>-<h6><?php echo $row['Naam'] ;?></h6></option>
			   <?php } ?>
	     </select>
	    <span class ='input-group-btn input-group-append'><a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='left' title='Selecteer eventueel ander persoon om een reservering toe te voegen.En click dan 2x op de zoek knop.'>
	      <button class = 'btn btn-warning glyphicon glyphicon-search my-group-button' type= 'button' onclick="this.form.submit('myForm')"></button></a> 
	   </span> 
	   </div>
	   </form>
	     
	   <?php } /// end if admin user
	   ?>
	</th>
  	</tr>
  </thead> 
  <thead>
    <tr  >
      <th width=20% colspan =2  ><h5><b>Datum</b></h5></th>
      <th    colspan =<?php echo $max_width+5;?> ><h5><b> <?php echo $_datum; ?>   <?php echo $periode;?></b></h5></th>
    </tr>
  </thead> 
  <tr>
   <thead>
	 <tr   >
      <th colspan =2 ></th>
	
       <!-- button om terug te bladeren--->
        <th style="width:1%;"  class = "bg-secondary">
	   	 <a  class="btn btn-secondary glyphicon glyphicon glyphicon-fast-backward btn-sm "   title="Begin"   
         		 href="<?php echo $pageName;?>?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=1&kal_off#reserveringen"  tabindex="-1"></a>
 	  </th>
	   <th style="width:1%;" class = "bg-secondary">
	   	 <a  class="btn btn-secondary glyphicon glyphicon-triangle-left btn-sm "   title="Terug"   
         	 href="<?php echo $pageName;?>?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo $start-1;?>&kal_off#reserveringen"  tabindex="-1"></a>
 	  </th>
	  
	   <th  class= 'bg-velvet text-light text-center truncate-md' colspan=<?php echo $max_width-4;?>  > <h4>Baankeuze 1 - <?php echo $aantal_banen;?></h4></th>
		 
     <!-- button om verder te bladeren--->
	  <th width= 1% class = "bg-secondary" >
   	    <a  class="btn btn-secondary glyphicon glyphicon-triangle-right btn-sm mr-3"    title="Verder"   
            href="<?php echo $pageName;?>?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo $start+1;?>&kal_off#reserveringen"  tabindex="-1"></a>
 	 </th>
     <th width= 1% class = "bg-secondary" >
   	    <a  class="btn btn-secondary glyphicon glyphicon glyphicon-fast-forward btn-sm "    title="Einde"   
            href="<?php echo $pageName;?>?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo ($aantal_banen-$max_width)+1;?>&kal_off#reserveringen"  tabindex="-1"></a>
 	 </th>
    </tr>
   </thead> 
   </table>
   
   <span>
		   <a tabindex="0" width=10% class="btn btn-sm btn-info" role="button" data-html='true' data-trigger="focus"  data-toggle="popover" data-placement="bottom"  title="Scroll baan nummers" 
       data-content="Hierboven staan knoppen met pijlen naar links en rechts. Hiermee kan je scrollen naar hogere of lagere baan nummers. 
	   <br>Met de knoppen aan de uiteinden kan je direct naar het begin of het einde springen.<br>Als er geen 10 banen te zien zijn, kan je op je telefoon door over het scherm te vegen de andere banen zien.
	   <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Scrollen</a>
	   </span>
	  <!------------  tabel voor het invoeren van reserveringen----------------------////---->
     <table class=" col-12 table table-striped table-focus table-responsive-md  table-bordered shadow" style= 'border:1pt solid grey;'  >
      <thead>
	   <tr>
        <th style='border:0px;' class='glyphicon glyphicon-time'> 
	     </th>
	     <th width=5% class='bg-warning ' data-toggle="tooltip" data-placement="bottom" title="Aantal reserveringen/Max aantal personen"  style='font-size:8pt;'># Res</th>
 	 
	   <?php
	   /// Aantal banen
	    for ($i=$start;$i<=$einde;$i++){
			/// afwijkende baan nummers
		    $qry_baan  = mysqli_query($con,"SELECT * From baanres_baan_nummers where Vereniging_id = ".$vereniging_id." and Baan = ".$i."    ")	or die(' Fout in select baan afw');  
            $result2   = mysqli_fetch_array( $qry_baan);
			$baan_nr    = $result2['Baan_afw_nr'];
			if ($baan_nr ==''){
				$baan_nr = $i;
			}
	
			?>
		  <th  class= 'bg-velvet text-light text-center'  ><h4><?php echo $baan_nr;?><h4></th>
		<?php	
	   }// end for
     ?>
	   </tr>
	  </thead> 
   
  	  <?php
		// speelmomenten in eerste kolom. 
	 		  $sql        = mysqli_query($con,"SELECT * From baanres_speelmomenten where Dag_van_de_week = '".$datum_dag_naam."' and Vereniging_id = ".$vereniging_id." order by Begin_tijd ")     or die(' Fout in select config');  	
			
               while($row = mysqli_fetch_array( $sql )) {
                $begin      = $row['Begin_tijd'];
			    $eind       = $row['Eind_tijd'];
				
				$part        = explode (":", $begin);
                $begin_GMT   = mktime($part[0],$part[1],$part[2]);
				$part        = explode (":", $eind);
                $eind_GMT    = mktime($part[0],$part[1],$part[2]);
                $interval    = $interval_min * 60;
				?>
				  <input type="hidden" name="eind_tijd" value="<?php echo $eind_GMT;?>" /> 
				  <?php
				
			    // per speelmoment de intervallen van x min bepalen
				
				for ($x=$begin_GMT;$x<$eind_GMT;$x=$x+$interval){
					$vanaf = date("H:i",$x);
				?>
				   <tr>
				    <th scope = 'row' ><h5><b>
	                   <?php echo $vanaf."-".date("H:i",Addtime($x,$interval_min,$eind_GMT) );?>
					   </b></h5>
		            </th>  <!---- col--->
			     
                    <th class ='text-center' style='font-size:8pt;'>
					<?php
					$current_aantal =0;
					
					 $sql_kal = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender where 
					             Datum = '".$datum."' 
								 and Vereniging_id = ".$vereniging_id." 
								 and Begin_tijd_reservering = '".$vanaf."' ")     or die(' Fout in select kalenderxx');  	

                     $result  = mysqli_fetch_array( $sql_kal  );
                     $current_aantal = $result['Aantal'];
					 
					 //  aantal huidige reservingen / max aantal
					 echo $current_aantal."/".$max_aantal_splrs;
					 ?>
					</th>
				 
					<?php
					//van start tot einde
					
					 for ($i=$start;$i<=$einde;$i++){?>
					 <td style='vertical-align:middle;text-align:center;'  >
					   <?php
					    $sel_datum = $_GET['datum'];
						/// afwijkende baan nummers
		                $qry_baan  = mysqli_query($con,"SELECT * From baanres_baan_nummers where Vereniging_id = ".$vereniging_id." and Baan = ".$i."    ")	or die(' Fout in select baan afw');  
                        $result2   = mysqli_fetch_array( $qry_baan);
			            $baan_nr    = $result2['Baan_afw_nr'];
			            if ($baan_nr ==''){
				            $baan_nr = $i;
			            }	
						 /// bepaal aantal reserveringen
						$qry_res_baan  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   							where Vereniging_id = ".$vereniging_id." and Datum = '".$sel_datum."'
											and Baan = ".$baan_nr." 
											and Begin_tijd_reservering = '".$vanaf."'     ")  
								or die(' Fout in select aantal reserveringen');  	  
														
						     $result  = mysqli_fetch_array( $qry_res_baan  )		;
							 $aantal  = 0 ;
							 $aantal  = $result['Aantal'] ;
						
						      // namen van de spelers per baan
						 
	                         $qry_res_baan_splrs  = mysqli_query($con,"SELECT Naam, k.Licentie From baanres_kalender as k
							                 join namen as n  on k.Licentie = n.Licentie
                   							where k.Vereniging_id = ".$vereniging_id." and Datum = '".$sel_datum."'
											and Baan = ".$baan_nr." 
											and Begin_tijd_reservering = '".$vanaf."'     ")  
								or die(' Fout in select namen reserveringen ');  
                             $splrs='';								
							 while($row2 = mysqli_fetch_array( $qry_res_baan_splrs )) {
								       // alleen voornaam 
								       $part = explode(" ", $row2['Naam'] );
								       $splrs = $splrs.$part[0].", ";
								 } // end while
								 
							 $var = 'icon'.$result['Aantal'].'.png';
							 if ($result['Aantal'] == $max_aantal_splrs_per_baan){?>
								<img data-toggle="tooltip" data-html='true' data-placement="top" title="<?php echo $aantal;?> reservering(en) voor baan <?php echo $baan_nr;?> om <?php echo $vanaf;?>.Dat zijn <?php echo substr($splrs,0,-2);?>."
								src='images/icon_bezet.png' width=25>
								<?php
							 } else { 
			                      ?>
			                <input type='checkbox' name= 'baan_tijdstip[]' class='baan<?php echo $aantal;?>'   id= 'baan_<?php echo $x."_".$baan_nr;?>' value='<?php echo $baan_nr.";".$vanaf; ?>' /><label for="baan_<?php echo $x."_".$baan_nr;?>"></label>	
        					<?php if ($aantal > 0){?>
								<span style='color:red;' class='mb-0 mt=0' data-toggle="tooltip" data-html='true' data-placement="top" title="<?php echo $aantal;?> reservering(en) voor baan <?php echo $baan_nr;?> om <?php echo $vanaf;?>.Dat zijn <?php echo substr($splrs,0,-2);?>.">..
						           </span>         
       		                <?php } ?>
   	                  <?php } ?>
								 
					 </td>  <!---- col---> 		
	               <?php 
					 } //  for i = start
					?> 
				
				</tr> <!--- row---> 	
				<?php
				} // end for x
			}  // end for speelmoment
  
  ?>
   </table>
   </div> <!--- id = 'reserveringen'---->
   
   <div class="row"> 
     <div class="text-left col"> 
       <p class='text-left'><h5> Als je geen reserveringen ziet, selecteer datum en klik dan op de knop hiernaast</h5></p>
	</div> 
	  <div class="text-right col"> 
       <input type="submit" class="btn btn-info btn-lg" value="Ophalen of verzenden">
	</div> 
	</div>
	
  <table width=99%>
   <tr> 
   <td class= 'text-left'>
    <span><a tabindex="0" width=10% class="btn btn-sm btn-warning" role="button" data-html='true' data-toggle="popover" data-trigger='focus'  data-placement="top"  title="Afwijkende baan nummers" 
       data-content="Het kan zijn dat de baan nummers niet oplopen van 1,2,3 enz. Dat komt dan omdat de vereniging gebruik maakt van afwijkende baan nummers, zodat je direct weet waar je moet spelen.
	   <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Baan nrs</a></span>
	   </td>
	   <td style='text-align:center;'><span><a tabindex="0" width=10% class="btn btn-sm btn-warning" role="button" data-trigger='focus' data-html='true' data-toggle="popover" data-placement="top"  title="Wie speelt er?" 
       data-content="Als je met de muis over de .. gaat bij een reservering, dan zie je de namen van de personen.<hr><span style='text-align:right;'><button>Sluit uitleg</button></span>"> * ..wie</a>
	   </span>
    </tr>
	 </table>
   </form>
   
	<br>
   <div class="card-footer bg-transparent border-success table-responsive-md">
   <style>
   .vcenter  { vertical-align: middle;}
   </style>
   <h5 class='text-center'><b>
      <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='De onderstaande plaatjes geven aan hoeveel reserveringen er bestaan voor een baan/tijdstip'>
			  Aantal reserveringen (legenda)</a></h5> 
    <table  class='table  table-bordered table-responsive-md'>
       <tr>
         <td style='text-align:center;vertical-align:middle;' width=15%><h6> <img src='images/backup_not_checked.png' width=25>  geen</h6> </td>
	     <td style='text-align:center;vertical-align:middle;' width=15%><h6> <img src='images/icon1_big.png'             width=35>  </td>
	      <?php 
	      if ($max_aantal_splrs_per_baan == 2){?>
	        <td class='vcenter' width=15%><h6><img src='images/icon_bezet.png' width=25>  2 (max)</h6> </td>  
	      <?php } else {?>
	      <td style='text-align:center;vertical-align:middle;' width=15%><h6><img src='images/icon2_big.png'             width=35>  </h6> </td>
	      <td style='text-align:center;vertical-align:middle;' width=15%><h6><img src='images/icon3_big.png'             width=33>  </h6>  </td>
	      <td style='text-align:center;vertical-align:middle;' width=15%><h6><img src='images/icon_bezet.png'         width=25>  4 (max)</h6> </td> 	  
	    	<?php  }  
	      ?>
	   </tr>
	    </table>	
		<h5 class='text-center'><b>
		<a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" 
		         data-placement='top' 
		  title='Om andere leden ook de kans te geven te spelen is er een limiet gesteld voor het maximum aantal reserveringen per dag en per week voor een persoon.'>Limieten per dag en per week</a></h5> 
		  <table  class='table  table-bordered table-responsive-md'>
		  
		<?php
		// andere kleuren bij overschrijding limiet
         $dag_color = 'black;border-left:1pt solid white';
	 
		 if ($curr_aantal_per_dag  >= $max_freq_per_dag){
			 $dag_color = 'red;border-bottom:2pt solid red';
	     }
		 $week_color = 'black;border-left:1pt solid white';
	 	  
		 if ($curr_aantal_per_week  >= $max_freq_per_week){
			 $week_color = 'red;border-bottom:2pt solid red; ';
	     }
    	 ?> 
       <tr>
         <td style='text-align:center;vertical-align:middle;border-right:1pt solid white;' width=12%>
		  <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		     title='Om andere leden ook de kans te geven te spelen is er een limiet gesteld voor het maximum aantal reserveringen per dag en per week voor een persoon.'><img class= "img-fluid mr-3" src='images/max_dag.jpg' width=44> </a> 
		  </a> 
		 </td>
		 
	     <td style='text-align:left;vertical-align:middle;' width=5%>
		   <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Maximaal <?php echo $max_freq_per_dag;?> reserveringen per dag voor <?php echo $licentie;?> '><h4><center><?php echo $max_freq_per_dag;?></center></h4>
		   </a>
	      </td>
		
		<td  style='text-align:center;vertical-align:middle;border-right:1pt solid white;'  width=10%>
		   <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Datum : <?php echo $_datum;?>.Aanlogcode :<?php echo $licentie;?>'><img class= "img-fluid mr-3 mb-2" src='images/icon_dag.jpg' width=36>  
		   </a> 	  
		  </td>
		  
	     <td style='text-align:center;vertical-align:middle;color:<?php echo $dag_color;?>;' width=5%>
		  <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Momenteel <?php echo $curr_aantal_per_dag;?> reserveringen deze dag voor <?php echo $licentie;?> '>
			    <h4><center><?php echo $curr_aantal_per_dag;?></center></h4>
		   </a>
		  </td>
	     
		 <td style='text-align:center;vertical-align:middle;border-right:1pt solid white;' width=12%><h6><strong><a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" 
		      data-placement='top' title='Om andere leden ook de kans te geven te spelen is er een limiet gesteld voor het maximum aantal reserveringen per dag en per week voor een persoon.'>
			      <img class= "img-fluid mr-3" src='images/max_week.jpg' width=44></a></strong></h6> </td>
	     <td style='text-align:left;vertical-align:middle;' width=5%>
		  <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Maximaal <?php echo $max_freq_per_week;?> reserveringen per week voor <?php echo $licentie;?> '>
			  <h4><center><?php echo $max_freq_per_week;?></center></h4>
		  </a>
		  </td>
	 	
		<td style='text-align:center;vertical-align:middle;border-right:1pt solid white;' width=10%><h6><strong>
		   <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Week : <?php echo $week;?>.Aanlogcode  :<?php echo $licentie;?> '><img class= "img-fluid mr-3" src='images/icon_week.jpg' width=36>
		  </a> 	  </strong></h6>
		  </td> 

	      <td style='text-align:left;vertical-align:middle;color:<?php echo $week_color;?>;' width=5%>
		   <a href='#' data-html='true' data-width=400px data-height=100px data-toggle="tooltip" data-placement='top' 
		      title='Momenteel <?php echo $curr_aantal_per_week;?> reserveringen deze week voor <?php echo $licentie;?> '><h4><center><?php echo $curr_aantal_per_week;?></center></h4>
			</a>
		  </td>
      </tr>
	 </table>	
   
 
    </div>  <!--- card footer--->
   </div> <!--card body--->
  </div> <!--- card--->
  <br>
  
   
   </div> <!---- col--->
 </div>  <!-- row ----> 
  </div>  <!--- container -->
  
 <script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
  
});

$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  
});


$('.popover-dismiss').popover({
  trigger: 'focus'
})
 $("#fake").click(function() {
  $("html").toggleClass("csstransforms");
})
if ( $.browser.webkit ) {
    $(".my-group-button").css("height","+=1px");
}
$(function(){
  $("#example")
      .popover({
      title : 'tile',
      content : 'test'
    })
    .on('shown', function(e){
      var popover =  $(this).data('popover'),
        $tip = popover.tip();

      var close = $('<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
        .click(function(){
          popover.hide();
        });
      $('.popover-title', $tip).append(close);
    });
});

$('.pop').popover().click(function () {
    setTimeout(function () {
        $('.pop').popover('hide');
    }, 2000);
});

$('#licenties').editableSelect();
</script>

</body>
</html>
