<?php
#
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
<?php
 

ob_start();
include 'conf/mysqli.php'; 
//include('action.php');
include ('../ontip/versleutel_string.php'); // tbv telnr en email

function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}
$count =0 ;

$licentie           = $_POST['licentie']; 
$pin1               = $_POST['pin1']; 
$pin2               = $_POST['pin2']; 


// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

if ($licentie ==''){
	$message .= "* Er is geen toegangscode ingevuld.<br>";
	$error = 1;
}

if ($pin1 ==''){
	$message .= "* Er is geen eerste pin ingevuld.<br>";
	$error = 1;
}

if ($pin2==''){
	$message .= "* Er is geen tweede pin ingevuld.<br>";
	$error = 1;
}

if ($pin1 != $pin2 ){
	$message .= "* PIN1 en PIN2 zijn niet gelijk.<br>";
	$error = 1;
}

if (!is_numeric($pin1) ){
	$message .= "* PIN code is geen getal.<br>";
	$error = 1;
}

if (strlen($pin1) <>4  ){
	$message .= "* PIN code moet uit 4 cijfers bestaan.<br>";
	$error = 1;
}


if ($error == 0 ){
	$sql        = mysqli_query($con,"SELECT * From namen  where Licentie = '".$licentie."'  ")     or die(' Fout in select namen'); 
    $result     = mysqli_fetch_array( $sql ) ;
    $vereniging = $result['Vereniging']	;
	$_email      = versleutel_string($result['Email_encrypt']);
	$naam       = $result['Naam'];
	$id         = $result['Id'];
	$pin_encrypt = md5($pin1);
	$id_md5      = md5($id);
	
}

if ($naam ==''){
	$message .= "* Deze toegangscode is niet bekend. Registreer je eerst .<br>";
	$error = 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1
 


if ($error ==0){
	 
$subject = 'Aanpassen PIN OBER - '. $naam;
$to      = $_email;
$admin   = 'erik.hendrikx@gmail.com';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: OnTip OBER <noreply@ontip.nl>' . "\r\n" .
            'Reply-To: OnTip OBER <erik.hendrikx@ontip.nl>'.  "\r\n" .
	        'Bcc: '. $admin . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
$headers  .= "\r\n";

$bericht = "<table>"   . "\r\n";
$bericht .= "<tr><td><img src= 'https://www.ontip.nl/baanreservering/images/ober.png' width=80>"   . "\r\n";
$bericht .= "<td style= 'font-family:verdana;font-size:14pt;color:blue;'>Bevestiging registratie OBER</b><br>
             <span style='font-size:9pt;'>Een programma voor reserveren van Jeu de boules banen tijdens de corona crisis</span></td></tr>" . "\r\n";

$bericht .= "</table>"   . "\r\n";
$bericht .= "<br><br><hr/>".   "\r\n";
 
$bericht .= "<h3><u>PIN code wijziging aangevraagd.</u></h3>".   "\r\n";

$bericht .= "<table style= 'font-family:verdana;font-size:9pt;' >"   . "\r\n";
$bericht .= "<tr><td  width=200 >Naam</td><td>"         .  $naam     ."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Toegangscode </td><td>"    .  $licentie."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Vereniging </td><td>"  .  $vereniging."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >Email     </td><td>"   .  $_email       ."</td></tr>".  "\r\n";
$bericht .= "<tr><td  width=200 >PIN nieuw     </td><td>"     .  $pin1       ."</td></tr>".  "\r\n";
$bericht .= "</table>"   . "\r\n";

$replace = 'x='.$id_md5."&y=".$pin_encrypt;
$bericht .= "<br><div  width=200> <a href='https://www.ontip.nl/baanreservering/wijziging_pin.php?".$replace."'>Klik op deze link</a> om de nieuwe PIN te activeren</div>". "\r\n";
$bericht .= "<br><hr/>".   "\r\n";
$bericht .= "<br><span style='font-size:9pt;color:darkgrey;'>(c) Erik Hendrikx. Deze email is geautomatiseerd aangemaakt van het OBER programma</span>".   "\r\n";

mail($to, $subject, $bericht, $headers);

$email_mask = substr($_email,0,3)."***************".substr($_email,-3,5);

echo "<br><h5>PIN code wijziging is verstuurd naar ".$email_mask.". <br>Klik op de link in de email om de PIN daadwerkelijk te wijzigen.</h5>"; 

}
 
?>
<br>

<a href ='index.html' class="btn btn-primary"> Klik hier om terug gaan naar het aanlog scherm </a>
</div>  <!----- container--->
</body>
</html>
