<?php
# afloggen.php
# Reset aanlog op deze PC
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
#
# 18okt2018          1.0.1            E. Hendrikx
# Symptom:   		    None.
# Problem:       	  None.
# Fix:              Opgelost
# Feature:          None.
# Reference: 
#

//header("Location: ".$_SERVER['HTTP_REFERER']);
ob_start();
function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}

include 'conf/mysqli.php'; 

$vereniging_id = $_GET['ver_id'];

mysqli_query($con,"Update namen set Laatst = now(),
                              IP_adres = '' ,
                              Aangelogd = 'N'
                       WHERE IP_adres = '". md5($_SERVER['REMOTE_ADDR'])."' and Vereniging_id = ".$vereniging_id."   ");

if ($url ==''){
 $url = "index.php?ver_id=".$vereniging_id;
}
redirect($url);

ob_end_flush();
?>
