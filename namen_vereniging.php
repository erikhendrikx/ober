<?php 
# namen_vereniging.php
# Baanreservering  Report
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 17okt2020         -            E. Hendrikx 
# Symptom:   		None.
# Problem:     	    None
# Fix:              None
# Feature:          Link toegevoegd voor users per vereniging
# Reference: 
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">

@media print {
    body {
        margin: 10px;
		margin-top:15px;
		margin-right:15px;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 11px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
   }
	
@media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}

@media print {
    h5 {  font: 44px Arial, Helvetica,"Lucida Grande", serif;
          color:blue; }  
}

.noprint {display:none;}    
 
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id,Vereniging FROM namen WHERE  Vereniging_id = ".$_GET['ver_id']."  ") or die(' Fout in vereniging Id');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$vereniging     = $result0['Vereniging'];
 
 
$qry_namen   = mysqli_query($con,"SELECT * From namen WHERE  Vereniging_id = ".$_GET['ver_id']." order by Naam ")	or die(' Fout in select namen');  
  

?>
<div class= 'container'> 
  <div class= "row d-print-none ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th       data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?datum=<?php echo $datum;?>&<?php echo $kal;?>"   ></a>
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-list-alt data-toggle="tooltip" data-placement="bottom" title="Lijst "      href="report_reserveringen.php"   ></a>
  </div>

  
   
<h3> Report OBER  </h3>
  
 <?php
   $qry_res_totaal  = mysqli_query($con,"SELECT  count(*) as Aantal From baanres_kalender WHERE  Vereniging_id = ".$_GET['ver_id']." ")	or die(' Fout in select namen totaal ');  
   $result = mysqli_fetch_array( $qry_res_totaal );
   $totaal_namen  = $result['Aantal'];
   
  
?>
<br>
<h5 style= 'color:blue;font-weight:bold;'>Namen en aantal reserveringen per vereniging  <?php echo $vereniging;?></h5>
 <h6 style= 'color:black;font-weight:bold;'>Klik op de licentie voor een detail lijst.</h6>

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th style='border-bottom:1pt solid red;'  class='text-right red' width=2% >Nr.</th>
      <th style='border-bottom:1pt solid red;'  width=10%  >Licentie</th>
      <th style='border-bottom:1pt solid red;'  >Naam</th>
      <th style='border-bottom:1pt solid red;text-align:right;'  >Aantal</th>
      
     </tr>
	</thead>
	<?php
	
	$i=1;
	$totaal_namen = 0;
	while($row = mysqli_fetch_array( $qry_namen )) { 
	 
	  $qry_res_naam     = mysqli_query($con,"SELECT  count(*) as Aantal From baanres_kalender WHERE  Vereniging_id = ".$_GET['ver_id']."  and Licentie = '".$row['Licentie']."' ")	or die(' Fout in kalender naam totaal ');  
      $result           = mysqli_fetch_array( $qry_res_naam );
      $totaal_licentie  = $result['Aantal'];
	  $totaal_namen  = $totaal_namen + $totaal_licentie;
	?>
		
	  <tr>
    	<td class='text-right'><?php echo $i;?>.</td>
        <td class='text-left'><a href="reserveringen_per_naam.php?ver_id=<?php echo $_GET['ver_id'];?>&licentie=<?php echo $row['Licentie']; ?>"  target ='_seld'><?php echo $row['Licentie']; ?></a></td>
        <td><?php echo $row['Naam']; ?></td>
		<td class='text-right'><?php echo $totaal_licentie; ?> </td>
	 
       </td>
		</tr>
		<?php
          $i++;
		  }  // end while  row 
?>	
   <tr>
	  <td colspan = 3><b>Totaal</b></td>
	  <td class='text-right ' style='border-top:2pt solid black;'><b><?php echo $totaal_namen;?></b></td>
	 </tr>
	
 </table>
  <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
  
 <hr/> 
  
 
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  
 </div>  <!---- container---->
  

 </body>
 </html>
 