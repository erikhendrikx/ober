<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">


<style type="text/css">

h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 

$datum         = $_GET['datum'];
$licentie      = $_GET['licentie'];
$part          = explode("-", $datum);
$_datum        = strftime("%A %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
$vereniging_id = $_GET['ver_id'];

?>
<div class= 'container'> 
  <div class= "row  ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?ver_id=<?php echo $_GET['ver_id'];?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>"   ></a>
  </div>

<div class="row">
  <div class ='col'>
 <h2><img src='images/ober.png' width=45> OBER  Ontip Baan Reservering </h2>
 <h3>Aanpassen Profiel</h3>
<form action="muteren_profiel.php" method=post  name='myForm'>
       <input type="hidden" name="zendform"     value="1" /> 
       <input type="hidden" name="licentie_md5" value="<?php echo $licentie_md5;?>" /> 
 </div>
</div>
 
   <div class="form-group">
    <label for="licentie">Licentie</label>
    <input disabled autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"   type="text" name ='licentie' class="form-control" id="licentie" aria-describedby="licentieHelp" placeholder="<?php echo $licentie;?>">
         <small id="licentieHelp" class="form-text text-muted">Kan niet gewijzigd worden.</small> 
 </div>
   
<div class="form-group">
    <label for="licentie">Wijziging naam</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="text" name = 'naam' class="form-control" id="naam" aria-describedby="naamHelp" value="<?php echo $naam;?>">
	 <small id="naamHelp" class="form-text text-muted">Type je volledige naam</small>
  </div>
 
  <div class="form-group">
    <label for="exampleInputEmail1">Wijziging email adres</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="email" name = 'email' class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" value="<?php echo $email;?>">
 	 
 </div>
  <div class="form-group">
  
      <label for="inputCity">Vereniging</label>
  
  <select id ="inputVereniging" name= 'vereniging_id'  class="form-control" aria-describedby="verenigingHelp">
        <option value ='<?php echo $vereniging_id;?>' selected><?php echo $vereniging;?></option>
		<?php
			$sql        = mysqli_query($con,"SELECT * From vereniging  order by Vereniging_nr  ")     or die(' Fout in select ver');  	
			   while($row = mysqli_fetch_array( $sql )) {?>
			    <option value = '<?php echo $row['Id'];?>'  ><?php echo $row['Vereniging_nr']." - ".$row['Vereniging'];?></option>
        <?php
               }
		?>
      </select>
	   <small id="verenigingHelp" class="form-text text-muted">Selecteer je vereniging uit de lijst met OnTip verenigingen.</small>
	</div>  
	 <div class="form-group">
    <label for="exampleInputEmail1">Nieuwe PIN code</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="password" name = 'pin1' class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type een pincode">
 	 <small id="pinHelp" class="form-text text-muted">Niet gewijzigd als er niets wordt ingevuld.</small> 
 </div>
  <div class="form-group">
    <label for="exampleInputEmail1">PIN code nogmaals</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"    type="password" name = 'pin2'  class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type zelfde pincode">
 	 <small id="pinHelp" class="form-text text-muted">Ter kontrole, alleen bij nieuwe PIN code</small> 
 </div>
<br>
<h6>Na verzending van dit formulier ontvangt u op het nieuwe email adres een bevestiging </h6>

<br>
 <button type="submit" class="btn btn-primary">Verzenden</button>

			
</form>
 </div>  <!---- container---->
 
<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})
</script>

 </body>
 </html>
 