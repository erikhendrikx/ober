<?php
	if (isset($_GET['maand_nr'] )){
	     	$maand_nr  = $_GET['maand_nr'];
			$jaar      = $_GET['jaar'];
		} else {
			$maand_nr = date('m');
			$jaar     = date('Y');
		}
	
   /// kan niet eerder dan huidige maand 		 	 	
   $current = date('Y').sprintf("%02d",date('m') ) ;  
    
   if ($jaar.sprintf("%02d",$maand_nr) < $current){
        $maand_nr   = date('m');
      }	 	
 
   $jaar_verder = $jaar;
   $jaar_terug = $jaar;
   
   $maand_verder  = $maand_nr +1;	
   $maand_terug   = $maand_nr -1;
	   
   if ($maand_verder > 12){
	   $maand_verder = 1;
		$jaar_verder++;
   }
	  
    if ($maand_terug < 1){
		$maand_terug = 12;
		$jaar_terug--;

	}
	
   $dag           = date('d');
   $today         = date('Y')."-".date('m')."-".date('d');
?> 

 <div  id ='kalender' style='display:block;' >
  <hr>  
  <center>
     <h4 class='text-center'  ><b>Datum keuze	
 	   <span    >
		   <a tabindex="0" class="btn btn-sm btn-info" role="button" data-toggle="popover" data-placement="bottom"  title=" Hoe selecteer ik een datum?" 
       data-content="Klik in onderstaande kalender op een dag naar keuze. Met de blauwe knoppen boven de kalender kan je een maand terug of verder. Door tussen de blauwe knoppen te klikken kom je weer in de huidge maand.
	    In de legenda onder de kalender wordt de betekenis van de kleuren uitgelegd.Na het selecteren van een datum kan je voor die datum in het onderste gedeelte van het scherm een of meer banen reserveren.
		Via de kalender knop bovenaan het scherm kan je de kalender verbergen.
	    [Klik op '* Hoe kies ik ...'' om deze uitleg te sluiten]">* Hoe kies ik een datum?</a>
	   </span>
</b></h4>     
	
   
		<!--  navigatie maanden ----  met Ipad problemen met row en col ----///--->
	
	    <table border =0 width=60% cellspacing=0 cellpadding=0 >
	        <tr>
			 <td width = 30%   >
			 	  	  <a class="btn btn-lg glyphicon glyphicon-backward bg-primary"  data-toggle="tooltip" data-placement="bottom" title="Ga maand terug"  role="button" style='font-size:9pt;text-align:left;color:white;'   href ='<?php echo $pageName;?>?jaar=<?php echo $jaar_terug;?>&maand_nr=<?php echo $maand_terug;?>&ver_id=<?php echo $vereniging_id;?>'><a/>
     		 </td>
		      <td width = 30%>
			   		  <a class="btn btn-lg"   style='font-size:14pt;;color:blue;'  href ='<?php echo $pageName;?>?jaar=<?php echo date('Y');?>&maand_nr=<?php echo date('m');?>&ver_id=<?php echo $vereniging_id;?>'>
	 		  <?php echo ucfirst(strftime("%B - %Y", mktime(0, 0 , 0, $maand_nr , $dag, $jaar)) ) ; ?>	 					</a>
			 </td> 
			 <td width = 30% class='text-right'>
			 		 <a class="btn btn-lg glyphicon glyphicon-forward bg-primary"  data-toggle="tooltip" data-placement="bottom" title="Ga maand verder "  role="button" style='font-size:9pt;text-align:right;color:white;'  href ='<?php echo $pageName;?>?jaar=<?php echo $jaar_verder;?>&maand_nr=<?php echo $maand_verder;?>&ver_id=<?php echo $vereniging_id;?>'> </a>
	        </td>	 
			</tr>
		</table>
		
	    
	  	<table border =1 width=60% cellspacing=0 cellpadding=0 style='box-shadow: 4px 4px 4px #888888;border-spacing: 8px 12px;'>
	 		<tr>
	 			<td class= 'bg-info text-center' style='font-size:8pt;'  width=10% >Week</td>
	 			<td class= 'bg-warning text-center' >Ma</td>
	 			<td class= 'bg-warning text-center' >Di</td>
	 			<td class= 'bg-warning text-center' >Wo</td>
	 			<td class= 'bg-warning text-center' >Do</td>
	 			<td class= 'bg-warning text-center' >Vr</td>
	 			<td class= 'bg-warning text-center' >Za</td>
	 			<td class= 'bg-warning text-center' >Zo</td>
	 		</tr>
	 		<tr></tr>
	 <?php		
	 	
	 	
	    for ($dag=1;$dag< 32 ;$dag++){
	 		$day_number = date ('N' ,mktime(0,0,0,$maand_nr,$dag,$jaar));
	
	       echo "<tr>"; 
		     $week_nr  = strftime("%W", mktime(0, 0, 0, $maand_nr , $dag, $jaar) );
		     $week_nr ++;
	 		    ?>
	 		      <td style='background-color:#EBF3FF;font-size:8pt;color:black;font-family:verdana;text-align:center;'><?php echo $week_nr; ?></td>
 	   	 	  <?php	
	 		     	
	 		  for ($weekdag=1;$weekdag < 8 ;$weekdag++){
	 		     	
	 	       if ($day_number == $weekdag and $dag < 99) { 
	 	    	 	$dag_naam      = substr(strftime("%A", mktime(0, 0, 0, $maand_nr , $dag, $jaar) ),0,2);
	 	    	 	$full_dag_naam = ucfirst(strftime("%A", mktime(0, 0, 0, $maand_nr , $dag, $jaar) ));
	 	    	 	$_dag          = sprintf("%02d",$dag);
	 	    	 	$_mnd          = sprintf("%02d",$maand_nr);
	 	    	 	$mnd_naam      = strftime("%B", mktime(0, 0, 0, $maand_nr , $dag, $jaar) );
	 	    	
				    $var = $full_dag_naam."_open";
					
				    $sql  = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id."  ")     or die(' Fout in select config');  	
				    $result  = mysqli_fetch_array( $sql) ;
                    $geopend = $result[$var];
				    $feestdag ='';
				      
				    $sql  = mysqli_query($con,"SELECT * From baanres_feestdagen where Vereniging_id = ".$vereniging_id."
                                 and Datum = '".$jaar."-".sprintf("%02d",$maand_nr)."-".sprintf("%02d",$dag)."'	")     or die(' Fout in select feestdagen');  	
				    $result  = mysqli_fetch_array( $sql) ;
					
					
                    if ($result['Id'] !=''){
						$geopend = 'N';
						$feestdag = $result['Omschrijving'];
					}
					
					$mijn_dag ='N';
					$sql  = mysqli_query($con,"SELECT * From baanres_kalender where Vereniging_id = ".$vereniging_id."
                                 and Datum = '".$jaar."-".sprintf("%02d",$maand_nr)."-".sprintf("%02d",$dag)."' 
                                 and Licentie = ".$licentie."  ")     or die(' Fout in select kalender');  	
				    $result  = mysqli_fetch_array( $sql) ;
                    if ($result['Id'] !=''){
						$mijn_dag ='J';
					}
					
					
				    if ($geopend =='N'){
						 $border = "border:1pt solid black";
						if ($today == $jaar."-".sprintf("%02d",$maand_nr)."-".sprintf("%02d",$dag)  ){ 
						 $border = "border:2pt solid red";
						}
					   if ($feestdag != ''){?>
								<td class="bg-warning" style='font-weight:bold;font-size:11pt;<?php echo $border;?>;text-align:center;font-family:arial;padding:3pt;color:black;' 	
							><?php echo $dag;?></td>
					   <?php } else {?>
						<td class="bg-secondary" style='font-weight:bold;font-size:11pt;text-align:center;<?php echo $border;?>;font-family:arial;padding:3pt;color:lightgrey;' 	
							><?php echo $dag;?></td>
					   <?php } ?>	
								
					<?php	
					}  else {
				   
			        if ($today <= $jaar."-".sprintf("%02d",$maand_nr)."-".sprintf("%02d",$dag) ){
			   
			            // default 
			       	   $border = "border:1pt solid black";
					    // vandaag krijgt een rode rand
						if ($today == $jaar."-".sprintf("%02d",$maand_nr)."-".sprintf("%02d",$dag)  ){ 
						 $border = "border:2pt solid red";
						}
						/// mijn dag reservering krijgt een blauwe rand
					   if ($mijn_dag == "J"){
			 			 $border = "border:2pt solid blue";
					   }
					   
 	   	         	  	  
		    	     	?>
 	                  	<td  style='font-weight:bold;font-size:9pt;text-align:center;font-family:arial;padding:3pt;<?php echo $border;?>;' 
 	              	   	 	onmouseover="this.bgColor='#16B0F8'"
		  	      	   	 	onmouseout="this.bgColor='white'"  
				      	 	onClick="location.href='<?php echo $pageName;?>?jaar=<?php echo $jaar;?>&maand_nr=<?php echo $maand_nr;?>&datum=<?php echo $jaar."-".$_mnd."-".$_dag;?>&ver_id=<?php echo $_GET['ver_id'];?> ';"	
				      	   ><?php echo $dag;		    
				   // geen dag vd maand
			          } else { ?>
			    	  <td  style='font-weight:bold;font-size:11pt;text-align:center;font-family:arial;padding:3pt;background-color:lightgrey;color:white;' 	>
					
			     	   <?php echo $dag;?></a>
	 	
			   <?php    } // geen dag vd maand ?>
   	 		   
 	   	 	   </td>
 	     	 	  <?php 
					} // geopend
					
					
  		     	     $dag++;
	 		     	 $day_number = date ('N' ,mktime(0,0,0,$maand_nr,$dag,$jaar));
	 		     	 
	 		     	 // 
	 		     	 if ( date ('m' ,mktime(0,0,0,$maand_nr,$dag,$jaar))   != $maand_nr) {
	 		     	 	  $dag =99;
	 		     	 }
	
		     	 	  }  else {
	 		          echo "<td style='background-color:lightgrey;font-size:8pt;color:lightgrey;font-family:verdana;border:1pt solid darkgrey;'>.</td>";
	 		     	 }
           	 		  
	 		  }/// end for 
	 		 echo "</tr>"; 
             $dag--;
       	 		
	 		}  // end for
	 		
	 		  ?>
	 		</table>
			<br>
			<table>
			 <tr>
			 <td style='font-size:9pt;'> Betekenis: </td>
			 <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:black;border:1pt solid red;' 	> vandaag</td>
			 <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:blue;border:1pt solid blue;' data-toggle="tooltip" data-placement="bottom" title="Op deze dag heb ik reserveringen"  	> mijn dag</td>
			 <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:white;color:black;border:1pt solid black;'	> dag open</td>
			 <td  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;background-color:lightgrey;color:black;' 	                > dag in verleden</td>
			 <td  class="bg-secondary"  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;;color:white;'                    	> dag gesloten</td>
			 <td  class="bg-warning"  style='font-weight:bold;font-size:7pt;text-align:center;font-family:arial;padding:3pt;;color:black;'                      	> feestdag gesloten</td>
			</tr>
</table>			
   </center>
   </div>
   <?php
?> 