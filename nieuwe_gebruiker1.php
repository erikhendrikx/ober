<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">

<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OBER  Ontip Baan reservering</title>
<style>
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}
// for safari
.focus().setSelectionRange(0, 9999)
</style>
</head>
<body OnLoad="document.myForm.naam.focus();">
<?php
include('conf/mysqli.php'); 
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

?> 
 <div class= 'container'> 
 
 
 <h2><img src='images/ober.png' width=45> OBER  Ontip Baan Reservering </h2>
 <h3>Registratie nieuwe gebruiker</h3>
<form action="registratie_nieuwe_gebruiker.php" method=post  name='myForm'>
       <input type="hidden" name="zendform" value="1" /> 


  <div class="form-group">
    <label for="licentie">Naam</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="text" name = 'naam' class="form-control" id="naam" aria-describedby="naamHelp" placeholder="Type je Naam">
	 <small id="naamHelp" class="form-text text-muted">Type je volledige naam</small>
  </div>
   <div class="form-group">
    <label for="licentie">Toegangscode (Licentie)</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"   type="text" name ='licentie' class="form-control" id="licentie" aria-describedby="licentieHelp" placeholder="Type je NJBB licentie of ander uniek nummer">
	 <small id="licentieHelp" class="form-text text-muted">Gebruik uniek Licentie nummer of Lid nummer.</small>
  </div>
   <div class="form-group">
    <label for="licentie">Email</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="email" name= 'email' class="form-control" id="licentie" aria-describedby="emailHelp" placeholder="Type je email adres">
	 <small id="emailHelp" class="form-text text-muted">Op dit email adres krijg je een bevestiging van de registratie</small>
  </div>
  <div class="form-group">
  
      <label for="inputCity">Vereniging</label>
  
  <select id ="inputVereniging" name= 'vereniging_id'  class="form-control" aria-describedby="verenigingHelp">
        <option value ='' selected>Kies...</option>
		<?php
			$sql        = mysqli_query($con,"SELECT * From vereniging  order by Vereniging_nr  ")     or die(' Fout in select ver');  	
			   while($row = mysqli_fetch_array( $sql )) {?>
			    <option value = '<?php echo $row['Id'];?>'  ><?php echo $row['Vereniging_nr']." - ".$row['Vereniging'];?></option>
        <?php
               }
		?>
      </select>
	   <small id="verenigingHelp" class="form-text text-muted">Selecteer je vereniging uit de lijst met OnTip verenigingen.</small>
	</div>  
 
  <div class="form-group">
    <label for="exampleInputEmail1">PIN code</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="password" name = 'pin1' class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type een pincode">
 	 <small id="pinHelp" class="form-text text-muted">De PIN code moet uit 4 cijfers bestaan.</small> 
 </div>
  <div class="form-group">
    <label for="exampleInputEmail1">PIN code nogmaals</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"    type="password" name = 'pin2'  class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type zelfde pincode">
 	 <small id="pinHelp" class="form-text text-muted">Ter kontrole</small> 
 </div>
  
  <div class="form-group row">
    <label for="maxSplrs" class="col-sm-8 col-form-label">Ik zou graag Admin rechten willen om de verenigings gegevens te kunnen aanpassen<br><em>Eén admin per vereniging .Klik op de schakelaar </em></label>
     <div class="col-sm-2">
	 
	  <div class='form-check'>
		 <label class ="form-check-label"> 
        <input type="checkbox" name ='adminuser' data-width="150" data-height="35"  class='form-check-input'     data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Admin:Ja" data-off="Geen admin">
        </label>
	   </div ><!--- form check--->
	 </div> <!--- col--->
</div>  <!--- row--->


  
<br>
<?php
			$sql        = mysqli_query($con,"SELECT count(*) as Aantal from namen ")     or die(' Fout in select ver');  	
			$result     = mysqli_fetch_array( $sql );
			$aantal = $result['Aantal'];
			
			if ($aantal < 60){?>
                <button type="submit" class="btn btn-primary">Verzenden</button>
			<?php } else {?>
			<div > Er hebben zich nu <?php echo $aantal;?> personen aangemeld. Het maximale  aantal voor de proefperiode van een week is bereikt.<br>
			 Afhankelijk van de positieve ervaringen tijdens deze periode zal het aantal worden uitgebreid.</div>
			 <?php }?>
			
</form>
</div>
</body>
</html>
