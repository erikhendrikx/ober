<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">

 
<style type="text/css"  >
 .noprint {display:none;}     
	 

h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
//include('action.php');

if (!isset($_POST['datum'])){
	$datum         = $_GET['datum'];
	$vereniging_id = $_GET['ver_id'];
} else {
	$datum         = $_POST['datum'];
	$vereniging_id = $_POST['ver_id'];
}
 
$part          = explode("-", $datum);
$_datum        = strftime("%a %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 
$sql           = mysqli_query($con,"SELECT * from vereniging where Id = ".$vereniging_id."  ") or die('Fout in select');  
$result        = mysqli_fetch_array( $sql );
$vereniging    = $result['Vereniging'];
$today         = date('Y-m-d');
  
 $weeks = array();
  										  
 $qry_res_kalender  = mysqli_query($con,"SELECT WEEK(Datum) as Week, count(*) as Aantal From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id."  Group by Week order by Week ")	or die(' Fout in select kalender 1');  
  while($row = mysqli_fetch_array( $qry_res_kalender )) {
	  $weeks[] = $row['Week'].";".$row['Aantal'];

  }											

  // Vul hulp_stats_namen
 	$delete_query= "DELETE from `hulp_stats_namen`              
			    where Vereniging_id     = ".$vereniging_id." " ; 
     mysqli_query($con,$delete_query) or die ('Fout idelete hulp namen' );  
	
 
 
/// eerste is op datum, baan, begin reservering
  
$qry_res_kalender  = mysqli_query($con,"SELECT Datum, Baan, Begin_tijd_reservering From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id."  
										   Group by Datum, Baan, Begin_tijd_reservering
										   order by Datum, Baan, Begin_tijd_reservering  ")	or die(' Fout in select kalender 1');  
 
        while($row = mysqli_fetch_array( $qry_res_kalender )) {
             $insert_query = "INSERT INTO `hulp_stats_namen` (`Id`, `Vereniging_id`, Datum, Baan, Begin_tijd_reservering) 
	                              VALUES (NULL, ".$vereniging_id.", '".$row['Datum']."',".$row['Baan'].",'".$row['Begin_tijd_reservering']."' )";
         
	 	  mysqli_query($con,$insert_query) or die ('Fout insert hulp namen' );  
   }	// end while

// dan licenties toevoegen
 										  
$qry_hulp_naam  = mysqli_query($con,"SELECT * from hulp_stats_namen
                   			    		  where Vereniging_id = ".$vereniging_id."     ")	or die(' Fout in select kalender 1');  
 
      while($row1 = mysqli_fetch_array( $qry_hulp_naam )) { 
	  
	  $i=1;
	  $qry_res_kalender  = mysqli_query($con,"SELECT Licentie From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id."  
										   and Datum = '".$row1['Datum']."'
										   and Baan = ".$row1['Baan']."
										   and Begin_tijd_reservering = '".$row1['Begin_tijd_reservering']."' 
										    order by Licentie ")	or die(' Fout in select kalender 1');  
 		 
		 while($row2 = mysqli_fetch_array( $qry_res_kalender )) { 
	           $var = 'Licentie'.$i;
	           $update_query = "UPDATE `hulp_stats_namen` 
			                    SET ".$var."  = ".$row2['Licentie']." 
							    where Vereniging_id   = ".$vereniging_id."  
										   and Datum  = '".$row1['Datum']."'
										   and Baan   = ".$row1['Baan']."
										   and Begin_tijd_reservering = '".$row1['Begin_tijd_reservering']."' "; 
 										   
	 	  mysqli_query($con,$update_query) or die ('Fout insert hulp namen' );  
		  $i++;

	     }	// end while row2
	   }	// end while row1

 
  
 //$week_nr  = strftime("%W", mktime(0, 0, 0, $maand_nr , $dag, $jaar) );  
  
?>
<div class= 'container'> 
  <div class= "row  ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th d-print-none"    data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?ver_id=<?php echo $_GET['ver_id'];?>&datum=<?php echo $today;?>"   ></a>
  </div>
  
 
<div class="row">
  <div class ='col'>
 <h3> Baan reserveringen statistieken<h3>
  <h5>voor <?php echo $vereniging;?>   </h5>

 <h4>Aantal reserveringen per dag en per week</h4>
  
  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Datum</th>
      <th  >Week</th>
      <th  >Aantal</th>
   
    </tr>
	</thead>
	<?php
	
	   foreach ($weeks as $week_ext){ 
	
	  $part = explode(";", $week_ext);
	  $week        = $part[0];
	  $week_aantal = $part[1];
	
		 $qry_res_kalender  = mysqli_query($con,"SELECT Datum, count(*) as Aantal   From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id."  and Week(Datum) = ".$week." Group by Datum order by Datum ")	or die(' Fout in select kalender 2');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
			    $part          = explode("-", $row['Datum'] );
                $_datum       = strftime("%a %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
  
  ?>
	  <tr>
        	 <td><?php echo $_datum;?></td>
			 <td></td>
     	     <td><?php echo $row['Aantal'];?></td>
     	
     		
		</tr>
		<?php } // end while
		?>
		<tr>
	     <td>  </td>
		 <td><?php echo $week;?></td>
         <td><?php echo $week_aantal;?></td>
		</tr>
			<?php
		
	   }// end for
?>		
 </table>
 </section>
  <hr/>
  <h4>Aantal reserveringen per baan</h4>
  
  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Baan</th>
      <th  >Aantal</th>
   
    </tr>
	</thead>
	<?php
	$qry_res_kalender  = mysqli_query($con,"SELECT Baan, count(*) as Aantal   From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id."   Group by Baan order by Baan ")	or die(' Fout in select kalender 3');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
?>
	        <tr>
        	 <td><?php echo $row['Baan'];?></td>
	 	     <td><?php echo $row['Aantal'];?></td>
    		
		</tr>
		<?php } // end while
		?>
   </table>
	<hr/>
  
  <h4>Aantal reserveringen per persoon</h4>
  
   <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Toegangscode</th>
      <th  >Naam</th>
      <th  >Aantal</th>
   
    </tr>
	</thead> 
	<?php
	 										  
  	 $qry_res_kalender  = mysqli_query($con,"SELECT k.Licentie, Naam , count(*) as Aantal   From baanres_kalender as k 
	                                      join namen as n  on k.Licentie = n.Licentie
                   			    		  where k.Vereniging_id = ".$vereniging_id."  Group by k.Licentie  order by k.Licentie ")	or die(' Fout in select kalender 4');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
 
  ?>
	  <tr>
        	 <td><?php echo $row['Licentie'];?></td>
			 <td><?php echo $row['Naam'];?></td>
     	     <td><?php echo $row['Aantal'];?></td>
     	
     		
		</tr>
		<?php } // end while
		
		?>
		</table>
		
		<br>
		
	<h4>Aantal reserveringen per groep</h4>
  
   
   <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Namen</th>
      <th  >Aantal</th>
   
    </tr>
	</thead> 
	</tr>
	<?php
	
	$qry_hulp_naam  = mysqli_query($con,'SELECT concat(Licentie1,";", Licentie2,";",Licentie3,";",Licentie4) as Groep, count(*) as Aantal  FROM `hulp_stats_namen` 
	                     where Vereniging_id = '.$vereniging_id.'  Group by  Groep Order by Groep')	or die(' Fout in select kalender 1');  
 
     
      while($row1 = mysqli_fetch_array( $qry_hulp_naam )) { 
  ?>
 
  <?php
   $namen ='';
        $groep = $row1['Groep'];
		$aantal = $row1['Aantal'];
		
		$part = explode(";", $groep);
		
		foreach ($part as $licentie){
		
	       $qry_naam  = mysqli_query($con,"SELECT Naam from namen where Licentie = '".$licentie."'   ")	or die(' Fout in select namen');  
 		   $result     = mysqli_fetch_array( $qry_naam );
 		   $naam       = explode(" ", $result['Naam']);
		   if ($naam[0]!=''){
		   $namen      =  $namen.$naam[0].", ";
		   }
 		}?>
	 <tr>
		<td><?php echo substr($namen,0,-2);?></td>
		<td><?php echo $aantal;?></td>
	 </tr>
		
		<?php
 
	  }?>
	
	
	</table>
 
 
  
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  </div>   <!---- col---> 
  </div> <!---- row ----->
 </div>  <!---- container---->
 
 
<script>
$(document).ready(function(){
  $("#myBtn").click(function(){
    $('.toast').toast('show');
  });
});
</script>


 </body>
 </html>
 