<?php
# import_leden_xlsx_stap3.php
 
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 22mar2020         0.0.1       E. Hendrikx 
# Symptom:	        None.
# Problem:     	    None
# Fix:              None
# Feature:          PHPSpreadsheet
# Reference: 

// ivm include in import_inschrijvingen_xlsx_stap2.php niet actief
include('conf/mysqli.php');
setlocale(LC_ALL, 'nl_NL');
 $vereniging_id = 4;
 
$sql        = mysqli_query($con,"SELECT * From vereniging  where Id = ".$vereniging_id." ")     or die(' Fout in select ver1');  	
$result     = mysqli_fetch_array( $sql );
$vereniging = $result['Vereniging'];

function Addtime($currentTime, $minToAdd, $maxTime){
    	
    //The number of hours that you want
    //to Add from the date and time.
 	
	 //Convert those hours into seconds so
    //that we can Add them from our timestamp.
    $timeToAdd = ($minToAdd * 60);
 
     //Add the interval to Unix timestamp.
    $newTime = $currentTime +$timeToAdd;
	if ($newTime > $maxTime){
		 $newTime = $maxTime;
	}
	return   $newTime;
     }
	 
	 
require '../ontip/PHPspreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
 
// $xlsx_file = 'xlsx/reserveringen.xlsx';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// via include in import_inschrijvingen_xlsx_stap2.php
 
//$xlsx_file = 'xlsx/reserveringen.xlsx';

$inputFileName   = $xlsx_file;
$spreadsheet     = IOFactory::load($inputFileName);

$worksheet = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow         = $worksheet->getHighestRow(); // e.g. 10
$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

$kop     = $spreadsheet->getActiveSheet()->getCell('A1')->getValue();

if ($kop !='Reserveringen'){
	echo "Dit bestand komt niet overeen met de template."
	exit;
}


for ($i=3;$i < 100;$i++){

$baan = $spreadsheet->getActiveSheet()->getCell('C'.$i)->getValue();

if ($baan ==''){
	$i=1000;
	
} else {
	$value      = $spreadsheet->getActiveSheet()->getCell('A'.$i)->getValue();
    $_datum     = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($value);
    $datum      = date('Y-m-d',$_datum);
	$begin_tijd = $spreadsheet->getActiveSheet()->getCell('B'.$i)->getValue();
 	
    $baan       = $spreadsheet->getActiveSheet()->getCell('C'.$i)->getValue();
    $licentie1  = $spreadsheet->getActiveSheet()->getCell('D'.$i)->getValue();
    $licentie2  = $spreadsheet->getActiveSheet()->getCell('E'.$i)->getValue();
    $licentie3  = $spreadsheet->getActiveSheet()->getCell('F'.$i)->getValue();
    $licentie4  = $spreadsheet->getActiveSheet()->getCell('G'.$i)->getValue();
    
    // bepaal dagnaam
    $part = explode("-", $datum);
	$dag      = $part[2];
	$maand_nr = $part[1];
	$jaar     = $part[0];  
	$dag_naam = ucfirst(strftime("%A", mktime(0,0,0,$part[1],$part[2],$part[0]))  );
 	
	 // ophalen eindtijd  voor die dag
	 $qry        = mysqli_query($con,"SELECT * From baanres_speelmomenten where Vereniging_id = ".$vereniging_id."
                           and Dag_van_de_week = '".$dag_naam."'  and Begin_tijd = '".trim($begin_tijd)."' 	   ")	or die(' Fout in select eindtijd');  
     $result      = mysqli_fetch_array( $qry );
	 $begin_dag   = $result['Begin_tijd'];
	 $maxTime     = $result['Eind_tijd'];
	 
	  // bereken eind tijd tov tijd in Excel
	 $qry           = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$vereniging_id." ")	or die(' Fout in select vereniging cfg');  
     $result        = mysqli_fetch_array( $qry );
     $interval_min  = $result['Maximale_speelduur'];
 
 	 $begin       = $begin_tijd ;
	 $part        = explode (":", $begin);
	 $begin_GMT   = mktime($part[0],$part[1],$part[2]);
	 $part        = explode (":", $maxTime);
     $eind_GMT    = mktime($part[0],$part[1],$part[2]);
     $eind        = date("H:i",Addtime($begin_GMT ,$interval_min,$eind_GMT));
 
     ///  insert into kalender
	
	  for ($x=1;$x<5;$x++){
		  $var = 'licentie'.$x;
		  if ($$var !='' ){
			  
		 	$query = "INSERT INTO `baanres_kalender` (`Id`, `Vereniging_id`, `Licentie`, `Datum`, `Baan`, `Begin_tijd_reservering`, `Eind_tijd_reservering`, `Reserverings_nummer`) 
	          VALUES (NULL, ".$vereniging_id.", '".$$var."', '".$datum."',".$baan.", '".$begin."', '".$eind."', '1')";
  //    	      echo "<br>". $query;
     	   mysqli_query($con,$query) or die ('Fout in update reservering');   
		   $j++;
		  }// if $$var
	  } // for x
   }  // end if
  } // end for  i	

 

	?>
  <script language="javascript">
        alert("Er zijn <?php echo $j;?> reserveringen geimporteerd ")
    </script>
<br>
</fieldset>
 
