<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">


<style type="text/css">

h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
$pageName      = basename($_SERVER['SCRIPT_NAME']); 

$var           = $_GET['ver_id_md5'];
$sql           = mysqli_query($con,"SELECT * from vereniging where md5(Id) = '".$var."'  ") or die('Fout in select ver');  
$result        = mysqli_fetch_array( $sql );
$vereniging    = $result['Vereniging'];
$vereniging_id = $result['Id'];

$ip           = $_SERVER['REMOTE_ADDR'];
$sql          = mysqli_query($con,"SELECT Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0      = mysqli_fetch_array( $sql );  
$licentie     = $result0['Licentie'];
	 
// moet omgekeerd zijn!!
if (isset($_GET['kal_on'])){
  $kal           = 'kal_off';
}

 if (!isset($_GET['sort'])){
	$sort_key        =  'Licentie';
} else {
	$sort_key        = $_GET['sort'];

}

$part          = explode("-", $datum);
$_datum        = strftime("%A %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 
$qry   = mysqli_query($con,"SELECT * From namen where Vereniging_id=  ".$vereniging_id." order by ".$sort_key." ")	or die(' Fout in select namen');  

?>
<div class= 'container'> 
  <div class= "row  ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th  d-print-none"  data-toggle="tooltip" data-placement="bottom" title="Terug "      href="beheer_config.php?ver_id=<?php echo $vereniging_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&<?php echo $kal;?>"   ></a>
  </div>

  <FORM action="verwijder_gebruiker.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
    
<h3> Geregistreerde gebruikers <?php echo $vereniging;?>  <span  class='d-print-none'  ></h3>


		   <a tabindex="0" class="btn btn-sm btn-info" role="button" data-toggle="popover" data-html='true' data-trigger="focus"  data-placement="bottom"  title="Hoe verwijder ik een reservering ?" 
       data-content="Als je een gebruiker wilt verwijderen, zet dan een vinkje in de eerste kolom voor de naam die je wilt verwijderen en klik op de knop 'Verwijder geselecteerde ....'.   Een Super Admin (X) kan je nooit verwijderen.
	   <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Hoe verwijder ik een gebruiker?</a>
	   </span> </h3>

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th width=2%  class='d-print-none'  ><img src='images/trash_checked.png' width=20> </th>
      <th  >Nr</th>
       <th  ><a href="<?php echo $pageName;?>?ver_id_md5=<?php echo $_GET['ver_id_md5'];?>&sort=Licentie" target='_self'>Licentie</a></th>
       <th  ><a href="<?php echo $pageName;?>?ver_id_md5=<?php echo $_GET['ver_id_md5'];?>&sort=Naam" target='_self'>Naam</a> </th>
       <th  >Admin</th>
    </tr>
	</thead>
	<?php
	$i=1;
		while($row = mysqli_fetch_array( $qry )) {
				 $disable_option = ''; 
			 
		 if ($row['Licentie'] ==  $licentie or $row['Admin_user'] == 'X'){
			 $disable_option = 'disabled';
		 }
	
	    switch($row['Admin_user']){
			case "X" : $admin_user = 'Super';break;
			case "J" : $admin_user = 'J';break;
			default  : $admin_user = '-';break;
		}
       
		 
		 ?>
		 
		 
	  <tr>
	    <td  class='d-print-none  has-danger'>        
		 <input type='checkbox' name= 'delete[]' <?php echo $disable_option;?>    class='trash '   id= 'trash_<?php echo $row['Id']; ?>' value='<?php echo $row['Id']; ?>' /><label for="trash_<?php echo $row['Id']; ?>"></label>	
       </td>                 
		
	    <td><?php echo $i;?></td>
	    <td><?php echo $row['Licentie'];?></td>
	    <td><?php echo $row['Naam'];?></td>
		<td><?php echo $admin_user;?></td>
 	</tr>
		<?php 
		$i++;
		} // end while
?>		
		
		
 </table>
 <div class="d-print-none">
       <input type="submit" class="btn btn-info" value="Verwijder geselecteerde gebruikers">
</div>
 
  </form>
   	    <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
  <br><br><hr>
   
 	  <h4 id='toevoegen_gebruiker' class='d-print-none' style='color:teal;'>Toevoegen gebruikers zonder computer</h4>
 
  <br>
  <FORM action="toevoegen_gebruiker.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
 
  
  <div class="form-group d-print-none row">
    <label for="Naam" class="col-4 col-form-label">Naam</label>
     <div class="col-6  ">
      <input type="text" class="form-control   " aria-describedby="naamHelp"  id="naam" name ='naam' placeholder="Type de Naam" /> 
      <small id="naamHelp" class="form-text text-muted">Type de volledige naam</small>
   </div>
  </div>
  
   <div class="form-group d-print-none row">
    <label for="Licentie" class="col-4 col-form-label">Toegangscode</label>
     <div class="col-6">
      <input type="text" class="form-control   " aria-describedby="licentieHelp"  id="licentie" name ='licentie' placeholder="Type NJBB licentie of ander uniek nummer" /> 
 <small id="licentieHelp" class="form-text text-muted">Gebruik uniek Licentie nummer of Lid nummer.</small>
  </div>
  </div>
  
   
  
  <div class="form-group d-print-none  row">
     <div class = "offset-md-2 col-auto">
      <button   type="submit" class="btn btn-primary d-print-none">Verzenden</button>
    </div>
   </div>
   
 </div>  <!---- container---->
 
<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})
</script>

 </body>
 </html>
 