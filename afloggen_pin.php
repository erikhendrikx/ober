<?php
# afloggen_pin.php
# Reset aanlog op deze PC
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
#
# 18okt2018          1.0.1            E. Hendrikx
# Symptom:   		    None.
# Problem:       	  None.
# Fix:              Opgelost
# Feature:          None.
# Reference: 
#
$licentie  =  $_GET['licentie'];
//header("Location: ".$_SERVER['HTTP_REFERER']);
ob_start();
function redirect($url) {
    if(!headers_sent()) {
        //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    } else {
        //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}

include 'conf/mysqli.php'; 
 $update_query= "UPDATE `namen` 
	              SET Aangelogd    = 'N',
				      IP_adres     = '',
					  Laatst       = now() 
				  where Licentie = '".$licentie."' and Aangelogd ='J'  "; 
				  
				  	//echo "<br>".$update_query;
    mysqli_query($con,$update_query) or die ('Fout in update namen');  



if ($url ==''){
 $url = "index.html";
}
redirect($url);

ob_end_flush();
?>
