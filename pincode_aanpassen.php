<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OBER  Ontip Baan reservering</title>
<style>
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}
h6{
  font-size: 1.2vh;
}
</style>
</head>
<body OnLoad="document.myForm.naam.focus();">
<?php
include('conf/mysqli.php'); 
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

?> 
 <div class= 'container'> 
 
 
 <h2><img src='images/ober.png' width=45> OBER  Ontip Baan Reservering </h2>
 <h3>Aanpassen PIN code</h3>
<form action="muteren_pincode.php" method=post  name='myForm'>
       <input type="hidden" name="zendform" value="1" /> 


   <div class="form-group">
    <label for="licentie">Toegangscode</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"   type="text" name ='licentie' class="form-control" id="licentie" aria-describedby="licentieHelp" placeholder="Type je toegangscode">
	 <small id="licentieHelp" class="form-text text-muted">Gebruik de code die je bij registratie hebt ingevoerd.</small>
  </div>
   
  <div class="form-group">
    <label for="exampleInputEmail1">Nieuwe PIN code</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');" type="password" name = 'pin1' class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type een pincode">
 	 <small id="pinHelp" class="form-text text-muted">De PIN code moet uit 4 cijfers bestaan.</small> 
 </div>
  <div class="form-group">
    <label for="exampleInputEmail1">PIN code nogmaals</label>
    <input autofill="off" autocomplete="off"  readonly  
            onfocus="this.removeAttribute('readonly');"    type="password" name = 'pin2'  class="form-control" id="exampleInputEmail1" aria-describedby="pinHelp" placeholder="Type zelfde pincode">
 	 <small id="pinHelp" class="form-text text-muted">Ter kontrole</small> 
 </div>
<br>
<h6>Na verzending van dit formulier ontvangt u op het geregistreerde email adres een bevestiging met een link om de nieuwe PIN te activeren.</h6>

<br>
   <div class ="btn-group mb2">
		  <button type="submit" class="btn btn-sm  btn-primary active">Verzenden nieuwe PIN</button>
	      <a class="btn btn-secondary btn-sm" rol='button' style='color:white;'   href="nieuwe_gebruiker.php" tabindex="-1" aria-disabled="false">Nieuwe gebruiker</a>
 </div>
			
</form>
</div>
</body>
</html>
