<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="//bootstrap-extension.com/css/4.5.1/bootstrap-extension.min.css" type="text/css">
<script src="//bootstrap-extension.com/js/4.5.1/bootstrap-extension.min.js"></script>



<link href="css/bootstrap.min.css" rel="stylesheet">

<title>OnTip Baanreservering</title>

<style type="text/css">

td, th {padding:5pt;}

 input.baan0[type=checkbox] {
    display:none;
  }
  
 input.baan1[type=checkbox] {
    display:none;
  }
 input.baan2[type=checkbox] {
    display:none;
  }
 input.baan3[type=checkbox] {
    display:none;
  }
 input.baan0[type=checkbox] {
    display:none;
  }
 input.baan[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }

 input.baan0[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
 input.baan1[type=checkbox] + label
   {
	   background:url('images/x1.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
 
 input.baan2[type=checkbox] + label
   {
	   background:url('images/x2.png') no-repeat;
       height: 26px;
       width: 15px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
  
input.baan3[type=checkbox] + label
   {
	   background:url('images/x3.png') no-repeat;
       height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }  

 
input.baan0[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan1[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan2[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

input.baan3[type=checkbox]:checked + label
    {
	background:url('images/icon_checked.png') no-repeat;
         height: 25px;
         width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }

.thick_line_top     {border-top:4pt solid black;}
.thick_line_bottom  {border-bottom:4pt solid black;}


h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 
smbut {
  font-size: 1.2vh;
}

.table a
{
    display:block;
    text-decoration:none;
}


</style>
<script src="jquery.disable-autofill.js"></script>
<script src="../ontip/js/utility.js"></script>
<script src="../ontip/js/popup.js"></script>


<script language="JavaScript">
function changeDatum(id, datum) {
    // document.getElementById(id).innerHTML = datum;
    document.getElementById(id).value = datum;
    document.getElementById(id).style.color= "red";
   }
   
function ShowHide(id) {
    var x = document.getElementById(id);
    var y = document.getElementById('switch');
   
   
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
       
    }
}
</script>


</head>


<body >
<?php
include('conf/mysqli.php');
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');
$pageName = basename($_SERVER['SCRIPT_NAME']);

$aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}


$_datum ='..';
function Addtime($currentTime, $minToAdd, $maxTime){
    	
    //The number of hours that you want
    //to Add from the date and time.
	
	$part        = explode (":", $begin_tijd);
	//$currentTime = mktime($part[0], $part[1],$part[2]);
	
	 //Convert those hours into seconds so
    //that we can Add them from our timestamp.
    $timeToAdd = ($minToAdd * 60);
 
     //Add the interval to Unix timestamp.
    $newTime = $currentTime +$timeToAdd;
	if ($newTime >= $maxTime){
		 $newTime = $maxTime;
	}
	return   $newTime;
     }
 
 if (!isset($_GET['datum']) or $_GET['datum'] == '' ){
  $datum = date('Y-m-d');
 }

if (isset($_GET['datum']) and $_GET['datum'] !='' ){
	//  2020-05-18
    $datum  = $_GET['datum'];
}
$ver_id = $_GET['ver_id'];

$part   = explode("-", $datum);
$_datum = strftime("%A %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
$datum_dag_naam = ucfirst(strftime("%A", mktime(0,0,0,$part[1],$part[2],$part[0]))  );

if (isset($_GET['ver_id'])){
   $sql          = mysqli_query($con,"SELECT * From baanres_config where Vereniging_id = ".$_GET['ver_id']."  ")     or die(' Fout in select config');  	
   $result           = mysqli_fetch_array( $sql) ;
   $interval_min     = $result['Maximale_speelduur'];
   $aantal_banen     = $result['Aantal_banen'];
   $max_aantal_splrs = $result['Max_aantal_spelers'];
   $total_cols       = $aantal_banen+2;
   $vereniging_id    = $_GET['ver_id'];

$sql        = mysqli_query($con,"SELECT Begin_tijd, Eind_tijd From baanres_speelmomenten where Dag_van_de_week = '".$datum_dag_naam."' and Vereniging_id = ".$vereniging_id."  ")     or die(' Fout in select config');  	
$periode = ""; 
  while($row = mysqli_fetch_array( $sql )) {
	 $begin_tijd  = $row['Begin_tijd'];	
     $eind_tijd   = $row['Eind_tijd'];	
	 $periode .= "[".$begin_tijd."-".$eind_tijd."] ";
  }	
	
  $qry   = mysqli_query($con,"SELECT * From vereniging where Id = ".$_GET['ver_id']." ")	or die(' Fout in select vereniging');  
  $result = mysqli_fetch_array( $qry );
  $vereniging = $result['Vereniging'];

 $qry   = mysqli_query($con,"SELECT * From namen where Vereniging_id = ".$_GET['ver_id']." and Licentie ='".$licentie."'  ")	or die(' Fout in select vereniging');  
  $result = mysqli_fetch_array( $qry );
  $naam_licentie = $result['Naam'];

  
 if (isset($_GET['licentie'])){
	$licentie = $_GET['licentie'];
}
}// end get ver

	// default button clickbaar of niet
	$list_disabled_button  = 'disabled';
	$list_disabled_href    = 'aria-disabled="true"';
    $admin_disabled_button = 'disabled';
	$admin_disabled_href   = 'aria-disabled="true"';
	
     if ( isset($_GET['datum']) ){
	 	$list_disabled_button = '';
	    $list_disabled_href   ='aria-disabled="false"';
	 }
	 
	 if ($admin_user =='J'){
	    $admin_disabled_button = '';
	    $admin_disabled_href   = 'aria-disabled="false"';
	 }
	
     if (isset($_GET['kal_off'])){ 
	     $kal      = 'kal_on';
		 $curr_kal = 'kal_off';
	 } else {
		 $kal      = 'kal_off'; 
	     $curr_kal = 'kal_oon';
	 }
	
// tabel scroll waarden

if (isset($_GET['start'])){
	$start = $_GET['start'];
} else {
	$start     = 1;
}

if ($start < 1){
	$start    = 1;
}

$max_width = 10;  // aantal banen naast elkaar
if ($max_width > $aantal_banen){
	$max_width = $aantal_banen;
}

$einde      = $start + ($max_width-1);

if ($einde > $aantal_banen){
	$einde = $aantal_banen;
}
// Gelijk houden van breedte
$dif = $einde-$start;

if ($dif < $max_width){
	$start = $einde - ($max_width-1);
}
/*
echo "<br>start ".$start;
echo "<br>einde ".$einde;
echo "<br>max ".$max_width;
echo "<br>banen ".$aantal_banen;
*/

?>

 
<div class="container"  > 
 

  <div class= "row  align-content-right">
     <div class="col-12 text-center fixed-top">
	
	 <!----   buttons in de kop------------------------>
 	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-user  mx-auto   <?php echo $list_disabled_button;?>"  data-toggle="tooltip" data-placement="bottom" title="Mijn reserveringen "                     role="button" <?php echo $list_disabled_href;?>  href="lijst_reserveringen.php?datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&ver_id=<?php echo $_GET['ver_id'];?>"   ></a>
	 <a  class="btn btn-secondary glyphicon glyphicon-list-alt btn-lg <?php echo $admin_disabled_button;?>" data-toggle="tooltip" data-placement="bottom" title="Reserveringen voor <?php echo $datum;?>" role="button" <?php echo $admin_disabled_href;?> href="lijst_reserveringen_datum.php?ver_id=<?php echo $_GET['ver_id'];?>&datum=<?php echo $datum;?>"  tabindex="-1"></a>
	 <a  class="btn btn-secondary glyphicon glyphicon-cog btn-lg      <?php echo $admin_disabled_button;?>" data-toggle="tooltip" data-placement="bottom" title="Aanpassen vereniging gegevens"           role="button" <?php echo $admin_disabled_href;?> href="beheer_config.php?ver_id=<?php echo $_GET['ver_id'];?>"   tabindex="-1"></a>
     <a  class="btn btn-secondary glyphicon glyphicon-calendar btn-lg " data-toggle="tooltip" data-placement="bottom" title="Kalender aan/uit" href="<?php echo $pageName;?>?ver_id=<?php echo $_GET['ver_id'];?>&licentie=<?php echo $licentie;?>&<?php echo $kal;?>" ></a>
	 <a  class="btn btn-secondary glyphicon glyphicon-education btn-lg " data-toggle="tooltip" data-placement="bottom" title="Handleiding" href="uitleg.php?ver_id=<?php echo $_GET['ver_id'];?>" ></a>
 	 <a  class="btn btn-secondary glyphicon glyphicon glyphicon-edit btn-lg" data-toggle="tooltip" data-placement="bottom" title="Profiel aanpassen" href="profiel_aanpassen.php?ver_id=<?php echo $_GET['ver_id'];?>&licentie_md5=<?php echo $licentie_md5;?>" ></a>
 	 <a  class="btn btn-secondary glyphicon glyphicon-log-out btn-lg "   data-toggle="tooltip" data-placement="bottom" title="Afmelden"    href="afloggen_pin.php?licentie=<?php echo $licentie;?>"  tabindex="-1"></a>
    </div> <!-- col-->    
  </div>  <!--- row---->
  <br>
  <br>

  <br>
   
	
  <!--div class="row d-none d-md-block">
	  <div class="col-12 text-left"> 
	  
	     	  <h3 class="d-none d-md-block "><img src="images/ober.png" width="40vw"   alt="" loading="lazy"> OBER - Ontip Baan Reservering </h3>
       
        </div>
	</div-->
<div class= "row">
   <div class="col-12 text-center">
       <?php
          if (isset($_GET['ver_id']) and !isset($_GET['kal_off']) ){
              include("include_kalender1.php");
          }
    	?>
     </div> <!---- col--->
  </div>  <!-- row ----> 
   
  <div class="row">
   <div class="col" style= 'font-size:9pt;'>
   <br>
   <hr>
   
  <?php 
if (isset($datum)) {?>
        <h4 class='text-center'  ><b>Aanmaken baan reserveringen	
		   <span    >
		   <a tabindex="0" class="btn btn-sm btn-info" role="button" data-toggle="popover" data-placement="bottom"  title="Hoe reserveer ik een baan?" 
       data-content="Klik in het onderstaande schema op een keuze van tijdstip en baan om een reservering te maken.Dat kan ook als er al een reservering voor die baan bestaat. Er kunnen maximaal 4 personen per baan een reservering aanmaken. 
	   Het aantal reserveringen per baan wordt aangegeven met de oranje icoontjes met 1..3.
		 Klik daarna op de knop onderaan.[Klik op '* Hoe reserveer ik ...'' om deze uitleg te sluiten]">* Hoe reserveer ik een baan?</a>
	   </span></b></h4>
		 <h5 class='text-center  '>
		Met behulp van onderstaande tabel kunnen banen worden gereserveerd. Maximaal 4 personen per baan. Maximaal <?php echo $max_aantal_splrs;?> personen op speeldveld.<br>
  
'  </h5>
   <FORM action="insert_reservering.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $_GET['ver_id'];?>" /> 
        <input type="hidden" name="datum" value="<?php echo $datum;?>" /> 
        <input type="hidden" name="speelduur" value="<?php echo $interval_min;?>" /> 
        <input type="hidden" name="max_splrs" value="<?php echo $max_aantal_splrs;?>" /> 
		<input type="hidden" name="url" value="<?php echo $pageName;?>" /> 
		<input type="hidden" name="kal" value="<?php echo $curr_kal;?>" /> 
		
		
     <?php
        // de licentie wordt al ingevuld. Alleen admin users kunnen dit overrulen
        if ($admin_user != 'J'){
	        $user_option = 'disabled';
			?>
			<input type="hidden" name="licentie" value="<?php echo $licentie;?>" /> 
			<?php
         } ?>
	
	  
 <div class="table-responsive-md"> 	  
   <table width=100% class="col-12 table table-hover  border border-right-secondary " style= 'border:1pt solid grey;'  >
  <thead>
  <tr class='bg-primary'>
      <th colspan =<?php echo $total_cols;?> ><h5>Baanreservering voor periodes van <?php echo $interval_min;?> minuten. Maximaal <?php echo $aantal_banen;?> banen beschikbaar.</h5></th>
   
    </tr>
  </thead>
   <thead>
    <tr  >
      <th width=20% colspan =2 style='border-right:1pt solid darkgrey;'><h5><b>Licentie</b></h5></th>
	   <th class="border border-right-secondary"  colspan =<?php echo $max_width+4;?> ><h4><input <?php echo  $user_option;?> type='text' name='licentie' value ="<?php echo $licentie;?>"  size = 7>&nbsp<?php echo $naam_licentie ;?> </h4> </th>
  	</tr>
  </thead> 
  <thead>
    <tr  >
      <th width=20% colspan =2 style='border-right:1pt solid darkgrey;' ><h5><b>Datum</b></h5></th>
      <th class="border border-right-secondary"   colspan =<?php echo $max_width+4;?> ><h5><b> <?php echo $_datum; ?>   <?php echo $periode;?></b></h5></th>
    </tr>
  </thead> 
  <tr>
  <?php
    if  (isset($datum)){
		
	?>
   
    <thead>
	 <tr   >
      <th colspan =2 ><span    >
		   <a tabindex="0" width=10% class="btn btn-sm btn-info" role="button" data-toggle="popover" data-placement="bottom"  title="Scroll baan nummers" 
       data-content="Hiernaast staan knoppen met pijlen naar links en rechts. Hiermee kan je scrollen naar hogere of lagere baan nummers. Met de knoppen aan de uiteinden kan je direct naar het begin of het einde springen.
	   [Klik op '* Scrollen' om deze uitleg te sluiten]">* Scrollen</a>
	   </span></th>
	
       <!-- button om terug te bladeren--->
      
      <th style="width:1%;"  class = "bg-secondary">
	   	 <a  class="btn btn-secondary glyphicon glyphicon glyphicon-fast-backward btn-sm "   title="Bgin"   
         		 href="<?php echo $pageName;?>?ver_id=<?php echo $ver_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=1&kal_off"  tabindex="-1"></a>
 	  </th>
	   <th style="width:1%;" class = "bg-secondary">
	   	 <a  class="btn btn-secondary glyphicon glyphicon-triangle-left btn-sm "   title="Terug"   
         		 href="<?php echo $pageName;?>?ver_id=<?php echo $ver_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo $start-1;?>&kal_off"  tabindex="-1"></a>
 	  </th>
	  
	   <th  class= 'bg-velvet text-center truncate-md' colspan=<?php echo $max_width-4;?> style='color:white;'> <h4>Baankeuze 1 - <?php echo $aantal_banen;?></h4></th>
		 
       
	  <!-- button om verder te bladeren--->
	 <th width= 1% class = "bg-secondary" >
   	    <a  class="btn btn-secondary glyphicon glyphicon-triangle-right btn-sm mr-3"    title="Verder"   
         		 href="<?php echo $pageName;?>?ver_id=<?php echo $ver_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo $start+1;?>&kal_off"  tabindex="-1"></a>
 	 </th>
     <th width= 1% class = "bg-secondary" >
   	    <a  class="btn btn-secondary glyphicon glyphicon glyphicon-fast-forward btn-sm "    title="Einde"   
         		 href="<?php echo $pageName;?>?ver_id=<?php echo $ver_id;?>&datum=<?php echo $datum;?>&licentie=<?php echo $licentie;?>&start=<?php echo ($aantal_banen-$max_width)+1;?>&kal_off"  tabindex="-1"></a>
 	 </th>
    </tr>
   </thead> 
   </table>
   
     <table class=" col-12 table table-striped table-hover thick_line_bottom border border-right-secondary" style= 'border:1pt solid grey;'  >
 
     <thead>
	 <tr   >
      <th style='border:0px;' class='glyphicon glyphicon-time'> 
	   </th>
	  <th width=5% class='bg-warning ' data-toggle="tooltip" data-placement="bottom" title="Aantal reserveringen/Max aantal personen"  style='font-size:8pt;'># Res</th>
   
	 
	   <?php
	   /// Aantal banen
	    for ($i=$start;$i<=$einde;$i++){
			/// afwijkende baan nummers
		    $qry_baan  = mysqli_query($con,"SELECT * From baanres_baan_nummers where Vereniging_id = ".$_GET['ver_id']." and Baan = ".$i."    ")	or die(' Fout in select baan afw');  
            $result2   = mysqli_fetch_array( $qry_baan);
			$baan_nr    = $result2['Baan_afw_nr'];
			if ($baan_nr ==''){
				$baan_nr = $i;
			}
	
			?>
		  <th  class= 'bg-velvet' style=' color:white;text-align:center;'><h4><?php echo $baan_nr;?><h4></th>
		<?php	
	   }
     ?>
	 </tr>
	   </thead> 
   
  		<?php
		// speelmomenten in eerste kolom. 
	 		  $sql        = mysqli_query($con,"SELECT * From baanres_speelmomenten where Dag_van_de_week = '".$datum_dag_naam."' and Vereniging_id = ".$_GET['ver_id']."  ")     or die(' Fout in select config');  	
			
               while($row = mysqli_fetch_array( $sql )) {
                $begin      = $row['Begin_tijd'];
			    $eind       = $row['Eind_tijd'];
				
				$part        = explode (":", $begin);
                $begin_GMT   = mktime($part[0],$part[1],$part[2]);
				$part        = explode (":", $eind);
                $eind_GMT    = mktime($part[0],$part[1],$part[2]);
                $interval    = $interval_min * 60;
				?>
				  <input type="hidden" name="eind_tijd" value="<?php echo $eind_GMT;?>" /> 
				  <?php
				
			    // per speelmoment de intervallen van 45 min bepalen
				
				for ($x=$begin_GMT;$x<$eind_GMT;$x=$x+$interval){
					$vanaf = date("H:i",$x);
				?>
				   <tr>
				    <th scope = 'row' ><h5><b>
	                   <?php echo $vanaf."-".date("H:i",Addtime($x,$interval_min,$eind_GMT) );?>
					   </b></h5>
		            </th>  <!---- col--->
			     
                    <th style='font-size:8pt;border-right:1pt solid darkgrey;border-left:1pt solid darkgrey;'>
					<?php
					$current_aantal =0;
					
					 $sql_kal = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender where 
					             Datum = '".$datum."' 
								 and Vereniging_id = ".$_GET['ver_id']." 
								 and Begin_tijd_reservering = '".$vanaf."' ")     or die(' Fout in select kalender');  	

                     $result  = mysqli_fetch_array( $sql_kal  );
                     $current_aantal = $result['Aantal'];
					 
					 //  aantal huidige reservingen / max aantal
					 echo $current_aantal."/".$max_aantal_splrs;
					 
					 ?>
					</th>
					 
				 
					<?php
					//van start tot einde
					
					 for ($i=$start;$i<=$einde;$i++){?>
					 <td style='vertical-align:middle;text-align:center;'  >
					   <?php
					    $sel_datum = $_GET['datum'];
						/// afwijkende baan nummers
		                $qry_baan  = mysqli_query($con,"SELECT * From baanres_baan_nummers where Vereniging_id = ".$_GET['ver_id']." and Baan = ".$i."    ")	or die(' Fout in select baan afw');  
                        $result2   = mysqli_fetch_array( $qry_baan);
			            $baan_nr    = $result2['Baan_afw_nr'];
			            if ($baan_nr ==''){
				            $baan_nr = $i;
			            }	
						 
						    $qry_res_baan  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   							where Vereniging_id = ".$_GET['ver_id']." and Datum = '".$sel_datum."'
											and Baan = ".$baan_nr." 
											and Begin_tijd_reservering = '".$vanaf."'     ")  
								or die(' Fout in select reserv');  	  
														
						     $result  = mysqli_fetch_array( $qry_res_baan  )		;
							 $aantal  = 0 ;
							 $aantal  = $result['Aantal'] ;
							 
							 $var = 'icon_'.$result['Aantal'].'.png';
							 if ($result['Aantal'] == 4){?>
								<img onclick="ShowHide('spelers_<?php echo $x."_".$baan_nr;?>')" src='images/icon_bezet.png' width=25>
								<?php
							 } else { 
      	                      ?>
							  
 					<span data-toggle="tooltip" data-placement="bottom" title="al <?php echo $aantal;?> reservering(en) voor baan <?php echo $baan_nr;?> om <?php echo $vanaf;?>.Klik op 'Mijn reserveringen' op te zien wie dat zijn.">	  
  		            <input type='checkbox' name= 'baan_tijdstip[]' class='baan<?php echo $aantal;?>'   id= 'baan_<?php echo $x."_".$baan_nr;?>' value='<?php echo $baan_nr.";".$vanaf; ?>' /><label for="baan_<?php echo $x."_".$baan_nr;?>"></label>	
                      </span>          		
   	                  <?php } ?>
								 
					 </td>  <!---- col---> 		
	               <?php 
					 }
					?> 
				
				</tr> <!--- row---> 	
				<?php
				} // end for x
			}  // end for speelmoment
  
  ?>
   </table>
   <h6 class='text-center'>	 
  </span>
      <img src='images/backup_not_checked.png' width=25>  geen reserveringen |
	  <img src='images/icon_1.png' width=35>  1 reservering |
	  <img src='images/icon_2.png' width=35>  2 reserveringen |
	  <img src='images/icon_3.png' width=33>  3 reserveringen  |
	  <img src='images/icon_bezet.png' width=25>  4 reserveringen(max) 
	</h6>  
   </div> <!---  div responsive---->
   
   <table width=99%>
   <tr> 
   <td class= 'text-left'>
    <span><a tabindex="0" width=10% class="btn btn-sm btn-warning" role="button" data-toggle="popover" data-placement="top"  title="Afwijkende baan nummers" 
       data-content="Het kan zijn dat de baan nummers niet oplopen van 1,2,3 enz. Dat komt dan omdat de vereniging gebruik maakt van afwijkende baan nummers, zodat je direct weet waar je moet spelen.
	   [Klik op '* Baan ...' om deze uitleg te sluiten]">* Baan nummering</a></span>
	   </td>
	<td style='text-align:right;'>
      <input type="submit" class="btn btn-info btn-sm" value="Verzenden">
     </td>
	 </tr>
	 </table>
   </form>
    </div> <!---- col--->
   </div>  <!-- row ----> 
   
   <?php
     }// end if GET
	}// isset datum
 ?>
 
 </div>  <!--- container -->
 <script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})
 $("#fake").click(function() {
  $("html").toggleClass("csstransforms");
})
</script>

</body>
</html>
