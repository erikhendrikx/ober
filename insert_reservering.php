<?php

# insert_reservering.php
# Toevoegen van een baan reservering naar de baanres_kalender 
# aangeroepen vanuit  main.php
# POST  parameters:
#     $datum        
#     $vereniging_id
#     $licentie     
#     $reserveringen 			 array voor baan/tijdstip
#     $interval_min   			 duur reservering
#     $maxTime      
#     $max_splrs    
#     $url          
#     $kal   
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#
// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

function Addtime($currentTime, $minToAdd, $maxTime){
    	
    //The number of hours that you want
    //to Add from the date and time.
	
	$part        = explode (":", $begin_tijd);
	//$currentTime = mktime($part[0], $part[1],$part[2]);
	
	 //Convert those hours into seconds so
    //that we can Add them from our timestamp.
    $timeToAdd = ($minToAdd * 60);
 
     //Add the interval to Unix timestamp.
    $newTime = $currentTime +$timeToAdd;
	if ($newTime > $maxTime){
		 $newTime = $maxTime;
	}
	return   $newTime;
     }
	 
include('conf/mysqli.php'); 
//include('action.php');

$datum             = $_POST['datum'];
$vereniging_id     = $_POST['vereniging_id'];
$licentie          = $_POST['licentie'];
$reserveringen     = $_POST['baan_tijdstip'];
$part              = explode ("-", $datum);
$maand_nummer      = $part[1];
$jaar              = $part[0];

$interval_min      = $_POST['speelduur'];
$maxTime           = $_POST['eind_tijd'];
$max_splrs         = $_POST['max_splrs'];
$max_freq_per_dag  = $_POST['max_freq_per_dag'];
$max_freq_per_week = $_POST['max_freq_per_week'];
$url               = $_POST['url'];
$kal               = $_POST['kal'];

if ($licentie ==''){
	$message .= "* Er is geen toegangscode ingevuld.<br>";
	$error = 1;
}
//header("Location: ".$_SERVER['HTTP_REFERER']  );

/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1


foreach($reserveringen as $reservering){
	$part        = explode(";", $reservering);
	$baan        = $part[0];
	$begin       = $part[1];
	$part        = explode (":", $begin);
	$begin_GMT   = mktime($part[0],$part[1] );
    $eind        = date("H:i",Addtime($begin_GMT ,$interval_min,$maxTime));
  											
												
// kontrole of er voor hetzelfde tijdstip al een reservering op een andere baan is
    $qry_res_splrs  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Datum = '".$datum."'
												and Licentie  = '".$licentie."'
							    				and Begin_tijd_reservering = '".$begin."'     ")  
							    	or die(' Fout in select reserv kalender 1');  	  

  $result  = mysqli_fetch_array( $qry_res_splrs );
  $aantal  = $result['Aantal'];
  //echo  "aantal ".$aantal;
  
  if ($aantal > 0){
	$message .= "* Er is voor ".$begin." al een reserving voor ".$licentie." ingevuld op een andere baan.<br>";
	$error = 1;
  }

/// controle op aantal per dag

												
 $qry_res_splrs  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Datum           = '".$datum."'
												and Licentie        = '".$licentie."' ")  
							    	or die(' Fout in select reserv kalender 2');  	  

  $result  = mysqli_fetch_array( $qry_res_splrs );
  $aantal  = $result['Aantal'];
  
   
 if ($aantal == $max_freq_per_dag){
	 
	$message .= "* Aantal reserveringen voor ".$licentie." hoger dan toegestaan DAG maximimum.<br>";
	$error = 1;
}

  // controle op aantal per week
  $part         = explode("-", $datum );
  $week        = strftime("%W",mktime(0,0,0,$part[1],$part[2],$part[0]));
   
  $qry_res_splrs  = mysqli_query($con,"SELECT count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Week(Datum)     =  ".$week."
												and Licentie        = '".$licentie."' ")  
							    	or die(' Fout in select reserv kalender 2');  	  

  $result  = mysqli_fetch_array( $qry_res_splrs );
  $aantal  = $result['Aantal'];

 if ($aantal == $max_freq_per_week){
	$message .= "* Aantal reserveringen voor ".$licentie." hoger dan toegestaan WEEK maximimum.<br>";
	$error = 1;
}
 
if ($error ==0){
// controle op max aantal spelers per tijdstip
   $qry_max_splrs  = mysqli_query($con,"SELECT Begin_tijd_reservering, count(*) as Aantal From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." 
												and Datum = '".$datum."'
												and Begin_tijd_reservering = '".$begin."'    ")  
							    	or die(' Fout in select reserv kalender 2');  	  
  $result  = mysqli_fetch_array( $qry_max_splrs );
  $aantal = $result['Aantal'];
  
   if ($aantal == $max_splrs){
	$message .= "* Er zijn voor ".$begin." al ".$max_splrs." reserveringen. Meer is niet toegestaan!<br>";
	$error = 1;
       }
   
   if ($error ==0 ){
	   	$query = "INSERT INTO `baanres_kalender` (`Id`, `Vereniging_id`, `Licentie`, `Datum`, `Baan`, `Begin_tijd_reservering`, `Eind_tijd_reservering`, `Reserverings_nummer`) 
	          VALUES (NULL, ".$vereniging_id.", '".$licentie."', '".$datum."',".$baan.", '".$begin."', '".$eind."', '1')";
      //	echo "<br>". $query;

 	mysqli_query($con,$query) or die ('Fout in update reservering');   
   }
}

} // foreach

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1

?>
<script type="text/javascript">
		window.location.replace("<?php echo $url;?>?datum=<?php echo $datum;?>&jaar=<?php echo $jaar;?>&maand_nr=<?php echo $maand_nummer;?>&licentie=<?php echo $licentie;?>&ver_id=<?php echo $vereniging_id;?>&<?php echo $kal;?>");
</script>


	
	


