<?php
# update_config.php
# aanpassen instellingen
# aangeroepen vanuit  main.php knoppen balk
 
#        
# Record of Changes:
# Date              Version      Person
# ----              -------      ------
# 24mei2020         1.0.1            E. Hendrikx
# Symptom:   		 None.
# Problem:      
# Fix:          
# Feature:      
# Reference: 
#

// Controles
$error   = 0;
$message = '';


if (!isset($_POST['zendform'])){
	exit;
}

header("Location: ".$_SERVER['HTTP_REFERER'].'#toevoegen_gebruiker');

include('conf/mysqli.php'); 
//include('action.php');

 
$vereniging_id       = $_POST['vereniging_id'];
$naam                = $_POST['naam'];
$licentie            = $_POST['licentie'];
  

if ($naam  ==''){
	$message .= "* Er is geen naam ingevuld.<br>";
	$error = 1;
}

if (licentie ==''){
	$message .= "* Er is geen licentie ingevuld.<br>";
	$error = 1;
}
 
  $qry   = mysqli_query($con,"SELECT * From namen where  Licentie =  '".$licentie."'  ")	or die(' Fout in select namen');  
  $result = mysqli_fetch_array( $qry );
  if ($result['Id']!='' ){
  	$message .= "* Toegangscode ".$licentie." is al bekend in het systeem."; 
	$error = 1;
 }
  
$sql      = mysqli_query($con,"SELECT * from vereniging where Id = ".$vereniging_id."  ") or die('Fout in select naam');  
$result     = mysqli_fetch_array( $sql );
$vereniging = $result['Vereniging'];

/// Toon foutmeldingen

if ($error == 1){

  $error_line      = explode("<br>", $message);   /// opsplitsen in aparte regels tbv alert
  ?>
   <script language="javascript">
        alert("Er zijn een of meer fouten gevonden bij het invullen :" + '\r\n' + 
            <?php
              $i=0;
              while ( $error_line[$i] <> ''){    
               ?>
              "<?php echo $error_line[$i];?>" + '\r\n' + 
              <?php
              $i++;
             } 
             ?>
              "")
    </script>
  <script type="text/javascript">
		history.back()
	</script>
<?php
 } // error = 1

if ($error == 0){
     $insert_query = "INSERT INTO `namen` (`Id`, `Vereniging_id`, `Vereniging`, `Licentie`, `Naam`,`Laatst`) 
	                              VALUES (NULL, ".$vereniging_id.", '".$vereniging."','".$licentie."','".$naam."', now() )";
 //echo "<br>". $insert_query;
 mysqli_query($con,$insert_query) or die ('Fout in insert ');   



 } // error = 0

?>

	
	


