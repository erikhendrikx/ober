<?php 
# reserveringen_per_naam.php
# Baanreservering  Report
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 17okt2020         -            E. Hendrikx 
# Symptom:   		None.
# Problem:     	    None
# Fix:              None
# Feature:          Link toegevoegd voor users per vereniging
# Reference: 
?>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">

@media print {
    body {
        margin: 10px;
		margin-top:15px;
		margin-right:15px;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 11px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
   }
	
@media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}

@media print {
    h5 {  font: 44px Arial, Helvetica,"Lucida Grande", serif;
          color:blue; }  
}

.noprint {display:none;}    
 
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
 $aangelogd = 'N';
include('aanlog_check_pin.php');
if ($aangelogd !='J'){
?>	
<script language="javascript">
		window.location.replace("index.html");
</script>
<?php
exit;
}
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id,Vereniging,Naam FROM namen WHERE  Vereniging_id = ".$_GET['ver_id']." and Licentie ='".$_GET['licentie']."'   ") or die(' Fout in vereniging Id');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$vereniging     = $result0['Vereniging'];
$naam           = $result0['Naam'];
 
 
$qry_namen   = mysqli_query($con,"SELECT * From namen WHERE  Vereniging_id = ".$_GET['ver_id']." order by Naam ")	or die(' Fout in select namen');  
  

?>
<div class= 'container'> 
  <div class= "row d-print-none ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th       data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?datum=<?php echo $datum;?>&<?php echo $kal;?>"   ></a>
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-list-alt data-toggle="tooltip" data-placement="bottom" title="Lijst "      href="report_reserveringen.php"   ></a>
  </div>

  
   
<h3> Report OBER  </h3>
  
 <?php
   $qry_res_totaal  = mysqli_query($con,"SELECT  count(*) as Aantal From baanres_kalender WHERE  Vereniging_id = ".$_GET['ver_id']." ")	or die(' Fout in select namen totaal ');  
   $result = mysqli_fetch_array( $qry_res_totaal );
   $totaal_namen  = $result['Aantal'];
   
  
?>
<br>
<h5 style= 'color:blue;font-weight:bold;'>Reservingen per licentie <?php echo $vereniging;?> - <?php echo $naam;?>   * [ <?php echo $_GET['licentie'];?> ]</h5>
 

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Datum</th>
      <th  >Baan</th>
      <th  >Begin</th>
      <th  >Eind </th>
      <th  >Spelers</th>
    </tr>
	</thead>
	<?php
	
	$i=1;
	$totaal_namen = 0;
 	$licenties = array();										
											
 $qry_res_kalender  = mysqli_query($con,"SELECT Id,Datum, Baan, Begin_tijd_reservering, Eind_tijd_reservering  From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id." and Licentie = '".$_GET['licentie']."' 
							    	    	order by Datum, Begin_tijd_reservering ")	or die(' Fout in select reservering ');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
			
		$part          = explode("-", $row['Datum']);
        $_datum        = strftime("%a %e %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 	 
			?>
			  	    <td><?php echo $_datum;?></td>
	     	    <td><?php echo $row['Baan'];?></td>
     	    <td><?php echo $row['Begin_tijd_reservering'];?></td>
     		<td><?php echo $row['Eind_tijd_reservering'];?></td>
     		<td>
     	    	<?php	 
     	    	$qry_res_splrs  = mysqli_query($con,"SELECT * From baanres_kalender
                            			    				where Vereniging_id = ".$vereniging_id." and 
															      Datum         = '".$row['Datum']."' and 
																  Baan          = ".$row['Baan']."
     	    						    				and Begin_tijd_reservering  = '".$row['Begin_tijd_reservering']."' order by Licentie ")	or die(' Fout in select reserv 2' );  
     	    	while($row2 = mysqli_fetch_array( $qry_res_splrs )) {
					
				 
    	    		echo $row2['Licentie']." ";
					
					if (!in_array($row2['Licentie'], $licenties)){
 					   $licenties[] =  $row2['Licentie'];
					}
     	    	}// end while
                    ?>
              </td>
     		
		</tr>
		<?php } // end while
?>	
    
	
 </table>
 <br>
 <h4> Licenties en namen  </h4>
 
   <table class="table table-striped table-hover" >
  <thead>
    <tr >
    <th  >Licentie</th>
    <th  >Naam</th>
   </tr>
   
   <?php
   foreach ($licenties as $licentie ){
	   
	   $qry_naam   = mysqli_query($con,"SELECT Licentie, Naam From namen WHERE  Vereniging_id = ".$_GET['ver_id']." and Licentie = '".$licentie."'  ")	or die(' Fout in select naam');  
       $result = mysqli_fetch_array( $qry_naam );
	   ?>
	   <tr>
	    <td  width= 15% ><?php echo $result['Licentie'];?></td>
        <td><?php echo $result['Naam'];?></td>
	   </tr>
	<?php   
   }// end foreach
   
   ?>
    </table>
   
   
  <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
  
 <hr/> 
  
 
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  
 </div>  <!---- container---->
  

 </body>
 </html>
 