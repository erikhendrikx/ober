<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">

@media print {
	
	@page {size: A4 landscape; }
	
	
    body {
        margin: 10px;
		margin-top:15px;
		margin-right:5px;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 9px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
  
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
 
    h5 {  font:11px Arial, Helvetica,"Lucida Grande", serif;
          color:blue; }  
}

.noprint {display:none;}    
 
h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

td{
  font-size: 1.2vh;
}



.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 
 
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$licentie       = $result0['Licentie'];
 
 ?>
 
  <div class= "row d-print-none ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?datum=<?php echo $datum;?>&<?php echo $kal;?>"   ></a>
  </div>

  
   
<h3> Report OBER  </h3>
  
 <?php
 $qry_res_verenigingen  = mysqli_query($con,"SELECT * From baanres_config  Order by  Vereniging_id ")	or die(' Fout in select config ');  
 	
?>
<br>
<h5 style= 'color:blue;font-weight:bold;'>Configuratie verenigingen</h5>

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th style='border-bottom:1pt solid red;'  class='text-right red' width=2% >Nr.</th>
      
   
   <?php
   $database   ='ontipnlv_baanreservering'; 
   $table_name ='baanres_config'; 
   $veld_qry    = mysqli_query($con,"SELECT COLUMN_NAME, DATA_TYPE   FROM INFORMATION_SCHEMA.COLUMNS   WHERE TABLE_SCHEMA = '".$database."' AND TABLE_NAME = '".$table_name."'  ");
  
  $cols = array();
 while($row = mysqli_fetch_array( $veld_qry )){
	 
	 if ($row['COLUMN_NAME'] !=	 'Laatst' and $row['COLUMN_NAME'] != 'Id' ){
	   $cols[] = $row['COLUMN_NAME'];
	 ?>
	 
	 <th style='border-bottom:1pt solid red;font-size:7pt;'  ><?php echo str_replace("_", " ",$row['COLUMN_NAME']);?></th>
	 <?php
	 }
	 ?>
	<?php 
      }/// end while
   ?>
   </tr>
   
   <?php
   $i=1;
    while($row1 = mysqli_fetch_array( $qry_res_verenigingen )){?>
	
	<tr>
	 <td style='font-size:9pt;text-align:right;'> <?php echo $i;?>.</td>
	   
	  <?php
	  foreach ($cols as $col_name){?>
	  <td style='font-size:9pt;'> <?php echo $row1[$col_name] ;?></td>
     <?php
	  } // end dor each
   ?>
     </tr>
	 <?php 
	 $i++;
	}// end while
	?>
   
   </table>
   
   
 
 	    <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
 <hr/>  
  
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  
   

 </body>
 </html>
 