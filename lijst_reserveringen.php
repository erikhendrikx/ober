<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="images/logo.ico">


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
<link href="css/bootstrap.min.css" rel="stylesheet">


<style type="text/css">
@media print {
    body {
        margin: 10;
        padding: 0;
        line-height: 1.4em;
        word-spacing: 1px;
        letter-spacing: 0.2px;
        font: 13px Arial, Helvetica,"Lucida Grande", serif;
        color: #000;
    }
	
.noprint {display:none;}    


h1 {
  font-size: 5.9vw;
}
h2 {
  font-size: 3.0vh;
}

h3{
  font-size: 2.4vh;
}

h4{
  font-size: 1.8vh;
}

h5{
  font-size: 1.2vh;
}

h6{
  font-size: 1vh;
}

.text-responsive {
  font-size: calc(100% + 1vw + 1vh);
}
 
 input.trash[type=checkbox] {
    display:none;
  }
  
input.trash[type=checkbox] + label
   {
	   background:url('images/backup_not_checked.png') no-repeat;
     height: 25px;
       width: 25px;  
       display:inline-block;
       padding: 0 0 0 0px;
   }
   
input.trash[type=checkbox]:checked + label
    {
	background:url('images/trash_checked.png') no-repeat;
       height: 25px;
       width: 25px;  
        display:inline-block;
        padding: 0 0 0 0px;
    }
	</style>
</head>

<body>
<?php
ini_set('default_charset','UTF-8');
setlocale(LC_ALL, 'nl_NL');

include('conf/mysqli.php'); 

$datum         = $_GET['datum'];

// moet omgekeerd zijn!!
if (isset($_GET['kal_on'])){
  $kal           = 'kal_off';
}
 
$ip             = $_SERVER['REMOTE_ADDR'];
$sql            = mysqli_query($con,"SELECT Vereniging_id, Licentie FROM namen WHERE  IP_adres = '".md5($ip)."'   and Aangelogd ='J'  ") or die(' Fout in select ip check');  
$result0        = mysqli_fetch_array( $sql );  
$vereniging_id  = $result0['Vereniging_id'];
$licentie       = $result0['Licentie'];

$licentie_md5       = $_GET['id'];

$part          = explode("-", $datum);
$_datum        = strftime("%A %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
 

  $qry   = mysqli_query($con,"SELECT * From namen where  md5(Licentie) =  '".$licentie_md5."'  ")	or die(' Fout in select namen');  
  $result = mysqli_fetch_array( $qry );
  $naam   = $result['Naam'];
  $licentie       = $result['Licentie'];

?>
<div class= 'container'> 
  <div class= "row d-print-none ">
  	 <a  class="btn btn-secondary btn-lg glyphicon glyphicon-th     data-toggle="tooltip" data-placement="bottom" title="Terug "      href="main.php?datum=<?php echo $datum;?>&<?php echo $kal;?>"   ></a>
  </div>

  <FORM action="verwijder_reservering.php" method=post  name='myForm'>
        <input type="hidden" name="zendform" value="1" /> 
        <input type="hidden" name="vereniging_id" value="<?php echo $vereniging_id;?>" /> 
        <input type="hidden" name="datum" value="<?php echo $datum;?>" /> 
        <input type="hidden" name="licentie" value="<?php echo $licentie;?>" /> 
   
<h3> Mijn Baan reserveringen  </h3>
 <a tabindex="0" width=10% class="btn btn-sm btn-info d-print-none" role="button" data-html='true' data-trigger="focus"  data-toggle="popover" data-placement="bottom"  title="Hoe verwijder ik een reservering ?" 
       data-content="Als je een reservering wilt verwijderen, zet dan een vinkje in de eerste kolom voor de reservering die je wilt verwijderen en klik op de knop 'Verwijder geselecteerde ....'.  Alleen jouw reservering wordt verwijderd!
	   <hr><span style='text-align:right;'><button>Sluit uitleg</button></span>">* Hoe verwijder ik een reservering?</a>
  

<h5>voor <?php echo $naam;?>  </h5>

  <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th class ='d-print-none' width=2% ><img src='images/trash_checked.png' width=20> </th>
      <th  >Datum</th>
      <th  >Week</th>
      <th  >Baan</th>
      <th  >Begin </th>
      <th  >Eind </th>
      <th  >Spelers</th>
    </tr>
	</thead>
	<?php
	$licenties = array();
	$today     = date('Y-m-d');
	$part      = explode("-", $today );
    $deze_week = strftime("%W", mktime(0, 0, 0, $part[1],$part[2],$part[0]) );	
	 										
	$qry_res_kalender  = mysqli_query($con,"SELECT * From baanres_kalender
                   			    		  where Vereniging_id = ".$vereniging_id." and Week(Datum) >= ".$deze_week."
							    	    	and Licentie = '".$licentie."' order by Datum, Begin_tijd_reservering ")	or die(' Fout in select reservering ');  
												
		while($row = mysqli_fetch_array( $qry_res_kalender )) {
			$part          = explode("-", $row['Datum'] );
            $_datum        = strftime("%a %d %b %Y",mktime(0,0,0,$part[1],$part[2],$part[0]));
		    $week_nr       = strftime("%W", mktime(0, 0, 0, $part[1],$part[2],$part[0]) );
	
                 ?>
	  <tr>
	    <td class='d-print-none'> 
            <?php
			$disable_option ='';
			// al gespeelde reserveringen in deze week mag je niet verwijderen
			if ($row['Datum'] < $today and $week_nr == $deze_week){
				  $disable_option = 'disabled';
			 } ?>	
	     	 <input type='checkbox' name= 'delete[]' class='trash  d-print-none' <?php echo $disable_option;?>  id= 'trash_<?php echo $row['Id']; ?>' value='<?php echo $row['Id']; ?>' /><label for="trash_<?php echo $row['Id']; ?>"></label>	
          </td>                 
		
		<td><?php echo $_datum;?></td>
        <td><?php echo $week_nr;?></td>
	    <td><?php echo $row['Baan'];?></td>
	    <td><?php echo $row['Begin_tijd_reservering'];?></td>
		<td><?php echo $row['Eind_tijd_reservering'];?></td>
		<td>
		<?php	 
		$qry_res_splrs  = mysqli_query($con,"SELECT * From baanres_kalender
                   			    				where Vereniging_id = ".$vereniging_id." and Datum = '".$row['Datum']."'
							    				and Begin_tijd_reservering  = '".$row['Begin_tijd_reservering']."' and Baan = ".$row['Baan']." 
									            Order by Licentie ")	or die(' Fout in select reserv 2' );  
												
		while($row2 = mysqli_fetch_array( $qry_res_splrs )) {
			echo $row2['Licentie']." ";
			 if(!in_array($row2['Licentie'], $licenties)){
			    $licenties[] = $row2['Licentie'];
			 }
		}// end while splrs row2
        ?>
       </td>
		</tr>
		<?php }  // end while kalender row 
?>		
 </table>
 
 <em class='d-print-none'><h6>Reserveringen van eerder in deze week kunnen niet verwijderd worden</h6></em><br>
 
       <input type="submit" class="btn btn-info d-print-none" value="Verwijder geselecteerde reserveringen">
 </form>
	<br>
	
	<br>
 
 <table class="table table-striped table-hover" >
  <thead>
    <tr >
      <th  >Nr</th>
      <th  >Toegangscode</th>
      <th  >Naam</th>
    </tr>
	</thead>
		
	<?php
	$i=1;
	if ($licenties !=''){
	foreach($licenties as $licentie){
	 
		$qry_naam_splrs  = mysqli_query($con,"SELECT Licentie, Naam From namen where Licentie ='".$licentie."' and Vereniging_id = ".$vereniging_id." ")	or die(' Fout in select namen' );  
		$row = mysqli_fetch_array( $qry_naam_splrs );
		?>
		  <tr>
			  <td><?php echo $i;?></td>
			  <td><?php echo $row['Licentie'];?></td>
			  <td><?php echo $row['Naam'];?></td>
			  </tr>
		<?php
		$i++;
		}// end foreach
    } // end if
?>		
		
 </table>
 	    <button class="btn btn-secondary d-print-none"  onclick="window.print();">Print pagina</button>
 <hr/> 
  
  
  <span style='font-size:9pt;'><img src="images/ober.png" width="25"   alt="" loading="lazy">  OnTip OBER  (c) Erik Hendrikx <?php echo date('Y');?>
  
 </div>  <!---- container---->
 
<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
$('.popover-dismiss').popover({
  trigger: 'focus'
})
</script>

 </body>
 </html>
 