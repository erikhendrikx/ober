<?php
# import_l_xlsx_stap2
# Record of Changes:
#
# Date              Version      Person
# ----              -------      ------
# 16okt2018          1.0.1           E. Hendrikx
# Symptom:   		 None.
# Problem:       	 None.
# Fix:               None.
# Feature:           None.
# Reference:  

 
include('conf/mysqli.php');
//include('action.php');
  
$ftp_server       = $_POST['ftp_server'];
$vereniging_id    = $_POST['vereniging_id'];
$name             = $_FILES['userfile']['name'];
$source_file      = $_FILES['userfile']['tmp_name'];
$paths            = "/public_html/baanreservering/xlsx/"; 
$timest           = date('ymdhi');
$destination_file = $paths.'reserveringen_'.$vereniging_id."_".$timest.'.xlsx';
$xlsx_file        = 'xlsx/reserveringen_'.$vereniging_id."_".$timest.'.xlsx';
 
//
if ($name  == '')
{
	$error =1;
	?>
	<script type="text/javascript">
	   	alert("Geen bestand geselecteerd !")
		  window.location.replace('import_reserveringen_xlsx_stap1.php');
</script>
<?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// set up a connection to ftp server
$conn_id          = ftp_connect($ftp_server);
ftp_pasv($conn_id,TRUE);
 
// login with username and password
$login_result     = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
 
// check connection and login result
if ((!$conn_id) || (!$login_result)) {
	$error = 1;
	?>
	<script type="text/javascript">
	   	alert("Probleem met verbinding maken met server!" + '\r\n' +
	          "Server : <?php echo  $ftp_server; ?> en user : <?php echo  $ftp_user_name; ?>")
		 	window.location.replace('import_reserveringen_xlsx_stap1.php');
</script>
<?php
}

// upload the file to the path specified

$upload = ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY);
 
 //echo "Download locatie : ". $paths."<br>";
 // echo "Source file      : ". $name;
 
  
// check the upload status
if (!$upload) {
	$error =1;
	?>
	<script type="text/javascript">
	   	alert("Bij uploaden is er iets fout gegaan!" + '\r\n' + 
	          "Download locatie : <?php echo  $paths; ?>" + '\r\n' +
	          "Source file      : <?php echo  $name; ?>")
		  window.location.replace('import_reserveringen_xlsx_stap1.php');
</script>
<?php
  } 
// close the FTP connection
ftp_close($conn_id);	

//////////////////////////////////////////////////////////////////////////////////////////////

if ($error == 0){
   include('import_reserveringen_xlsx_stap3.php');
}
?>
